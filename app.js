var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
var profile = require('./routes/profile');
var myMate = require('./routes/myMate');
var nearBy = require('./routes/nearBy');
var events = require('./routes/event');
var post = require('./routes/post');
var group = require('./routes/community')
var profilometer = require('./routes/profilometer');
var album = require('./routes/album');
var secret = require('./routes/secret');
var saved = require('./routes/saved');

// Vendor Middleware //

var property = require('./routes/vendor_routes/login');
var vendorProfile = require('./routes/vendor_routes/vendorProfile');
var vendorEvent = require('./routes/vendor_routes/event');
var document = require('./routes/vendor_routes/document');
var coupon = require('./routes/vendor_routes/coupon');
var vendorAlbum = require('./routes/vendor_routes/album');
var booking = require('./routes/vendor_routes/booking');
var community = require('./routes/vendor_routes/community')

var app = express();


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
const S3_BUCKET = process.env.S3_BUCKET;
//app.use(express.bodyParser({limit: '50mb'}));
app.use('/', index);
app.use('/user', users);
app.use('/profile', profile);
app.use('/mates', myMate);
app.use('/nearby', nearBy);
app.use('/events', events, vendorEvent);
app.use('/post',post);
app.use('/group', group);
app.use('/profilometer', profilometer);
app.use('/album', album, vendorAlbum);
app.use('/secret',secret);
app.use('/saved', saved)


//vendor //

app.use('/property', property);
app.use('/vendorProfile', vendorProfile);
//app.use('/vendorEvent', vendorEvent)
app.use('/document', document);
app.use('/coupon', coupon);
app.use('/booking', booking)
app.use('/community', community)



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  var msg  = err.message;
  var err = req.app.get('env') === 'development' ? err : {};


  // render the error page
    res.status(err.status || 500);
    res.header("Content-Type","application/json");

    res.json({
        message: msg,
        error: err
    });

 
});


module.exports = app;
