
var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const schema = require("../vendor/vendor_schema/schema");
var validator = require("validator");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');
var unirest = require("unirest");

var _ = require("lodash");



var vendorDB = model(seraphDB, 'vendorProperty');

vendorDB.schema = schema.property


vendorDB.useTimestamps('created','updated')


function addclaimProperty(props, callback) {

    if (!validator.isMobilePhone(props.phoneNo, 'en-IN')) {
        return callback(new errors.ValidationError(
            props.phoneNo + ' this mobile no is not valid mobile no.'));
    }

    vendorDB.where({phoneNo: props.phoneNo},{varName : "vendorProperty"},
        function (err, vendor) {
            if (err) {
                return callback(err);
            }     
       else {
           sendOtp(props.phoneNo).then(function (res) {
               if(res.Status === "Success") {
                   props.otpSession =  res.Details;
                   vendorDB.save(props, function (err, vendor) {
                       if (err)  {
                           return callback(err);
                       } else { 
                           
                           callback(null, vendor);
                       }
                    
                   });
               } else {
                return callback(new errors.ValidationError(
                       props.phoneNo + ' this mobile no is not in a service'));
               }
            }).then(function (err) {
               if(err)  return callback(err);

           });
       }
    });
}

function createvendorProperty(props, callback) {

    if (!validator.isMobilePhone(props.phoneNo, 'en-IN')) {
        return callback(new errors.ValidationError(
            props.phoneNo + ' this mobile no is not valid mobile no.'));
    }

    vendorDB.where({phoneNo: props.phoneNo},{varName : "vendorProperty"},
        function (err, user) {
            if (err)  return callback(err);
            
    //    if(user.length > 0) {
    //       return callback(new errors.ValidationError(
    //         props.phoneNo +   'this mobile no is already exists.'));
    //    } 
    else {
            vendorDB.save(props, function (err, user) {
                       if (err)  {
                           return callback(err);
                       } else {
                           //var user = new  User(user);
                           callback(null, user);
                       }
                    
                   });
               } 
            })
}

function checkpropertyName(city, name, callback){

    var params = {

        city:city,
        name:name
    }
    if(name ==''){
        return callback(new errors.ValidationError('Name should not be blank'))
    } else{
    var cypher = "MATCH (n:vendorProperty)  where n.city = {city} AND n.name = {name} return n as vendor"

    seraphDB.query(cypher,params,function(err, checkpropertyName){
        if (err) {
            return callback(err);
            
        } else {
            console.log(checkpropertyName)
            if(checkpropertyName.length > 0){
                let property = checkpropertyName[0];
                if(property.tempBusinessId){
                    return callback(new errors.ValidationError("Property already claim, use your Business ID to login"))
                } else{
                    return callback(null, property);
                }
            } else {
                return callback(new errors.ValidationError("Your property doesn't added yet in your system, please add you proerty"));
            }
           

        }
    })
}

}

function autocompelete(name ,city,   callback){

    var params = {
        name: name,
        city:city
        
    }
    if(name ==''){
        return callback(new errors.ValidationError('Name should not be blank'))
    }
    var cypher = "Match (n:vendorProperty) where  n.name contains {name} ";
    if(city && city!==''){
        cypher = cypher + "AND n.city = {city}"
    }
    cypher = cypher + " return  collect(n.name)  as vendor"
   

    seraphDB.query(cypher,params,function(err, checkpropertyName){
        if (err) {
            return callback(err);
            
        } else {
            console.log(checkpropertyName[0])
            return callback(null, checkpropertyName[0]);
        }
    })


}

function existingVendor(businessId, callback){

    var params = {
        businessId: businessId
    }

    // var cypher = "match(v:vendor) where v.businessId= {businessId} with v match(v)-[:HAS_VENDOR]->(vo:vendorProperty) " +
    //              "return v as vendor, vo as propety"

     var cypher = "match(vo:vendorProperty) where vo.tempBusinessId = {businessId}  return  vo as property"
    
    seraphDB.query(cypher,params, function(err, existingVendors){
      
        if(err){
            return callback(err)
        }else{
            if(existingVendors.length > 0){
                //console.log(existingVendors[0].vendor.businessId)
                console.log(existingVendors)
                sendOtp(existingVendors[0].phoneNo).then(function(res){
                    if(res.Status === 'Success'){
                       // existingVendors.otpSession = res.Details;
                        let property = existingVendors[0];
                        property.otpSession = res.Details;
                        console.log(property)
                        vendorDB.update(property, function (err, updatedVendor) {

                            if (err) {
                                return callback(err);
                            }
                            return callback(null, updatedVendor);
                        });
                    }
                })
            }else{
                return callback(new errors.ValidationError(
                    "The Busniess ID doesn't exit"));
            }
          
        }
          
    });

}

function sendOtpbyId(id , callback) {
    vendorDB.read(id, function (err, vendor) {
        if(err) {
            return callback(err);
        } else {
            if(vendor) {
                sendOtp(vendor.phoneNo).then(function (res) {
                    if (res.Status === "Success") {
                        vendor.otpSession = res.Details;
                        //vendor_.verified = false;

                        vendorDB.update(vendor, function (err, updatedVendor) {

                            if (err) {
                                return callback(err);
                            }
                            return callback(null, updatedVendor);
                        });
                    } else {
                        return callback(new errors.ValidationError(
                            "Mobile no is incorrect or not in service"));
                    }
                },function (err) {
                    //  console.log(err.message);
                    return callback(err);

                });
            }
        }
        });

 };

 

function sendOtp(phoneNo) {

    var deferred = defer();
    var req = unirest("GET", config.otpApiUrl + config.OtpApiKey + "/SMS/" + phoneNo + "/AUTOGEN");

    req.headers({
        "content-type": "application/x-www-form-urlencoded"
    });

    req.form({});

    req.end(function (res) {
        console.log(JSON.stringify(res));
        if (res.error) {
            console.log(JSON.stringify(res.error));
            deferred.reject(new Error(res.body.Details));
            //   return;
        } else {
            deferred.resolve(res.body);
        }
    });
    return deferred.promise;

}

function validateUserOtp(props, callback){
    vendorDB.read(props.id, function(err, vendor){
        if( err){
            return callback(err)
        }else{
            if(vendor){
                validateOtpNo(props.otpNo, vendor.otpSession).then(function(res){
                    if(res.Status === "Success"){
                        if(!vendor.tempBusinessId || (vendor.tempBusinessId && vendor.tempBusinessId == '')){
                            vendor.tempBusinessId = makeId();
                        }
                        vendor['status']= res.Status;
                        vendor['verified'] = true;
                      
                        try {
                            vendorDB.update(vendor, function(err, updateVendor){
                                if(err){
                                    return callback (err)
                                }else{
                                    return callback(null, updateVendor);
                                }
                            })

                        } catch (err) {
                            console.log(err);
                            throw err;
                        }
                    }else {
                        console.log(res);
                        return callback(new errors.ValidationError(res.Details));
                    }
                })
            }
        }
    }

)
}

function validateOtpNo(otpNo, sessionCode) {


    var deferred =   defer();
    var req = unirest("GET", config.otpApiUrl+config.OtpApiKey + "/SMS/VERIFY/" + sessionCode + "/" + otpNo);

    req.headers({
        "content-type": "application/x-www-form-urlencoded"
    });

    req.form({});

    req.end(function (res) {

        console.log(JSON.stringify(res));
        if (res.error) {
            console.log(JSON.stringify(res.error));
            deferred.reject(new Error(res.body.Details));
         //   return;
        } else {
            deferred.resolve(res.body);
        }
      //  if (res.error) return reject(res.body);


      //  return resolve(res.body)

    });


return deferred.promise;

}


function resendOtp(id , callback) {
    vendorDB.read(id, function (err, vendor) {
        if(err) {
            return callback(err);
        } else {
            if(vendor) {
                sendOtp(vendor.phoneNo).then(function (res) {
                    if (res.Status === "Success") {
                        vendor.otpSession = res.Details;
                        vendor.verified = false;

                        vendorDB.save(vendor, function (err, updatedVendor) {

                            if (err) {
                                return callback(err);
                            }
                            return callback(null, updatedVendor);
                        });
                    } else {
                        return callback(new errors.ValidationError(
                            "Mobile no is incorrect or not in service"));
                    }
                },function (err) {
                    //  console.log(err.message);
                    return callback(err);

                });
            }
        }
        });

 };

 function makeId(text) {
    var text = "DM";
    var id = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for (var i = 0; i < 5; i++)
        text += id.charAt(Math.floor(Math.random() * id.length)).substring(0,1);

    return text;
}

//  function getStatus(props, callback){
//     vendorDB.where({tempBusniessId: props.tempBusniessId},{varName : 'vendorProperty'},
//      function(err, vendorStatus){
//         if(err){
//             return callback(err)
//         }else{
//             return callback(null, vendorStatus)
//         }
//     })
// }

function getStatus(id, callback){

    var params = {
        id: id
    }

    // var cypher = "match(v:vendor) where v.businessId= {businessId} with v match(v)-[:HAS_VENDOR]->(vo:vendorProperty) " +
    //              "return v as vendor, vo as propety"

     var cypher = "match(vo:vendorProperty) where vo.tempBusinessId = {id}  return  vo as property"
    
    seraphDB.query(cypher,params, function(err, vendorStatus){
      
        if(err){
            return callback(err)
        }else{
            
            return callback(null, vendorStatus)
        }
          
    });

}

module.exports = {

    addclaimProperty:addclaimProperty,
    createvendorProperty: createvendorProperty,
    //update: update,
    validateUserOtp: validateUserOtp,
    resendOtp: resendOtp,
    sendOtpbyId: sendOtpbyId,
    checkpropertyName: checkpropertyName,
    autocompelete: autocompelete,
    getStatus: getStatus,
    existingVendor: existingVendor  
}