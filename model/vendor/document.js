var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const schema = require("../vendor/vendor_schema/schema");
var validator = require("validator");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');


var _ = require("lodash");


var vendor = model(seraphDB, 'vendor');
var document = model(seraphDB,'document');


vendor.schema = schema.vendor;
document.schema = schema.document;

vendor.compose(document, 'document', 'HAS_DOCUMENT', {updatesTimestamp: true})

function createdDoument(vendorDocument, callback){
    vendor.read(vendorDocument.id, function(err, documents){
        if(err){
            return callback(err)
        }else{
            if(!documents){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{

                var vendordocument = _.assign(documents, vendorDocument);
                vendor.update(vendordocument, function(err, document){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, document)
                    }
                })
            }
        }
    })
}

function updateVendoDocument(vendorBody, callback){
    
            vendor.update(vendorBody, function(err, afterupdateVendor){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, afterupdateVendor)
                    }
            })
            
}

function updateVerifyDocument(vendorBody, callback){

    document.read(vendorBody.id, function(err, existingDocument){
        if(err){
            return callback(err)
        }else{
            if(!existingDocument){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{

            var updateVendorDocument = _.mergeWith(existingDocument, vendorBody, customizer);
            
            document.update(vendorBody, function(err, afterupdateVendor){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, afterupdateVendor)
                    }
                })
            
            }
       }
    })
}

function customizer(objValue, srcValue) {

    //console.log(typeof objValue[0]);

    if (_.isArray(objValue)) {
       return   srcValue;
    }
}


function getVendorDcoument(id, callback){
    vendor.read(id,
     function(err, vendorStatus){
        if(err){
            return callback(err)
        }else{
            return callback(null, vendorStatus)
        }
    })
}

module.exports = {
    createdDoument : createdDoument,
    updateVendoDocument: updateVendoDocument,
    getVendorDcoument: getVendorDcoument,
    updateVerifyDocument: updateVerifyDocument
}

