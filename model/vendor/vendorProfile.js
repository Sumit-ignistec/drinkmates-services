var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const schema = require("../vendor/vendor_schema/schema");
var validator = require("validator");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');
var unirest = require("unirest");
var all = require("node-promise").all;

var item = require("./menu.json");

var _ = require("lodash");

var property = model(seraphDB, 'vendorProperty');
var vendor = model(seraphDB, 'vendor');
var location = model(seraphDB, 'location');
var vendorLocation = model(seraphDB, 'vendorLocation');
var openDays = model(seraphDB, 'openDays');
var details = model(seraphDB, 'details');
var structure = model(seraphDB, 'structure');
var menu = model(seraphDB, 'menu');
var photo = model(seraphDB, 'photo');
var table = model(seraphDB, 'table');



property.schema = schema.property;
vendor.schema  = schema.vendor;
location.schema = schema.location;
vendorLocation.schema = schema.vendorLocation;
openDays.schema = schema.openDays;
details.schema = schema.details;
structure.schema = schema.structure;
menu.schema = schema.menu;
photo.schema = schema.photo;
table.schema = schema.table;


property.compose(vendor, 'vendor', 'HAS_VENDOR', {many: false, updatesTimestamp:true});
vendor.compose(vendorLocation, 'vendorLocation', 'HAS_ADDRESS', {many: false, updatesTimestamp:true});
vendor.compose(location, 'location', 'HAS_LOCATION', {updatesTimestamp: true});
vendor.compose(openDays, 'openDays', 'HAS_OPENDAYS', {many: false, updatesTimestamp:true});
vendor.compose(details, 'details', 'HAS_DETAILS', {many: false, updatesTimestamp:true});
vendor.compose(structure, 'structure', 'HAS_STRUCTURE', {many: true, updatesTimestamp:true});
structure.compose(table, 'table', 'HAS_TABLE', {many: false, updatesTimestamp:true});
vendor.compose(menu, 'menu', 'HAS_MENU', {updatesTimestamp:true});
vendor.compose(photo, 'photo', 'HAS_PHOTO', {updatesTimestamp:true});


vendor.useTimestamps('created','updated');

function createVendorProfile(vendorProfile, callback){
    property.read(vendorProfile.id, function(err, prprty) {
        if(err) {
            return callback(err)
        }else{
            if(!prprty){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            } else{
                vendorProfile.isRegister = true;
                if(prprty.tempBusinessId) {
                    vendorProfile.vendor.businessId = prprty.tempBusinessId;
                } else {
                    vendorProfile.vendor.businessId = makeId();
                }

                var vendor = _.mergeWith(prprty, vendorProfile, customizer);
                console.log(vendor);
                property.update(vendor, function(err, vendor){
                     if(err){
                         return callback(err)
                     }           
                        else{

                            if(vendor.vendor.location.id){ 

                                var params = {
                                        id:vendor.vendor.location.id
                                }
                             
                                var cypher = "Match(l:location)  where ID(l) ={id} " 
                                            + "with  l call spatial.addNode('drinkmates', l) yield node return l as location"

                                seraphDB.query(cypher,params,function(err, spaitalLocation){
                                    if(err) {
                                        return callback(err);
                                    }
                                });


                                if(vendor.vendor && vendor.vendor.structure) {

                                    createTableStructure(vendor.vendor.structure, function(err, structures) {
                                        if(err) {
                                            return callback(err);
                                        } else {
                                            vendor.vendor.structure = structures;
                                        }


                                    });

                                }
                         return callback(null, vendor)
                     }
                    }
                
                 })
                
            }

        }

    })

}


// function addToSpacial(id) {
//     var params = {
//         id: id
//     }
//     var cypher = "Match(l:location)  where ID(l) ={id} "
//         + "with  l call spatial.addNode('drinkmates', l) yield node return l as location"

//     seraphDB.query(cypher,params,function(err, spaitalLocation){
//        if(err){
//         console.log(err.message);
//        }
//        console.log(spaitalLocation)
//     })
// }

function updateVendorProfile(vendorBody, callback){

    vendor.read(vendorBody.id, function(err, existingProfile){
        if(err){
            return callback(err)
        }else{
            if(existingProfile){

                var updateVendorProfile = _.mergeWith(existingProfile, vendorBody, customizer);
            vendor.update(updateVendorProfile, function(err, afterupdateVendor){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, afterupdateVendor)
                    }
                })
            }
        }
    })
}

function customizer(objValue, srcValue) {

    //console.log(typeof objValue[0]);

    if (_.isArray(objValue)) {
       return   srcValue;
    }
}

function getVendor(id, callback){
    property.read(id,
     function(err, vendorStatus){
        if(err){
            return callback(err)
        }else{
            return callback(null, vendorStatus)
        }
    })
}


function makeId(text) {
    var text = "DM";
    var id = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for (var i = 0; i < 5; i++)
        text += id.charAt(Math.floor(Math.random() * id.length)).substring(0,1);

    return text;
}


function getVendorPhoto(vendorId, type, callback){

    var params = {
        vendorId: parseInt(vendorId),
        type:type
    }

    var cypher = "match(vendor:vendor) where ID(vendor)= {vendorId} with vendor " +
                 "match(vendor)-[:HAS_PHOTO]->(photo:photo{type:{type}}) return photo.name as photo";


     seraphDB.query(cypher, params, function(err, getVendorPhotos){

        if(err){
            return callback(err)
        }else{
            return callback(null, getVendorPhotos)
        }
     })            
}

function createdMenu(Menu, callback){
    vendor.read(Menu.id, function(err, menus){
        if(err){
            return callback(err)
        }else{
            if(!menus){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{
                Menu.menu.dishQuantity = 0;
                var vendormenu = _.assign(menus, Menu);
                vendor.update(vendormenu, function(err, items){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, items)
                    }
                })
            }
        }
    })
}


// function getAllMenu(text, filterType, callback) {
//     if(item.menuItem) {
//         var filterMenu = item.menuItem.filter(function(items){
//             return items[filterType].toLowerCase()==text.toLowerCase()
//         })
//         return callback(null, filterMenu)
//     } else {
//         return callback(new Error("no menu found"));
//     }

// }

function getAllMenu(text, callback) {
    if(item.menuItem) {
        var filterMenu = item.menuItem.filter(function(items){
            return items.cuisineType.toLowerCase().startsWith(text.toLowerCase());
        })
        return callback(null, filterMenu)
    } else {
        return callback(new Error("no menu found"));
    }

}

function getAllrecommendedType(text, callback) {
    if(item.menuItem) {
        var filterMenu = item.menuItem.filter(function(items){
            return items.recommendedType.toLowerCase().startsWith(text.toLowerCase());
        })
        return callback(null, filterMenu)
    } else {
        return callback(new Error("no menu found"));
    }

}


function forgetBusinessId(phoneNo, callback) {
    property.where({phoneNo: phoneNo},{varName: "vendorProperty"},
     function (err, vendor) {
        console.log(vendor)
         if(err) {
             return callback(err);
         } else {
             if(vendor.length > 0) {
                 var vendor_ = vendor[0];
                 sendOtp(vendor_.phoneNo).then(function (res) {
                     if (res.Status === "Success") {
                         vendor_.otpSession = res.Details;
                         vendor_.verified = false;
 
                        property.update(vendor_, function (err, updatedVendor) {
 
                             if (err) {
                                 return callback(err);
                             }
                             return callback(null, updatedVendor);
                         });
                     } else {
                         return callback(new errors.ValidationError(
                             "Mobile no is incorrect or not in service"));
                     }
                 },function (err) {
                     //  console.log(err.message);
                     return callback(err);
 
                 });
             }
         }
         });
 
  };
 

  function sendOtp(phoneNo) {

    var deferred = defer();
    var req = unirest("GET", config.otpApiUrl + config.OtpApiKey + "/SMS/" + phoneNo + "/AUTOGEN");

    req.headers({
        "content-type": "application/x-www-form-urlencoded"
    });

    req.form({});

    req.end(function (res) {
        console.log(JSON.stringify(res));
        if (res.error) {
            console.log(JSON.stringify(res.error));
            deferred.reject(new Error(res.body.Details));
            //   return;
        } else {
            deferred.resolve(res.body);
        }
    });
    return deferred.promise;

}


function getVendorTableStructure(vendorId, reqObj , callback){
    var reqTimeStamp;
    if(reqObj && reqObj.date && reqObj.time) {
        var bookDate = new Date(reqObj.date);
        var bookTime = new Date(reqObj.time);
        reqTimeStamp = new Date(bookDate.getFullYear(), bookDate.getMonth(), bookDate.getDate(),bookTime.getHours(), bookTime.getMinutes(), 0, 0).getTime();

    }


    var params = {
        vendorId: parseInt(vendorId),
        floorCode: reqObj.floorCode,
        //timeStamp: queryObj.date
    }

    var cypher = "match(v:vendor) where ID(v)= {vendorId} with v  " +
                 "optional match(v)-[:HAS_STRUCTURE]->(str:structure)";
                 
    var fortable = " with v as vendor, str as structure match(structure)-[:HAS_TABLE]->(table:table) " +
                    "optional match(booking:booking)-[r:BOOKED_TABLE{isBooked:true}]->(table)";           
    var endQuery = " return table{.*, id:ID(table), booking:booking, isBooked:r.isBooked} as table";

    if(reqObj.floorCode){
        cypher = cypher + ' where  str.floorCode = {floorCode}';
    }
    
    cypher = cypher + fortable;
    console.log(cypher)

    if(reqTimeStamp) {
        params.timeStamp =  reqTimeStamp;
       
        cypher = cypher + ' where booking.bookingEndTimestamp >=  {timeStamp} AND booking.bookingTimestamp <= {timeStamp}'
    }


    cypher = cypher + endQuery;
    console.log(cypher)

    seraphDB.query(cypher, params, function(err, vendorTables){
        if(err){
            return callback(err)
        }else{
            
            return callback(null, vendorTables)
        }
    })
     
}





function createTableJSON(count, occupieSpace, tableArray, floorCode) {
   var lastCount = tableArray.length;
   var loopEnd = parseInt(count) + parseInt(lastCount);
   var loopStart = parseInt(lastCount)+1;
    for(var i = loopStart; i<= loopEnd; i++) {
        var table = {
            tableCode: floorCode+"T"+i,
            capacity: occupieSpace,
            inService: true,
            floorCode: floorCode
        }
        tableArray.push(table);
    }
}



function createTableStructure(structures, callback) {
    var tableStructures = structures.map(structure => saveTable(structure));
    var promises = all(tableStructures);

    promises.then(function(values) {
        return callback(null, values);
    },function (err) {
        return callback(err);
    });
}

function saveTable(tblStructure) {
    var deferred =   defer();
   var tableArray = [];
   if(tblStructure.doubleTable) {
       createTableJSON(tblStructure.doubleTable, 2, tableArray, tblStructure.floorCode);
   }

    if(tblStructure.singleTable) {
        createTableJSON(tblStructure.singleTable, 1, tableArray, tblStructure.floorCode);
    }

    if(tblStructure.fourTable) {
        createTableJSON(tblStructure.fourTable, 4, tableArray, tblStructure.floorCode);
    }

    if(tblStructure.sixTable) {
        createTableJSON(tblStructure.sixTable, 6, tableArray, tblStructure.floorCode);
    }

    if(tblStructure.eightTable) {
        createTableJSON(tblStructure.eightTable, 8, tableArray, tblStructure.floorCode);
    }
   

    tblStructure.table = tableArray;

    structure.update(tblStructure, function(err, tableStructure) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(tableStructure);
        }
    });

     return deferred.promise;

}



module.exports = {
    createVendorProfile: createVendorProfile,
    updateVendorProfile: updateVendorProfile,
    getVendor:getVendor,
    getVendorPhoto: getVendorPhoto,
    createdMenu: createdMenu,
    getAllMenu: getAllMenu,
    forgetBusinessId: forgetBusinessId,
    getAllrecommendedType:getAllrecommendedType,
    getVendorTableStructure: getVendorTableStructure
}