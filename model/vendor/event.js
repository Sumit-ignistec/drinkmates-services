var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const schema = require("../vendor/vendor_schema/schema");
const schemaUser = require("../post/schema");
var validator = require("validator");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');
var unirest = require("unirest");

var _ = require("lodash");

//var property = model(seraphDB, 'vendorProperty');
var vendor = model(seraphDB, 'vendor');
var vendorEvent = model(seraphDB, 'vendorEvent');
var vendorLocation = model(seraphDB, 'vendorEventLocation');



vendor.schema = schema.vendor;
vendorEvent.schemaUser = schemaUser.vendorEvent;
vendorLocation.schema = schema.vendorLocation;



vendor.compose(vendorEvent, 'vendorEvent', 'HAS_VENDOREVENT', {updatesTimestamp: true});
vendor.compose(vendorLocation, 'vendorLocation', 'HAS_ADDRESS', {many: false, updatesTimestamp:true});
//vendorEvent.compose(eventLocation, 'vendorEventLocation', 'VENDORPROPOSED_LOCATION', {many: true, updatesTimestamp:true});


vendorEvent.useTimestamps('created', 'updated');


function createdvendorEvent(vendorEvent, callback){
    vendor.read(vendorEvent.id, function(err, event){
        if(err){
            return callback(err)
        }else{
            if(!event){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{

                var vendorevent = _.assign(event, vendorEvent);
                vendor.update(vendorevent, function(err, events){
                    if(err){
                        return callback(err)
                    }else{

                        return callback(null, events)
                    }
                })
            }
        }
    })
}


function updateVendorEvent(events, callback){
    vendorEvent.read(events.id, function(err, updateEvent){
        if(err){
            return callback(err)
        }else{
            if(updateEvent){

                var updatevendorEvent = _.merge(updateEvent, events);
                console.log(updatevendorEvent)
                vendorEvent.update(updatevendorEvent, function(err, updatesEvent){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, updatesEvent)
                    }
                })
            }
        }
    })
}


module.exports = {
    createdvendorEvent: createdvendorEvent,
    updateVendorEvent: updateVendorEvent
}