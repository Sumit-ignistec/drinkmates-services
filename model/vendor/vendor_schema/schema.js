



const  property = {

    name:{type:String, required:true},
    phoneNo:{type:String, required:true},
    website:{type:String, required:true},
    address1:{type:String, required:true},
    address2:{type:String, required:true},
    city:{type:String, required:true},
    pincode:{type:Number},
    state:{type:String, required:true},
    tempBusinessId:{type:String}
}

const vendor = {
    name:{type:String},
    businessId: {type:String},
    description:{type:String},
    isActive:{type:Boolean},
    activePlan:{type:String}
}

const vendorLocation = {
    address1:{type:String, required:true},
    address2:{type:String, required:true},
    pinCode:{type:Number, required:true},
    city:{type:String, required:true},
    state:{type:String, required:true},
    country:{type:String},
}

const openDays = {
    day:{type:Array},
    fullStartHour:{type:Number},
    fullEndHour:{type:Number},
    breakfastStartHour:{type:Number},
    breakfastEndHour:{type:Number},
    lunchStartHour:{type:Number},
    lunchEndHour:{type:Number},
    dinnerStartHour:{type:Number},
    dinnerEndHour:{type:Number}
}

const details = {
    typeCuisin:{type:Array},
    isSelfService:{type:Boolean},
    isValetParking:{type:Boolean},
    isRoof:{type:Boolean},
    type:{type:String},
    minBudget:{type:String},
    area:{type:String},
    serviceType:{type:String},
    parkingCar:{type:String},
    parkingBike:{type:String},

    
}

const structure = {
    floorName:{type:String},
    floorCode:{type:String},
    type:{type:String, required:true},
    singleTable:{type:Number},
    doubleTable:{type:Number},
    fourTable:{type:Number},
    sixTable:{type:Number},
    eightTable:{type:Number},
    isSmokingAllowed:{type:Boolean},
    isAlcoholAllowed:{type:Boolean},

}

const table = {
    floorCode:{type:String},
    tableCode:{type:String},
    capacity:{type:Number},
    inService:{type:Boolean}
}

const menu = {
    name :{type:String, required:true},
    cuisineType:{type:String, required:true},
    isChefSpecial:{type:String},
    prices:{type:String, required:true},
    description:{type:String},
    spicyLevel:{type:String},
    availableStartTime:{type:String},
    availableEndTime:{type:String},
    availableDays:{type:Array},
    recommendedType:{type:String},
    image:{type:String},
    dishQuantity:{type:Number}
}

const photo = {
    type:{type:String},
    image:{type: Array}
}

const location = {
    latitude : { type:  Number},
    longitude : {type : Number},
    type: {type: String}
}


const document = {

    type: {type: String},
    documentName:{type:String},
    documentID:{type:Number},
    isVerified:{type:Boolean},
    image:{type:String},
    registerAddress:{type:String},
    address2:{type:String},
    state:{type:String},
    city:{type:String},
    pincode:{type:Number},
    expiryDate:{type:String}
}

const album = {
    name:{type:String, required:true}
}

const coupon = {

    promoName: {type:String, required:true},
    promotype: {type:String, required:true},
    comboName:{type:Array},
    comboPrice: { type: Number},
    comboSavedPrice: { type: Number},
    totalPrice:{type:Number},
    percentageMinBill:{type:Number},
    percentageDiscount:{type:Number},
    quantity: {type:String},
    redeemUse: {type:String},
    promoDescription: {type:String},
    promocode: {type:String},
    promoCodeType:{type:String},
    termsandconditions: {type:Array},
    image:{type:String}
 
   // redeemPeriodFrom: {type:String},
   // redeemPeriodTo: {type:String},
 //    item:{type:String},
 //    comboPrice:{type:Number},
 //    savedPrice: {type:Number},
 //    minBill: {type:Number},
 //    percentOff:{type:Number},
 //    startTime: {type:String},
 //    endTime: {type:String},
 //    openDays: {type:Array},
 //    combo: {type:String},
 //    percentage: {type:String},
 //    loyalty: {type:String},
    
 }

    
const validity = {
        validityType:{type:String},
        validityStartDate:{type:Number},
        validityEndDate:{type:Number},
        validityStartTime:{type:Number},
        validityEndTime:{type:Number},
        couponDate:{type:Array}
    }   

    const booking = {

        memberSize:{type:Number},
        bookingDate:{type:String},
        bookingTime:{type:String},
        bookingRequest:{type:Boolean},
        declineReason:{type:String},
        activeProperty:{type:String},
        guestName:{type:String},
        guestPhoneNo:{type:Number},
    }

module.exports = {
    property:  property,
    vendor: vendor,
    location: location,
    openDays: openDays,
    details: details,
    structure: structure,
    menu: menu,
    photo: photo,
    vendorLocation: vendorLocation,
    document: document,
    album : album,
    coupon: coupon,
    validity: validity,
    booking : booking,
    table: table   
    
}