'use strict';


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../post/schema");
const schemaVendor = require('../vendor/vendor_schema/schema')

var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');

var _ = require("lodash");
//var profileOMeter = require('../model/user/profilometer');






var vendor = model(seraphDB,'vendor');
var post = model(seraphDB,'post');
var coupon = model(seraphDB,'coupon');
var validity = model(seraphDB,'validity')
//var menu = model(seraphDB,'menu');



vendor.schemaVendor = schemaVendor.vendor;
post.schema = schema.post;
coupon.schemaVendor = schemaVendor.coupon;
validity.schemaVendor = schemaVendor.validity
//menu.schemaVendor = schemaVendor.menu;

vendor.compose(post, 'post', 'POST_CREATED_BY',{updatesTimestamp:true});
post.compose(coupon, 'coupon', 'HAS_COUPON', {updatesTimestamp:true});
coupon.compose(validity, 'validity', 'HAS_VALIDITY', {updatesTimestamp:true});
//vendor.compose(menu, 'menu', 'HAS_MENU',{updatesTimestamp:true});

function createdCoupon(vendorCoupon, callback){
    vendor.read(vendorCoupon.id, function(err, coupons){
        if(err){
            return callback(err)
        }else{
            if(!coupons){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{
                if(coupons.promocode){
                    vendorCoupon.post.coupon.promocode = coupons.promocode
                }else{
                  vendorCoupon.post.coupon.promocode = makeId();
                }
                var vendorcoupons = _.assign(coupons, vendorCoupon);
                vendor.update(vendorcoupons, function(err, coupon){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, coupon)
                    }
                })
            
            }
        }
    })
}

function makeId(text) {
    var text = "DM";
    var id = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for (var i = 0; i < 5; i++)
        text += id.charAt(Math.floor(Math.random() * id.length)).substring(0,1);

    return text;
}

function updateVendorCoupon(vendorBody, callback){

    post.read(vendorBody.id, function(err, existingCoupon){
        if(err){
            return callback(err)
        }else{
            if(!existingCoupon){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{ 
                
            var updateVendorcoupon = _.mergeWith(existingCoupon, vendorBody, customizer);
            
            post.update(updateVendorcoupon, function(err, afterupdateVendor){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, afterupdateVendor)
                    }
                })
            
            }
       }
    })
}

function customizer(objValue, srcValue) {

    //console.log(typeof objValue[0]);

    if (_.isArray(objValue)) {
       return   srcValue;
    }
}


function getVendorCouponTotal(vendorId, callback){

    var params = {
        vendorId: parseInt(vendorId)
    }

    var cypher = "match(v:vendor) where ID(v)= {vendorId} with v match(v)-[:POST_CREATED_BY]-(p:post{type:'vendor_post'}) with v,p " +
                  "match(p)-[:HAS_COUPON]->(c:coupon) return count(c) as vendorTotalCoupon"

    seraphDB.query(cypher, params, function(err, vendorCouponTotal){
        if(err){
            return callback(err);
        }else{
            return callback(null, vendorCouponTotal);
        }
    })             
}


function getVendorCoupon(vendorId, callback){

    var params = {
        vendorId: parseInt(vendorId)
    }
    
    var cypher = "Match(v:vendor)where ID(v)= {vendorId} with v match(v)-[:POST_CREATED_BY]->(p:post) with v, p match(p)-[:HAS_COUPON]->(c:coupon)-[:HAS_VALIDITY]->(validity:validity) " +
                 "return v as vendor, p as post,c as coupon, validity";
                 
     seraphDB.query(cypher, params, function(err, vendorCoupons){
         if(err){
             return callback(err)
         }else{
             return callback(null, vendorCoupons)
         }
     })            
}


module.exports = {
    createdCoupon: createdCoupon,
    updateVendorCoupon: updateVendorCoupon,
    getVendorCouponTotal: getVendorCouponTotal,
    getVendorCoupon: getVendorCoupon

}

