'use strict';


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../post/schema");
const schemaVendor = require('../vendor/vendor_schema/schema')

var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');

var _ = require("lodash");



var vendor = model(seraphDB,'vendor');
var community = model(seraphDB, 'community');



vendor.schemaVendor = schemaVendor.vendor;
community.schema = schema.community;


vendor.compose(community, 'community', 'VENDOR_CREATED_BY',{updatesTimestamp:true});

community.useTimestamps('created','updated');


function createdCommunity(vendorcommunity, callback){
    vendor.read(vendorcommunity.id, function(err, vendorCommunity){
        if(err){
            return callback(err)
        }else{
            if(!vendorCommunity){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{

                var vendordocument = _.assign(vendorCommunity, vendorcommunity);
                vendor.update(vendordocument, function(err, communityVendor){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, communityVendor)
                    }
                })
            }
        }
    })
}

// function updateCommunity(userGroup, callback) {

//     vendor.read(userGroup.id,function (err, groups) {
//         console.log(userGroup.id)
//         if(err) {
//             return callback(err);
//         } else {
//             if (groups) {
//                 console.log(groups)
//                 var updatedGroup = _.assign({},groups,userGroup);
//                 vendor.update(updatedGroup, function (err, groups) {
//                     if (err) {
//                         return callback(err);
//                     } else {
//                         return callback(null, groups);
//                     }
//                 });
//             } else {
//                 return callback(new errors.ValidationError(
//                     "this group  doesn't exists."));

//             }
//         }
//     });



// }

function communityNearByUser(vendorId, latitude, langitude, distance, callback){

    var params = {
        latitude: parseFloat(latitude),
        langitude: parseFloat(langitude),
        distance: parseFloat(distance),
        vendorId: parseInt(vendorId)
    }
    
 var cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude},longitude:{langitude}},{distance}) yield distance as d, node as location  with d,location " +
                     "match(loginVendor:vendor) where ID(loginVendor) = {vendorId} with loginVendor,d, location " +
                     "match(location)<-[:HAS_LOCATION]-(u:user) " +
                     "match(u)-[:HAS_PROFILE]->(p:profile) optional match(u)-[:HAS_PROFILOMETER]->(po:profileOMeter) optional  match(u)<-[r:INVITE]-(loginVendor) " +
                     "return u as user,d as distance, collect(p) as profile, po.overallRating as profileOMeter, r.status as status";

    seraphDB.query(cypher,params,function(err, nearByUsers) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, nearByUsers);
                    }
    });           
}

function shareToComunityPost(postId, communityId, queryObj, callback) {

    var params = {
        postId: postId,
        communityId: communityId,
        isShared: true,
        postedDate: queryObj.postedDate,
        description: queryObj.description,
        scheduleDate:queryObj.scheduleDate,
        scheduleTime:queryObj.scheduleTime
    
    }
    var cypher = "MATCH(g:community) where ID(g) = {communityId} with g Match(p:post) where ID(p) = {postId}  with p,g " +
                 "Merge (p)<-[r:SHARED_TO_COMMUNITY]-(g)  SET r.isShared= {isShared}, r.created=timestamp(), p.postedDate = timestamp() ,  p.scheduleTime= {scheduleTime}, p.description = {description} " +
                 "return g as community,p as post";

    
                 seraphDB.query(cypher,params,function(err, nearByUsers) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, nearByUsers);
                    }
    });    

}

function getVendorCommunity(vendorId, callback){
    
    var params = {
            vendorId: parseInt(vendorId)
    }

        var cypher = "match(v:vendor) where ID(v)= {vendorId} with v match(v)-[r:VENDOR_CREATED_BY]->(c:community) " +
                      "return c as community";

         
        seraphDB.query(cypher,params,function(err, vendorCommunity) {
                        if (err) {
                            return callback(err);
                            
                        } else {
                            return callback(null, vendorCommunity);
                        }
        });            
}


function vendorInviteUser(vendorId, userId, callback){

    var params = {
        vendorId: parseInt(vendorId),
        userId: parseInt(userId),
        status: 'pending'
    }

    var cypher = "MATCH(v:vendor) where ID(v)= {vendorId} with v Match(u:user) WHERE id(u) IN {userId} with v,u " +
                 "Merge (v)-[r:INVITE]->(u) SET r.status = {status}, r.created = timestamp() " +
                 "return u as user, v as vendor, r.status as status"


    seraphDB.query(cypher, params, function(err, vendorInvit){
        if(err){
            return callback(err)
        }else{
            return callback(null, vendorInvit)
        }
    })             
}


function communityPost(vendorId, callback){

    var params = {
        vendorId: parseInt(vendorId)
    }

    var cypher = "match(v:vendor) where ID(v)= {vendorId} with v match(v)-[:VENDOR_CREATED_BY]->(c:community) with v as vendor, c as community " +
                  "match(community)-[:SHARED_TO_COMMUNITY]->(p:post) optional match(p)-[:HAS_EVENT]->(e:event)  " +
                  "optional match(p)-[:HAS_COUPON]->(coupon:coupon) optional match (coupon)-[:HAS_VALIDITY]-(validity:validity) " +
                  "return e as event ,p as post ,coupon,validity"

    seraphDB.query(cypher, params, function(err, communitypost){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, communitypost)
                    }
                })                   
}

module.exports = {
    //updateCommunity: updateCommunity
    createdCommunity: createdCommunity,
    communityNearByUser: communityNearByUser,
    shareToComunityPost: shareToComunityPost,
    getVendorCommunity:getVendorCommunity,
    vendorInviteUser: vendorInviteUser,
    communityPost: communityPost
}