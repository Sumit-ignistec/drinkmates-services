'use strict';


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../post/schema");
const schemaVendor = require('../vendor/vendor_schema/schema')

var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');

var _ = require("lodash");

var notification = require("./../../utils/notification"); 

var user = model(seraphDB, 'user');
var vendor = model(seraphDB,'vendor');
var booking = model(seraphDB, 'booking');


user.schema = schema.user;
vendor.schemaVendor = schemaVendor.vendor;
booking.schemaVendor = schemaVendor.booking;


user.compose(booking, 'booking', 'BOOKED_BY',{updatesTimestamp:true});
booking.compose(vendor, 'vendor', 'FOR_VENDOR',{updatesTimestamp:true})

vendor.compose(booking, 'booking', 'WALK_IN_BOOKING',{updatesTimestamp:true})


booking.useTimestamps('created','updated');

function createdBooking(userBooking, callback){
    user.read(userBooking.id, function(err, booking){
        if(err){
            return callback(err)
        }else{
            if(!booking){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{
                if(userBooking.booking && userBooking.booking.bookingDate && userBooking.booking.bookingTime) {
                    var bookDate = new Date(userBooking.booking.bookingDate);
                    var bookTime = new Date(userBooking.booking.bookingTime);
                    var bookTimestamp  = new Date(bookDate.getFullYear(), bookDate.getMonth(), bookDate.getDate(), bookTime.getHours(), bookTime.getMinutes(), 0, 0);
                    userBooking.booking.bookingTimestamp = bookTimestamp.getTime();
                    userBooking.booking.bookingEndTimestamp = bookTimestamp.getTime() + (1 * 60 * 60 * 1000);
                }
                
                var userbooking = _.assign(booking, userBooking);
                user.update(userbooking, function(err, userbookings){
                    if(err){
                        return callback(err)
                    }else {
                        return callback(null, userbookings)
                    }
                })
            }
        }
    })
}


function createdWalkinBooking(userBooking, callback){
    vendor.read(userBooking.id, function(err, booking){
        if(err){
            return callback(err)
        }else{
            if(!booking){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."));
            }else{
            if(userBooking.booking && userBooking.booking.bookingDate && userBooking.booking.bookingTime) {
                    var bookDate = new Date(userBooking.booking.bookingDate);
                    var bookTime = new Date(userBooking.booking.bookingTime);
                    var bookTimestamp  = new Date(bookDate.getFullYear(), bookDate.getMonth(), bookDate.getDate(), bookTime.getHours(), bookTime.getMinutes(), 0, 0);
                    userBooking.booking.bookingTimestamp = bookTimestamp.getTime();
                    userBooking.booking.bookingEndTimestamp = bookTimestamp.getTime() + (1 * 60 * 60 * 1000);
                }
                var userbooking = _.assign(booking, userBooking);
               vendor.update(userbooking, function(err, userbookings){
                    if(err){
                        return callback(err)
                    }else{

                        // if(userBooking.tableId) {
                        //     allocateTableByVendor(userBooking.booking.id, userBooking.tableId);
                            
                        // }
                            return callback(null, userbookings)
                        }

                       
                    
                })
            
            
            }
        }
    })
}

function bookingRequestUpdate(bookingUser, callback) {

    booking.read(bookingUser.id,function (err, bookings) {
        if(err) {
            return callback(err);
        } else {
            if (bookings) {
                var updatedAlbum = _.merge({},bookings,bookingUser);
            booking.update(updatedAlbum, function (err, vendorBookings) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, vendorBookings);
                    }
                });
            } else {
                return callback(new errors.ValidationError(
                    "this id  doesn't exists."));

            }
        }
    });



}


function getBookingRequest(vendorId, callback){
    
    var params = {
        vendorId: parseInt(vendorId)
    }

    var cypher = "Match(v:vendor) where ID(v)= {vendorId} with v match(v)<-[:FOR_VENDOR]-(b:booking) with b, v " +
                 "match(b)<-[:BOOKED_BY]-(u:user)  return b{.*, id:ID(b), user:u } as booking ORDER BY booking.bookingDate  asc";

    seraphDB.query(cypher, params, function(err, requestBooking){
        if(err){
            return callback(err)
        }else{
            return callback(null, requestBooking)
        }
    })             

}

function confirmBookingList(vendorId, callback){

    var params = {
        vendorId: parseInt(vendorId)
    }

    var cypher = "Match(v:vendor) where ID(v)= {vendorId} with v match(v)<-[:FOR_VENDOR]-(b:booking{bookingRequest:true}) with v as vendor , b as booking " +
                 "Match(u:user)-[:BOOKED_BY]->(booking) with u , booking match(u)-[:HAS_PROFILE]->(p:profile) optional match(booking)-[:BOOKED_TABLE]-(table:table) return booking{.*, id:ID(booking), user:u, city:p.city, table:table.tableCode} as booking ORDER BY booking.bookingDate desc";

    seraphDB.query(cypher, params, function(err, confirmBooking){
        if(err){
            return callback(err)
        }else{
            return callback(null, confirmBooking)
        }
    })             

}

function getuserBooking(userId, callback){

    var params = {
        userId :parseInt(userId)
    }

    var cypher = "Match(u:user) where ID(u)= {userId} with u match(u)-[BOOKED_BY]->(b:booking) with u as user , b as booking " +
                 "Match(booking)-[:FOR_VENDOR]->(v:vendor) with v ,booking match(v)-[:HAS_ADDRESS]->(vl:vendorLocation) return v{.*, booking:booking, vendorLocation:vl}  as booking ORDER BY booking.bookingDate  DESC";


    seraphDB.query(cypher, params, function(err, userBookingList){
                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, userBookingList)
                    }
                })                  
}


function activeTableList(vendorId, callback){

    var params = {
        vendorId: parseInt(vendorId)
    }

    // var cypher = "Match(v:vendor) where ID(v)= {vendorId} with v match(v)<-[:FOR_VENDOR]-(b:booking{activeProperty:'checkin'}) with v as vendor , b as booking " +
    //              "Match(u:user)-[:BOOKED_BY]->(booking) with u , booking match(u)-[:HAS_PROFILE]->(p:profile) return booking{.*, id:ID(booking), user:u, city:p.city} as booking ORDER BY booking.bookingDate  desc";

var cypher = "Match(v:vendor) where ID(v)= {vendorId} with v match(v)<-[:FOR_VENDOR]-(b:booking{activeProperty:'checkin'}) with v as vendor , b as booking " +
             "Match(u:user)-[:BOOKED_BY]->(booking) with u , booking  optional match(booking)-[order:ORDER_BY_USER]->(menu:menu) optional match(booking)-[:BOOKED_TABLE]->(table:table) " +
             "optional match(u)-[:HAS_PROFILE]->(p:profile) with u , p,booking,table, collect(menu{.name, .prices,  dishQuanity:order.orderDishQuantity}) as order " +
             "return  booking{.*, id:ID(booking), user:u, city:p.city, order:order, table:table.tableCode} as booking ORDER BY booking.bookingDate  desc";

    seraphDB.query(cypher, params, function(err, activeTable){
        if(err){
            return callback(err)
        }else{
            return callback(null, activeTable)
        }
    })             


}


function allocateTableByVendor(bookingId, tableId, callback){

    var params = {
        bookingId: parseInt(bookingId),
        tableId: parseInt(tableId),
        //isBooked:true
    };


    var cypher = "MATCH (booking:booking) where ID(booking)= {bookingId} with booking match(table:table) where ID(table)= {tableId} with booking, table " +
                 "MERGE (booking)-[r:BOOKED_TABLE{isBooked:true}]->(table) " +
                 "return table.tableCode"
               

    seraphDB.query(cypher,params,function(err, allocteTable) {

        if (err) {
            return callback(err);
        } else{
                return callback(null, allocteTable);
        }
        
    });

}


function menuOrderByUser(bookingId, menuIds, orderDishQuantity, callback){

    var params = {
        bookingId: bookingId,
        menuIds: menuIds,
        orderDishQuantity: orderDishQuantity
    }

    var cypher = "Match(booking:booking) where ID(booking) = {bookingId} with  booking Match(menu:menu) where  id(menu) = {menuIds} with booking , menu " +
                  "Merge(booking)-[r:ORDER_BY_USER]->(menu) SET r.orderDishQuantity = {orderDishQuantity}, r.created = timestamp() " +
                  "return booking, menu";

        seraphDB.query(cypher, params, function(err, orderMenu){
            if(err){
                return callback(err)
            }else{
                return callback(null, orderMenu)
            }
        })          
}

module.exports = {
    createdBooking : createdBooking,
    bookingRequestUpdate: bookingRequestUpdate,
    getBookingRequest: getBookingRequest,
    confirmBookingList: confirmBookingList,
    getuserBooking:getuserBooking,
    activeTableList: activeTableList,
    allocateTableByVendor: allocateTableByVendor,
    menuOrderByUser:menuOrderByUser,
    createdWalkinBooking: createdWalkinBooking
    
}