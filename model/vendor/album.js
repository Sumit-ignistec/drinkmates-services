'use strict';


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../post/schema");
const schemaVendor = require('../vendor/vendor_schema/schema')

var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');

var _ = require("lodash");


var vendor = model(seraphDB, 'vendor')
var album = model(seraphDB,'album');
var photo = model(seraphDB, 'photo');


vendor.schemaVendor = schemaVendor.vendor;
album.schemaVendor = schemaVendor.album;
photo.schemaVendor = schemaVendor.photo;


vendor.compose(album, 'album', 'HAS_ALBUM', {updatesTimestamp: true});
album.compose(photo, 'photo', 'HAS_PHOTO', {updatesTimestamp: true})


album.useTimestamps('created', 'updated');

function createVendorAlbum(vendorAlbum, callback) {
    vendor.read(vendorAlbum.id,function (err, vendors) {
              if (err) {
                  return callback(err);
              } else {
  
                  if (!vendors) {
                      return callback(new errors.ValidationError(
                           "this Vendor ID   doesn't exists."));
                  } else {
                      var newAlbum = {
                          album: vendorAlbum.album
                      }
      
                      var albumCreate = _.assign(vendors, newAlbum);
                     vendor.update(albumCreate, function (err, vendoralbums) {
                          if (err) {
                              return callback(err);
                          } else {
                              return callback(null, vendoralbums);
                          }
                      });
  
                  }
              }
          });
  }

//   function updateAlbums(userAlbum, callback) {

//     album.read(userAlbum.id,function (err, albums) {
//         if(err) {
//             return callback(err);
//         } else {
//             if (albums) {
//                 var updatedAlbum = _.merge({},albums,userAlbum);
//                 album.update(updatedAlbum, function (err, album) {
//                     if (err) {
//                         return callback(err);
//                     } else {
//                         return callback(null, album);
//                     }
//                 });
//             } else {
//                 return callback(new errors.ValidationError(
//                     "this album  doesn't exists."));

//             }
//         }
//     });



// }


function updateAlbums(userAlbum, callback) {

    album.read(userAlbum.id,function (err, albums) {
          if(err) {
              return callback(err);
          } else {
              if (albums) {
                  var updatedAlbum = _.mergeWith(albums,userAlbum, customizer);
                album.update(updatedAlbum, function (err, album) {
                      if (err) {
                          return callback(err);
                      } else {
                          return callback(null, album);
                      }
                  });
              } else {
                  return callback(new errors.ValidationError(
                      "this album  doesn't exists."));
  
              }
          }
      });
  
  
  
  }
  
  
  function customizer(objValue, srcValue) {
  
      //console.log(typeof objValue[0]);
  
      if (_.isArray(objValue)) {
         return   srcValue;
      }
  }
  

function getVendorAlbum(vendorId, callback){

    var params = {
        
        vendorId: parseInt(vendorId)
    }
    
    var cypher = "match(v:vendor) where ID(v)={vendorId} with v match(v)-[:HAS_ALBUM]->(a:album) " +
                 " return a as album"

    seraphDB.query(cypher,params,function(err, getvendorAlbums){
        if (err) {
            return callback(err);
            
        } else {
            return callback(null, getvendorAlbums);
        }
    })
}

module.exports = {
    createVendorAlbum: createVendorAlbum,
    updateAlbums: updateAlbums,
    getVendorAlbum: getVendorAlbum
}