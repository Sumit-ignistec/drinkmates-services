'use strict';


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../post/schema");
var config = require("../../utils/config");
var model = require('seraph-model');
var event = require('./event');
var _ = require("lodash");

var notification = require("../../utils/notification");



var userDB = model(seraphDB, 'user');
var post = model(seraphDB, 'post');
var content = model(seraphDB,'content');
var activity = model(seraphDB,'activity');
var location = model(seraphDB, 'location');
var eventLocation = model(seraphDB, 'checkin');

userDB.schema = schema.user;
post.schema = schema.post;
content.schema = schema.content;
activity.schema =  schema.activity;
location.schema = schema.location;
eventLocation.schema = schema.eventLocation;





userDB.compose(post, 'post', 'POST_CREATED_BY', {updatesTimestamp: true});
post.compose(content, 'content', 'HAS_CONTENT', {updatesTimestamp: true});
post.compose(activity, 'activity', 'HAS_ACTIVITY', {updatesTimestamp: true});
post.compose(eventLocation, 'checkin', 'HAS_CHECK_IN', {updatesTimestamp:true});
eventLocation.compose(location, 'location', 'CHECK_IN_GEO_LOCATION', {updatesTimestamp:true});
post.compose(location, 'location', 'POST_GEO_LOCATION', {updatesTimestamp:true});



userDB.useTimestamps('created','updated');

function createPost (userPost, callback) {
    userDB.where({mobileNo: userPost.mobileNo},{varName : "user"},
        function (err, user) {
            if (err) {
                return callback(err);
            } else {

                if (user.length <= 0) {
                    return callback(new errors.ValidationError(
                        "this mobile no  doesn't exists."));
                } else {
                    var newPost = {
                        post: userPost.post
                    }
                    var userpost = _.assign(user[0], newPost);
                    userDB.update(userpost, function (err, post) {
                        if (err) {
                            return callback(err);
                        } else {
                            console.log(post);
                            if(post.post.location && post.post.location.id) {
                                event.addToSpacial(post.post.location.id);
                            }

                            if(post.post.checkin) {
                                if(post.post.checkin.location && post.post.checkin.location.id) {
                                    event.addToSpacial(post.post.checkin.location.id);
                                }
                            }

                            if(userPost.shareToFrndIds) {
                                event.shareToFrnd(post.post.id, userPost.shareToFrndIds, true);
                            }

                            if(userPost.notShareToFrndIDs) {
                                event.shareToFrnd(post.post.id, userPost.notShareToFrndIDs, false);
                            }

                            if(userPost.shareToGroupIds) {
                                event.shareToGrp(post.post.id, userPost.shareToGroupIds, true);
                            }

                            if(userPost.tagUserIds) {
                                tagUser(post.post.id, userPost.tagUserIds);
                            }

                            }
                            return callback(null, post);

                    });

                }
            }

        });
}

function updatePost(postBody, callback) {

    post.read(postBody.id ,function (err, existingPost) {
        if(err) {
            return callback(err);
        } else {
            if (existingPost) {
                //    console.log(existingEvent);
                var updatedPost = _.merge(existingPost, postBody);
           //     console.log(updatedPost);
                post.update(updatedPost, function (err, afterUpdatedPost) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, afterUpdatedPost);
                    }
                });
            }
        }
    });
}

function tagUser(postId, userIds){

    var params = {
        postId: postId,
        userId: userIds
    }

    // var cypher = "MATCH(p:post) where ID(p)={postId} with p Match(u:user) where ID(u) in {userId} with p,u merge (p)<-[tag:TAG_TO]-(u)"
    //     +" return u as user,p as post ,tag as tag"

  var cypher =  "Match(postCreator:user)-[:POST_CREATED_BY]->(p:post) where ID(p)={postId} with p, postCreator " +
                "Match(u:user) where ID(u) in {userId} with p,u,postCreator merge (p)<-[tag:TAG_TO]-(u) " +
                "return u as user,p as post ,tag as tag, postCreator.name as creator";

    seraphDB.query(cypher,params,function(err, tagUser){
        if(err) {
            console.log(err.message);
        }else{
            if(tagUser.length > 0){
                tagUser.forEach(host => {
                    
               
                      var notificationParams = {
                          notifyTo: host.user.id,
                          notifyFrom: host.post.id,
                          activityRequired: true,
                          notiFyType: 'tagPost',
                          notifyFromName: host.creator
                      }

                      

                      if(host.user && host.user.pushToken) {
                          notificationParams.pushToken = host.user.pushToken;
                          
                      }
      
                      notification.createNotification(notificationParams, function (err, notification) {
                          if (err) {
                              console.log(err.message);;
                          } else{
      
                          }
                      });
                  });
                       console.log(null , tagUser[0])
                      //return callback(null, cohost[0]);
                  }
              
                  else {
                      
                      console.log(new errors.ValidationError(
                          "user doesn't exist."));
                  }
            
        }
    })
}

function getUserPost(userId,callback) {
    var params = {
        userId: parseInt(userId)
    }
    
    var cypher = "match(u:user) where ID(u)= {userId} with u match(u)-[:POST_CREATED_BY]->(p:post) with u ,collect(p) as post1 " +
        "optional match(u)-[:REL_USER{status:'accepted'}]-(other:user)-[:POST_CREATED_BY]->(p:post{shareToAllfriend:true}) with u,other,collect(p) as post2, post1 " +
        "optional match(other)<-[:SHARED_TO{isShared:true}]-(p:post) with collect(p) as post3,post1,post2 optional match (other)-[:TAG_TO]->(p:post) with collect(p) as post4, post3,post1,post2 " +
        "UNWIND (post1+ post2 + post3+ post4) AS post with post as p optional match(p)-[:HAS_ACTIVITY]->(a:activity)  with p, a " +
        "optional match (p)-[:HAS_CHECK_IN]->(check:checkin) with p,a, check " +
        "optional match (p)<-[:POST_CREATED_BY]-(user:user)  with p,check, user{id:ID(user), .name, .imageName, .mobileNo} as createdUser,a  " +
        "optional match(p)<-[comms:COMMENTED]-(comm:user)   with  p, check, createdUser, collect(comm {.name, .mobileNo, comment: comms.comment, .imageName, created: comms.created,id: ID(comms), createdId: ID(comm)}) as comm, a " +
        "optional match(p)<-[liks:LIKE]-(likeUsers:user) with p, check, createdUser,comm ,{avg:avg(liks.star),users:collect(likeUsers{.name, .mobileNo ,star: liks.star, .imageName, created: liks.created,id: ID(liks), createdId: ID(likeUsers)})} as likes,a optional match (p)-[:HAS_CONTENT]->(c:content)   " +
        "with p, check, createdUser,comm, collect(c) as c,likes,a optional match (p)<-[:TAG_TO]-(tag:user) with p,check, createdUser, c, likes, collect(tag{.name}) as tag,a, comm  " +
        "optional match (p)-[:HAS_EVENT]->(e:event)-[:PROPOSED_LOCATION]->(el:eventLocation)  with e, collect(el) as el, p, c, check, a , tag, likes, comm, createdUser " +
        "optional match (e)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with e{id:ID(e),.*, eventLocation: el, interested: count(interestedUser)} as event, p, c, check, a , tag, likes, comm, createdUser " +
        "optional match(p)<-[coHost:CO_HOST]-(hostUser:user) with  event, p, c, check, a , tag, likes, comm, createdUser,collect(hostUser{.name}) as cohost " +
        "return p{.*, id:ID(p), content:c, checkin:check, activity:a,createdBy: createdUser, event:event, tagUser:tag, comments:comm,likes:likes, coHost: cohost} as post ORDER BY post.id desc";
    seraphDB.query(cypher,params,function(err, posts){
        if (err) {
            return callback(err);
        } else {
            return callback(null, posts);
        }
    })
}

function getmyPost(userId, callback) {
    var params = {
        userId: parseInt(userId)
    }

 var cypher = "match(u:user) where ID(u)= {userId} with u match(u)-[:POST_CREATED_BY]->(p:post) with u, p " +
         "optional match(p)-[:HAS_ACTIVITY]->(a:activity)  with p, a " +
        "optional match (p)-[:HAS_CHECK_IN]->(check:checkin) with p,a, check " +
        "optional match (p)<-[:POST_CREATED_BY]-(user:user)  with p,check, user{id:ID(user), .name, .imageName, .mobileNo} as createdUser,a  " +
        "optional match(p)<-[comms:COMMENTED]-(comm:user)   with  p, check, createdUser, collect(comm {.name, .mobileNo, comment: comms.comment, .imageName, created: comms.created,id: ID(comms), createdId: ID(comm)}) as comm, a " +
        "optional match(p)<-[liks:LIKE]-(likeUsers:user) with p, check, createdUser,comm ,{avg:avg(liks.star),users:collect(likeUsers{.name, .mobileNo ,star: liks.star, .imageName, created: liks.created,id: ID(liks), createdId: ID(likeUsers)})} as likes,a optional match (p)-[:HAS_CONTENT]->(c:content)   " +
        "with p, check, createdUser,comm, collect(c) as c,likes,a optional match (p)<-[:TAG_TO]-(tag:user) with p,check, createdUser, c, likes, collect(tag{.name}) as tag,a, comm  " +
        "optional match (p)-[:HAS_EVENT]->(e:event)-[:PROPOSED_LOCATION]->(el:eventLocation)  with e, collect(el) as el, p, c, check, a , tag, likes, comm, createdUser " +
        "optional match (e)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with e{id:ID(e),.*, eventLocation: el, interested: count(interestedUser)} as event, p, c, check, a , tag, likes, comm, createdUser " +
        "optional match(p)<-[coHost:CO_HOST]-(hostUser:user) with  event, p, c, check, a , tag, likes, comm, createdUser,collect(hostUser{.name}) as cohost " +
        "return p{.*, id:ID(p), content:c, checkin:check, activity:a,createdBy: createdUser, event:event, tagUser:tag, comments:comm,likes:likes, coHost: cohost} as post ORDER BY post.id desc";

    seraphDB.query(cypher,params,function(err, posts){
        if (err) {
            return callback(err);
        } else {
            return callback(null, posts);
        }
    })
}

// function getAvgStarByUser(postId, callback){

//     var params = {
//         postId: parseInt(postId)
//     }

//     var cypher = "MATCH(p:post) where ID(p)= {postId} with p " +
//                  "match (p)<-[liks:LIKE]-(users:user)-[:HAS_PROFILE]->(profile:profile) " +
//                  "return p{.*,avgStar:avg(liks.star),users:collect(users{.name, star: liks.star, imageName:profile.imageName, created: liks.created,id: ID(users)})} as likes";

//           seraphDB.query(cypher, params, function(err , avgLikes){

//             if(err){
//                 return callback(err)
//             }else{
//                 return callback(null , avgLikes)
//             }
//           })       


// }



module.exports = {
    createPost: createPost,
    updatePost:updatePost,
    getUserPost: getUserPost,
    getmyPost: getmyPost,
    //getAvgStarByUser: getAvgStarByUser
}