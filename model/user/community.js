/**
 * Created by sumitgabhane on 11/8/17.
 */
/**
 * Created by sumitgabhane on 6/21/17.
 */

'use strict';


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
var validator = require("validator");
const  schema = require("../post/schema");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');
var unirest = require("unirest");
var _ = require("lodash");
var cities = require("./cities.json");

var userDB = model(seraphDB, 'user');
var community = model(seraphDB,'community');

userDB.schema = schema.userDB;
community.schema = schema.community;

userDB.compose(community, 'community', 'GROUP_CREATED_BY', {updatesTimestamp: true});


community.useTimestamps('created','updated');

function createGroup(userGroup, callback) {
    userDB.where({mobileNo: userGroup.mobileNo},{varName : "user"},
        function (err, user) {
            if (err) {
                return callback(err);
            } else {

                if (user.length <= 0) {
                    return callback(new errors.ValidationError(
                         "this mobile no  doesn't exists."));
                } else {
                    var newGroup = {
                        community: userGroup.community
                    }

                    var groupCreate = _.assign(user[0], newGroup);
                    userDB.update(groupCreate, function (err, groups) {
                        if (err) {
                            return callback(err);
                        } else {
                            return callback(null, groups);
                        }
                    });

                }
            }
        });
}

function updateGroup (userGroup, callback) {

    community.read(userGroup.id,function (err, groups) {
                        console.log(userGroup.id)
                        if(err) {
                            return callback(err);
                        } else {
                            if (groups) {
                                console.log(groups)
                                var updatedGroup = _.assign({},groups,userGroup);
                                community.save(updatedGroup, function (err, groups) {
                                    if (err) {
                                        return callback(err);
                                    } else {
                                        return callback(null, groups);
                                    }
                                });
                            } else {
                                return callback(new errors.ValidationError(
                                    "this group  doesn't exists."));

                            }
                        }
                    });



}

function addMember(mobileNos, groupID, callback){
    
        var params = {
            mobileNos: mobileNos,
            groupID: groupID,
            //updated: new Date().getTime()
        };
        console.log(groupID)
        var cypher = "MATCH(g:community) where ID(g)= {groupID} with g Match(u:user) where u.mobileNo in {mobileNos} with g,u"
                    +" Merge (g)<-[:HAS_MEMBER{created: timestamp()}]-(u) return u as user,g as community"

                 console.log(cypher)
         seraphDB.query(cypher,params,function(err, tagmembers) {
                        if (err) {
                            return callback(err);
                            
                        } else {
                            return callback(null, tagmembers);
                            
                        }
        });
    
}


function removeMember(mobileNos, groupID, callback){
    
        var params = {
            
            mobileNos: mobileNos,
            groupID: groupID,
            //updated: new Date().getTime()
        };
    
        var cypher = "MATCH(g:community) where ID(g)= {groupID} with g match(u:user) where u.mobileNo in {mobileNos} with g,u "
                     +" Match (g)<-[r:HAS_MEMBER]-(u) delete r"
    
         seraphDB.query(cypher,params,function(err, untagmembers) {
                        if (err) {
                            return callback(err);
                            
                        } else {
                            return callback(null, untagmembers);
                        }
        });
    
}

function getGroupList(mobileNo, callback){
    
    var params = {
            mobileNo: mobileNo
    }

        var cypher = "match(u:user{mobileNo:{mobileNo}})-[:GROUP_CREATED_BY|:HAS_MEMBER]->(g:community) " + 
                    "WITH g,u match(g)<-[r:GROUP_CREATED_BY|:HAS_MEMBER]-(other:user) " +
                    " with g, u, other as members match (g)<-[r:GROUP_CREATED_BY]-(creator:user) " +
                     " return creator.name as name, ID(creator) as creatorId, count(members) as member, g as group, r.secret as secret"

         
        seraphDB.query(cypher,params,function(err, getMember) {
                        if (err) {
                            return callback(err);
                            
                        } else {
                            return callback(null, getMember);
                        }
        });            
}


function getGroupAndMember(groupID, callback){
    
        var params = {
                groupID: parseInt(groupID)
        }
    
            var cypher = "MATCH(g:community) where ID(g)= {groupID} with g   Match (g)<-[:HAS_MEMBER |:GROUP_CREATED_BY]-(u:user) with g ,u as user " +
                "Optional Match (user)-[r:HAS_PROFILE]->(profile:profile) return collect(user {user, profile}) as members, g as group";
    
             
            seraphDB.query(cypher,params,function(err, getGroupAndMember) {
                            if (err) {
                                return callback(err);
                                
                            } else {
                                return callback(null, getGroupAndMember);
                            }
            });            
    }

function nearByGroups(latitude, longitude, distance, callback){

    var params = {

        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
        distance: parseFloat(distance)
    }

       var cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude},longitude:{longitude}},{distance}) yield distance as distance ,node as location with distance,location"
                    +" Match(location)-[:HAS_LOCATION]-(u:user)-[:GROUP_CREATED_BY]->(g:community{type:'public'}) WITH g,u"
                    +" match(g)<-[r:GROUP_CREATED_BY |:HAS_MEMBER]-(other:user) with g, u, count(other) as member match (g)<-[r:GROUP_CREATED_BY]-(creator:user)  return creator.name as name ,g as group,  member, r.secret as secret"
         
        seraphDB.query(cypher,params,function(err, nearByGroups) {
                        if (err) {
                            return callback(err);
                            
                        } else {
                            return callback(null, nearByGroups);
                        }
        });            
}

function deleteGroup(groupID,callback){
    
    var params = {
            groupID: parseInt(groupID)
    }

        var cypher = "Match(g:community) where ID(g)= {groupID} DETACH delete g";

         
        seraphDB.query(cypher,params,function(err, deletegroups) {
                        if (err) {
                            return callback(err);
                            
                        } else {
                            return callback(null, deletegroups);
                        }
        });            
}


function getmyGroupPost(groupID, callback){

    var params = {
        groupID: parseInt(groupID)
      }

      var cypher = "match(g:community) where ID(g)= {groupID}  with g match(g)<-[:SHARED_TO]-(p:post) with g, p " +
         "optional match(p)-[:HAS_ACTIVITY]->(a:activity)  with p, a " +
        "optional match (p)-[:HAS_CHECK_IN]->(check:checkin) with p,a, check " +
        "optional match (p)<-[:POST_CREATED_BY]-(user:user)  with p,check, user{id:ID(user), .name, .imageName, .mobileNo} as createdUser,a  " +
        "optional match(p)<-[comms:COMMENTED]-(comm:user)   with  p, check, createdUser, collect(comm {.name, comment: comms.comment, .imageName, created: comms.created,id: ID(comms)}) as comm, a " +
        "optional match(p)<-[liks:LIKE]-(likeUsers:user) with p, check, createdUser,comm ,{avg:avg(liks.star),users:collect(likeUsers{.name, star: liks.star, .imageName, created: liks.created,id: ID(liks)})} as likes,a optional match (p)-[:HAS_CONTENT]->(c:content)   " +
        "with p, check, createdUser,comm, collect(c) as c,likes,a optional match (p)<-[:TAG_TO]-(tag:user) with p,check, createdUser, c, likes, collect(tag{.name}) as tag,a, comm  " +
        "optional match (p)-[:HAS_EVENT]->(e:event)-[:PROPOSED_LOCATION]->(el:eventLocation)  with e, collect(el) as el, p, c, check, a , tag, likes, comm, createdUser " +
        "optional match (e)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with e{id:ID(e),.*, eventLocation: el, interested: count(interestedUser)} as event, p, c, check, a , tag, likes, comm, createdUser " +
        "optional match(p)<-[coHost:CO_HOST]-(hostUser:user) with  event, p, c, check, a , tag, likes, comm, createdUser,collect(hostUser{.name}) as cohost " +
        "return p{.*, id:ID(p), content:c, checkin:check, activity:a,createdBy: createdUser, event:event, tagUser:tag, comments:comm,likes:likes, coHost: cohost} as post ORDER BY post.id desc";

        seraphDB.query(cypher,params,function(err, getmygroupPost) {
            if (err) {
                return callback(err);
                
            } else {
                return callback(null, getmygroupPost);
            }
});        

}

// function getownGroup(userId, callback){

//     var params = {

//         userId: parseInt(userId)
//     }

//     var cypher = "match(u:user) where ID(u)= {userId} with u match(u)-[:GROUP_CREATED_BY]->(g:group)  with u,g match(g)<-[:SHARED_TO]-(p:post) with g,p,u " +
//     "optional match(p)-[:HAS_ACTIVITY]->(a:activity)  with p, a ,g " +
//    "optional match (p)-[:HAS_CHECK_IN]->(check:checkin) with p,a, check, g " +
//    "optional match (p)<-[:POST_CREATED_BY]-(user:user)  with p,check, user{id:ID(user), .name, .imageName, .mobileNo} as createdUser,a ,g " +
//    "optional match(p)<-[comms:COMMENTED]-(comm:user)   with  p, check, createdUser, g, collect(comm {.name, comment: comms.comment, .imageName, created: comms.created,id: ID(comms)}) as comm, a " +
//    "optional match(p)<-[liks:LIKE]-(likeUsers:user) with p, check, g, createdUser,comm ,{avg:avg(liks.star),users:collect(likeUsers{.name, star: liks.star, .imageName, created: liks.created,id: ID(liks)})} as likes,a optional match (p)-[:HAS_CONTENT]->(c:content)   " +
//    "with p, check, createdUser,comm, collect(c) as c,likes,a ,g optional match (p)<-[:TAG_TO]-(tag:user) with p,check, g, createdUser, c, likes, collect(tag{.name}) as tag,a, comm  " +
//    "optional match (p)-[:HAS_EVENT]->(e:event)-[:PROPOSED_LOCATION]->(el:eventLocation)  with e, collect(el) as el, p, c, check, a , tag, likes, comm, createdUser, g " +
//    "optional match (e)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with e{id:ID(e),.*, eventLocation: el, interested: count(interestedUser)} as event, p, c, check, a , tag, likes, comm, createdUser, g " +
//    "optional match(p)<-[coHost:CO_HOST]-(hostUser:user) with  event, p, c, check, a , tag, likes, comm, g, createdUser,collect(hostUser{.name}) as cohost " +
//    "return p{.*, id:ID(p), content:c, checkin:check, activity:a,createdBy: createdUser, event:event, tagUser:tag, comments:comm,likes:likes, coHost: cohost, group: g} as post ORDER BY post.id desc";

//    seraphDB.query(cypher,params,function(err, getmygroup) {
//     if (err) {
//         return callback(err);
        
//     } else {
//         return callback(null, getmygroup);
//     }
// });        

// }

function getownGroup(userId,callback){
    
    var params = {
            userId: parseInt(userId)
    }

        var cypher = "match(u:user) where ID(u)= {userId} with u match(u)-[r:GROUP_CREATED_BY]->(g:community) " +
                     "optional  match(g)<-[:HAS_MEMBER]-(other:user) " +
                     "return g as group , u.name as name ,collect(other) as member";

         
        seraphDB.query(cypher,params,function(err, groups) {
                        if (err) {
                            return callback(err);
                            
                        } else {
                            return callback(null, groups);
                        }
        });            
}

module.exports = {
    createGroup: createGroup,
    updateGroup: updateGroup,
    addMember: addMember,
    removeMember: removeMember,
    getGroupList: getGroupList,
    getGroupAndMember: getGroupAndMember,
    nearByGroups: nearByGroups,
    deleteGroup: deleteGroup,
    getmyGroupPost:getmyGroupPost,
    getownGroup: getownGroup
    }