/**
 * Created by sumitgabhane on 7/1/17.
 */



const user = {
    name: { type: String, required: true },
    mobileNo: { type: String, required: true },
    countryCode: { type: String},
    imageName: {type: String}

}


 const token = {
     accessToken: { type: String},
     refreshToken : { type: String },
     lastloginTimestamp : { type: Number }
 }


 const profile = {
     gender : { type: String},
     dob : {type: String },
     city : {type: String },
     imageName : {type : String},
     profileHeadine : {type : String},
     aboutUs : {type : String},
     profession: {type : Array},
     languageKnown: {type : Array},
     drinkLikes: {type : Array},
     interest: {type : Array},
     displayField: {type : String}
}

 const location = {
     latitude : { type:  Number},
     longitude : {type : Number},
     type: {type: String}
 }

 const events = {
    
   name:{type:String, required:true},
   Description:{type:String, required:true},
   open:{type:String, required:true},
   close:{type:String, required:true},
   private:{type:String, required:true},
   commerical:{type:String, required:true},
   entryFees:{type:Number, required:true},
   photo:{type : Array},
   location:{type : Array}
}

const group = {
    name:{type:String, required:true},
    description:{type:String, required:true},
    type:{type:String, required:true},
    icon:{type:String}
}  

 module.exports = {
     user: user,
     token: token,
     profile: profile,
     location: location,
     group: group


 }