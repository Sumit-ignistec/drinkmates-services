var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../schema/schema");
var validator = require("validator");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');
var unirest = require("unirest");
var _ = require("lodash"); 

//var profile_ = require('./profile');

var userDB = model(seraphDB, 'user');
var profileOMeter = model(seraphDB,'profileOMeter');
 
userDB.schema = schema.user;
profileOMeter.schema = schema.profileOMeter;

userDB.compose(profileOMeter, 'profileOMeter', 'HAS_PROFILOMETER');

const registrationPoints = 0.12345;

const starDeductionForAbused = 100;
const starDeductionForUnfriendBlock = 50;

var calculateProfilometer = function(props, callback){
    var starAchived = props.ratingReceived;
    var ratingBy = props.ratingByMobileNo;
    var ratingTo = props.ratingToMobileNo;
    var fiveStar, fourStar, threeStar, twoStar, oneStar;
    var previousRating = props.previousRating;
    var ProfiloMeter;
    userDB.where({mobileNo: ratingTo},{varName : "user"},
        function (err, user) {
            if (err) {
                return callback(err);
            } else {

                if (user.length <= 0) {
                    return callback(new errors.ValidationError(
                         "this mobile no  doesn't exists."));
                } else {
                    userDB.readComposition(user[0],'profileOMeter',function(err,profileOMeter){
                        if(err){
                            return callback(err);
                        } else {
                            if(profileOMeter){
                                var profMeter = {
                                    profilometer: user[0].profileOMeter
                                }

                                //ProfiloMeter = _.assign(user[0], profMeter);

                                userDB.where({mobileNo:ratingBy},{varName : "ratingByUser"},function(err,ratingByUser){
                                    if(err){
                                        return callback(err);
                                    } else{
                                        if(ratingByUser.length <= 0)
                                        {
                                            return callback(new errors.ValidationError(
                                                "this mobile no. of rating by user dosen't exists."
                                            ))
                                        } else {
                                            userDB.readComposition(ratingByUser[0],'profileOMeter',function(err, profilOMeter){
                                                if(err){
                                                    return callback(err);
                                                } else {
                                                    if(profilOMeter){
                                                        var RatingByProfileOMeter = _.assign(ratingByUser[0],profilOMeter); 
                                                        user[0].profileOMeter = evaluateFormula(RatingByProfileOMeter,user[0].profileOMeter,starAchived,previousRating);
                                                        
                                                        //var userProiloMeter = _.assign(user[0], ProfiloMeter.profileOMeter);
                                                        userDB.update(user[0], function (err, profiloMeter) {
                                                            if (err) {
                                                                return callback(err);
                                                            } else {
                                                                return callback(null, profiloMeter);
                                                            }
                                                        });
                                                    }
                                                }
                                            })
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
                }

        });
 
}

var evaluateFormula = function(RatingByProfileOMeter,ratingForProfileOMeter,starAchived,previousRating){
    fiveStar = ratingForProfileOMeter.fiveStar;
    fourStar = ratingForProfileOMeter.fourStar;
    threeStar = ratingForProfileOMeter.threeStar;
    twoStar = ratingForProfileOMeter.twoStar;
    oneStar = ratingForProfileOMeter.oneStar;
    if(RatingByProfileOMeter.overallRating >= 4.5)
    {
        if(previousRating != null)
        {
            if(previousRating == 5)
            {
                fiveStar = fiveStar - 5; 
                ratingForProfileOMeter.fiveStar = fiveStar
            }
            else if(previousRating == 4)
            {
                fourStar = fourStar - 4;
                ratingForProfileOMeter.fourStar = fourStar
            }
            else if(previousRating == 3)
            {
                threeStar = threeStar - 3;
                ratingForProfileOMeter.threeStar = threeStar
            }
            else if(previousRating == 2)
            {
                twoStar = twoStar - 2;
                ratingForProfileOMeter.twoStar = twoStar
            }
            else if(previousRating == 1)
            {
                oneStar = oneStar - 1;
                ratingForProfileOMeter.oneStar = oneStar
            }
        }

        if(starAchived == 5)
        {
            fiveStar = fiveStar + 5;
            ratingForProfileOMeter.fiveStar = fiveStar
        }
        else if(starAchived == 4)
        {
            fourStar = fourStar + 4;
            ratingForProfileOMeter.fourStar = fourStar
        }
        else if(starAchived == 3)
        {
            threeStar = threeStar + 3;
            ratingForProfileOMeter.threeStar = threeStar
        }
        else if(starAchived == 2)
        {
            twoStar = twoStar + 2;
            ratingForProfileOMeter.twoStar = twoStar
        }
        else if(starAchived == 1)
        {
            oneStar = oneStar + 1;
            ratingForProfileOMeter.oneStar = oneStar
        }
    }
    else {
        if(previousRating != null)
        {
            if(previousRating == 5)
            {
                fiveStar = fiveStar - 1;
                ratingForProfileOMeter.fiveStar = fiveStar
            }
            else if(previousRating == 4)
            {
                fourStar = fourStar - 1;
                ratingForProfileOMeter.fourStar = fourStar
            }
            else if(previousRating == 3)
            {
                threeStar = threeStar - 1;
                ratingForProfileOMeter.threeStar = threeStar
            }
            else if(previousRating == 2)
            {
                twoStar = twoStar - 1;
                ratingForProfileOMeter.twoStar = twoStar
            }
            else if(previousRating == 1)
            {
                oneStar = oneStar - 1;
                ratingForProfileOMeter.oneStar = oneStar
            }
        }

        if(starAchived == 5)
        {
            fiveStar = fiveStar + 1;
            ratingForProfileOMeter.fiveStar = fiveStar
        }
        else if(starAchived == 4)
        {
            fourStar = fourStar + 1;
            ratingForProfileOMeter.fourStar = fourStar
        }
        else if(starAchived == 3)
        {
            threeStar = threeStar + 1;
            ratingForProfileOMeter.threeStar = threeStar  
        }
        else if(starAchived == 2)
        {
            twoStar = twoStar + 1;
            ratingForProfileOMeter.twoStar = twoStar
        }
        else if(starAchived == 1)
        {
            oneStar = oneStar + 1;
            ratingForProfileOMeter.oneStar = oneStar
        }
    }
    var rating = ((fiveStar*5)+(fourStar*4)+(threeStar*3)+(twoStar*2)+(oneStar*1))/(fiveStar+fourStar+threeStar+twoStar+oneStar)    
    ratingForProfileOMeter.overallRating = rating;
    return ratingForProfileOMeter;
}



var getUserProfileOMeter = function(props,callback){
    var mobileNo = props.mobileNo;
    userDB.where({mobileNo:mobileNo},{varName:"user"},function(err,user){
        if(err){
            callback(err);
        } else {
            if (user.length <= 0) {
                return callback(new errors.ValidationError(
                     "this mobile no  doesn't exists."));
            } else {
                //userDB.where(user[0],"profileOMeter",function(err,profileOMeter){
                return callback(null,user[0].profileOMeter);
                //})
            }
        }
    })
}
 
module.exports = {
    calculateProfilometer : calculateProfilometer, 
    getUserProfileOMeter : getUserProfileOMeter//,
    //evalUserInitialRating : evalUserInitialRating
}
 