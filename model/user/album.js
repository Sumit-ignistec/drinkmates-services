'use strict';

var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../post/schema");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');
var _ = require("lodash");



var userDB = model(seraphDB, 'user');
var album = model(seraphDB,'album');
var content = model(seraphDB,'content');



userDB.schema = schema.user;
album.schema = schema.album;
content.schema = schema.content;




userDB.compose(album, 'album', 'HAS_ALBUM', {updatesTimestamp: true});
album.compose(content, 'content', 'HAS_CONTENT',{updatesTimestamp: true});



album.useTimestamps('created', 'updated');

function createAlbum(userAlbum, callback) {
    userDB.where({mobileNo: userAlbum.mobileNo},{varName : "user"},
        function (err, user) {
            if (err) {
                return callback(err);
            } else {

                if (user.length <= 0) {
                    return callback(new errors.ValidationError(
                         "this mobile no  doesn't exists."));
                } else {
                    var newAlbum = {
                        album: userAlbum.album
                    }

                    var albumCreate = _.assign(user[0], newAlbum);
                    userDB.update(albumCreate, function (err, albums) {
                        if (err) {
                            return callback(err);
                        } else {
                           
                            if(userAlbum.coShareIds) {
                                coShare(albums.album.id, userAlbum.coShareIds,userAlbum.type);
                            }
                        
                            return callback(null, albums);
                        }
                    });

                }
            }
        });
}




function updateAlbum(userAlbum, callback) {

    album.read(userAlbum.id,function (err, albums) {
        if(err) {
            return callback(err);
        } else {
            if (albums) {
                var updatedAlbum = _.merge({},albums,userAlbum);
                album.update(updatedAlbum, function (err, album) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, album);
                    }
                });
            } else {
                return callback(new errors.ValidationError(
                    "this album  doesn't exists."));

            }
        }
    });



}


function coShare(albumId, userIds, type){

    var params = {
        albumId: albumId,
        userIds: userIds,
        type : type
    }

    var cypher = "MATCH(a:album) where ID(a)={albumId} with a Match(u:user) where ID(u) in {userIds} with a,u merge (a)<-[c:CO_SHARE{type:{type}}]-(u) " +
                 "return u as user,a as album ,c as coshare"

         seraphDB.query(cypher,params,function(err, coShare){
            if(err) {
                console.log(err.message);
            }
                })

}


function getuserAlbum(userId, callback){

    var params = {
        
        userId: parseInt(userId)
    }
    console.log(userId)
    var cypher = "match(u:user) where ID(u)={userId} with u match(u)-[:HAS_ALBUM]->(a:album) " +
                 " return a as album"

    seraphDB.query(cypher,params,function(err, getuserAlbums){
        if (err) {
            return callback(err);
            
        } else {
            return callback(null, getuserAlbums);
        }
    })
}

function getalbumContent(albumId, callback){

    var params = {
        
        albumId :parseInt(albumId)
    }
    console.log(albumId)
    var cypher = "MATCH(a:album) where ID(a)={albumId} with a match(a)-[r:HAS_CONTENT]->(c:content) with a, c " +
                 "RETURN a as album,collect(c) as content"

    seraphDB.query(cypher,params,function(err, getuserAlbums){
        if (err) {
            return callback(err);
            
        } else {
            return callback(null, getuserAlbums);
        }
    })
}


function deleteAlbum(userId, albumId, callback){

    var params = {

        userId: parseInt(userId),
        albumId: albumId
    }
    
    var cypher = "Match(u:user) where ID(u)= {userId} with u Match(u)-[:HAS_ALBUM]->(a:album) where ID(a) IN {albumId} DETACH delete a"

    seraphDB.query(cypher,params,function(err, deletealbum){
        if (err) {
            return callback(err);
            
        } else {
            return callback(null, deletealbum);
        }
    })
}

function deleteAlbumContent(userId, albumId,contentId, callback){

    var params = {

        userId: parseInt(userId),
        albumId: parseInt(albumId),
        contentId: contentId
    }
    
    var cypher = "Match(u:user) where ID(u)= {userId} with u Match(u)-[:HAS_ALBUM]->(a:album) where ID(a)={albumId} with u as user,a as album " + 
                 "match(a)-[content:HAS_CONTENT]->(c:content) where ID(c) IN {contentId} " +
                 "DETACH delete content"

    seraphDB.query(cypher,params,function(err, deletealbumContent){
        if (err) {
            return callback(err);
            
        } else {
            return callback(null, deletealbumContent);
        }
    })
}




module.exports = {
         createAlbum: createAlbum,
         updateAlbum: updateAlbum,
        getuserAlbum: getuserAlbum,
        getalbumContent: getalbumContent,
        deleteAlbum: deleteAlbum,
        deleteAlbumContent: deleteAlbumContent
}