var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
var config = require("../../utils/config");


function savedPost(userId, postId, callback) {
    var params = {
        userId: parseInt(userId),
        postId: parseInt(postId)
    }

 var cypher = "MATCH(u:user) where ID(u)= {userId} with u Match(p:post) where ID(p) = {postId} with u,p " +
              "Merge (u)-[r:SAVED_BY]->(p)  with u, p, u{id:ID(u), .name} as savedBy " +
              "return p{.*, savedby: savedBy} as post ORDER BY post.id desc";

    seraphDB.query(cypher,params,function(err, saved){
        if (err) {
            return callback(err);
        } else {
            return callback(null, saved);
        }
    })
}


// function savedBy(userId, postId, callback) {
//     var params = {
//         userId: parseInt(userId),
//         postId: parseInt(postId)
//     }

//  var cypher = "MATCH(u:user) where ID(u)= {userId} with u Match(p:post) where ID(p) = {postId} with u,p " +
//               "Merge (u)-[r:SAVED_BY]->(p)  with u, p, u{id:ID(u), .name} as savedBy " +
//               "optional match(p)-[:HAS_ACTIVITY]->(a:activity)  with p, a , savedBy " +
//               "optional match (p)-[:HAS_CHECK_IN]->(check:checkin) with p,a, check ,savedBy " +
//               "optional match (p)<-[:POST_CREATED_BY]-(user:user)  with p,check, user{id:ID(user), .name, .imageName, .mobileNo} as createdUser,a ,savedBy  " +
//               "optional match(p)<-[comms:COMMENTED]-(comm:user)   with  p, check, createdUser, collect(comm {.name, comment: comms.comment, .imageName, created: comms.created,id: ID(comms)}) as comm, a,savedBy " +
//               "optional match(p)<-[liks:LIKE]-(likeUsers:user) with p, check, createdUser,comm ,savedBy {avg:avg(liks.star),users:collect(likeUsers{.name, star: liks.star, .imageName, created: liks.created,id: ID(liks)})} as likes,a,savedBy " +
//               "optional match (p)-[:HAS_CONTENT]->(c:content) with p, check, createdUser,comm, collect(c) as c,likes,a,savedBy " +
//               "optional match (p)<-[:TAG_TO]-(tag:user) with p,check, createdUser, c, likes, collect(tag{.name}) as tag,a, comm ,savedBy " +
//               "optional match (p)-[:HAS_EVENT]->(e:event)-[:PROPOSED_LOCATION]->(el:eventLocation)  with e, collect(el) as el, p, c, check, a , tag, likes, comm, createdUser,savedBy " +
//               "optional match (e)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with e{id:ID(e),.*, eventLocation: el, interested: count(interestedUser)} as event, p, c, check, a , tag, likes, comm, createdUser , savedBy " +
//               "optional match(p)<-[coHost:CO_HOST]-(hostUser:user) with  event, p, c, check, a , tag, likes, comm, createdUser,collect(hostUser{.name}) as cohost,savedBy " +
//               "return p{.*, id:ID(p), content:c, checkin:check, activity:a,createdBy: createdUser, savedby: savedBy, event:event, tagUser:tag, comments:comm,likes:likes, coHost: cohost} as post ORDER BY post.id desc";

//     seraphDB.query(cypher,params,function(err, saved){
//         if (err) {
//             return callback(err);
//         } else {
//             return callback(null, saved);
//         }
//     })
// }


function getmySavedPost(userId, type, callback){

    var params = {
        userId: parseInt(userId),
        type: type,
        
    
    }
    
    var cypher = "match(u:user) where ID(u)= {userId} with u match(u)-[:SAVED_BY]->(p:post{type:{type}}) with u, p " +
                 "optional match(p)-[:HAS_ACTIVITY]->(a:activity)  with p, a " +
                 "optional match (p)-[:HAS_CHECK_IN]->(check:checkin) with p,a, check " +
                 "optional match (p)<-[:POST_CREATED_BY]-(user:user)  with p,check, user{id:ID(user), .name, .imageName, .mobileNo} as createdUser,a " +
                 "optional match(p)<-[comms:COMMENTED]-(comm:user)   with  p, check, createdUser, collect(comm {.name, comment: comms.comment, .imageName, created: comms.created,id: ID(comms)}) as comm, a " +
                 "optional match(p)<-[liks:LIKE]-(likeUsers:user) with p, check, createdUser,comm ,{avg:avg(liks.star),users:collect(likeUsers{.name, star: liks.star, .imageName, created: liks.created,id: ID(liks)})} as likes,a optional match (p)-[:HAS_CONTENT]->(c:content) " +
                 "with p, check,createdUser,comm, collect(c) as c,likes,a optional match (p)<-[:TAG_TO]-(tag:user) with p,check, createdUser, c, likes, collect(tag{.name}) as tag,a, comm " +
                 "optional match (p)-[:HAS_EVENT]->(e:event)-[:PROPOSED_LOCATION]->(el:eventLocation)  with e, collect(el) as el, p, c, check, a , tag, likes, comm, createdUser " +
                 "optional match (e)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with e{id:ID(e),.*, eventLocation: el, interested: count(interestedUser)} as event, p, c, check,a , tag, likes, comm, createdUser " +
                 "optional match(p)<-[coHost:CO_HOST]-(hostUser:user) with  event, p, c, check, a , tag, likes, comm, createdUser,collect(hostUser{.name}) as cohost " +
                 "return p{.*, id:ID(p), content:c, checkin:check, activity:a,createdBy: createdUser, event:event, tagUser:tag, comments:comm,likes:likes, coHost: cohost} as post ORDER BY post.id desc";
                 
    
                 seraphDB.query(cypher, params, function(err, mySaved){
                     if(err){
                         return callback (err)
                     }else{
                         return callback(null, mySaved)
                     }
                 })


}

function savedMates(fromuserId, touserId, callback){

    var params = {
        fromuserId: parseInt(fromuserId),
        touserId: parseInt(touserId)
    }

    var cypher = "MATCH(u:user) where ID(u)= {fromuserId} with u Match(other:user) where ID(other) = {touserId} with u,other " +
                 "match(other)-[:HAS_PROFILE]->(p:profile) with u , other , p as profile " +
                 "Merge (u)-[r:SAVED_BY]->(other)  return other{.*, profile: profile} as other ORDER BY other.id desc"

     seraphDB.query(cypher, params, function(err, savedMates){
         if(err){
             return callback(err)
         }else{
             return callback(null, savedMates)
         }
     })            
}

function getsaveMates(userId ,callback){

    var params = {
        userId: parseInt(userId)
    }

    var cypher = "match(u:user) where ID(u)={userId}  with u match(u)-[:SAVED_BY]->(other:user) with u , other " +
                 "optional match(other)-[:HAS_PROFILE]->(p:profile) with u , other ,p as profile " +
                 "optional match(other)-[:HAS_PROFILOMETER]->(po:profileOMeter) with u , other , profile , po as profileometer " +
                 "optional match(other)-[r:REL_USER]->(u) " +
                 "return  other as user, profile as profile, profileometer.overallRating as profileOMeter, r.status as status";

     seraphDB.query(cypher, params, function(err, savedMates){
         if(err){
             return callback(err)
         }else{
             return callback(null, savedMates)
         }
     })            
}



function saveCheckin(userId, checkinId, callback){

    var params = {
        userId: parseInt(userId),
        checkinId: checkinId
    }

    var cypher = "MATCH(u:user) where ID(u)= {userId} with u Match(p:post)-[:HAS_CHECK_IN]->(c:checkin) where ID(c)= {checkinId} " +
                 "Merge (u)-[r:SAVED_BY]->(c)  return c as checkin";

     seraphDB.query(cypher, params, function(err, savechekin){

        if(err){
            return callback(err)
        }else{
            return callback(null, savechekin)
        }
     })            
}







module.exports = {
    savedPost: savedPost,
    getmySavedPost: getmySavedPost,
    savedMates: savedMates,
    getsaveMates:getsaveMates,
    saveCheckin: saveCheckin
}










