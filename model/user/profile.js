/**
 * Created by sumitgabhane on 11/8/17.
 */
/**
 * Created by sumitgabhane on 6/21/17.
 */

'use strict';


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("./schema");
var validator = require("validator");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');
var unirest = require("unirest");
var _ = require("lodash");
var cities = require("./cities.json");
//var ProfiloMeter = require('../model/user/profilometer');

var userDB = model(seraphDB, 'user');
var profile = model(seraphDB, 'profile');
var location = model(seraphDB, 'location');
var profileOMeter = model(seraphDB,'profileOMeter');
const registrationPoints = 0.12345;


userDB.schema = schema.user;
profile.schema = schema.profile;
location.schema = schema.location;
profileOMeter.schema = schema.profileOMeter;


userDB.compose(profile, 'profile', 'HAS_PROFILE', {many: false, updatesTimestamp: true});
userDB.compose(location, 'location', 'HAS_LOCATION', {updatesTimestamp: true});
userDB.compose(profileOMeter, 'profileOMeter', 'HAS_PROFILOMETER', {updatesTimestamp: true});

userDB.useTimestamps('created','updated');
function createOrUpdateProfile (userProfile, callback) {
    var calculatedRating = 0.0;
    userDB.where({mobileNo: userProfile.mobileNo},{varName : "user"},
        function (err, user) {
            if (err) {
                return callback(err);
            } else {

                if (user.length <= 0) {
                    return callback(new errors.ValidationError(
                         "this mobile no  doesn't exists."));
                } else {
                    userDB.readComposition(user[0], 'profile' ,function (err, profile) {
                        if(err) {
                            return callback(err);
                        } else {
                            if (profile) {
                                user[0].profile = profile;
                                var userPrfle = _.mergeWith(user[0], userProfile, customizer);
                               // userPrfle = _.assign(userPrfle, newProfile);
                                userDB.update(userPrfle, function (err, profile1) {
                                    if (err) {
                                        return callback(err);
                                    } else {
                                        if(profile1 && profile1.profileOMeter) {
                                            profile1.profileOMeter.overallRating = evalUserInitialRating(userProfile.profile);
                                            userDB.update(profile1, function (err, profile2) {
                                                if (err) {
                                                    return callback(err);
                                                } else {
                                                    return callback(null, profile2);
                                                }
                                            });
                                        }
                                        return callback(null, profile1);
                                    }
                                }); 
                            } else {
                                var newProfile = {
                                    profile: userProfile.profile
                                }
                                var userPrfle = _.assign(user[0], newProfile);
                                userDB.save(userPrfle, function (err, profile) {
                                    if (err) {
                                        return callback(err);
                                    } else {
                                        if(profile.profileOMeter) {
                                            profile.profileOMeter.overallRating = evalUserInitialRating(userProfile.profile);
                                            userDB.update(profile, function (err, profiloMeter) {
                                                if (err) {
                                                    return callback(err);
                                                } else {
                                                    return callback(null, profiloMeter);
                                                }
                                            });
                                        }

                                        return callback(null, profile);
                                    }
                                });

                            }
                        }
                    });

                }
            }
        });
}


function createOrUpdateLocation (locationBody, callback) {
    userDB.where({mobileNo: locationBody.mobileNo},{varName : "user"},
        function (err, user) {
            if (err) {
                return callback(err);
            } else {

                if (user.length <= 0) {
                    return callback(new errors.ValidationError(
                        "this mobile no  doesn't exists."));
                } else {
                    userDB.readComposition(user[0], 'location' ,function (err, location) {
                        if(err) {
                            return callback(err);
                        } else {
                            if (location) {
                                user[0].pushToken = locationBody.pushToken;
                                var updatedlocation = {
                                    location: _.assign({},location,locationBody.location)
                                }
                                var updatedUserLocation = _.assign(user[0], updatedlocation);
                                userDB.save(updatedUserLocation, function (err, updatedLocation) {
                                    if (err) {
                                        return callback(err);
                                    } else {
                                        return callback(null, updatedLocation);
                                    }
                                });
                            } else {
                                var newLocation = {
                                    location: locationBody.location
                                }
                                var userLocation = _.assign(user[0],newLocation);

                                userDB.save(userLocation, function (err, createdLocation) {
                                    if(err) {
                                        return callback(err);
                                    } else {

                                       if(createdLocation.location.id){ 

                                        var params = {
                                                id:createdLocation.location.id
                                        }
                                     
                                        var cypher = "Match(l:location)  where ID(l) ={id} " 
                                                    + "with  l call spatial.addNode('drinkmates', l) yield node return l as location"

                                        seraphDB.query(cypher,params,function(err, spaitalLocation){

                                            if(err) {
                                                return callback(err);
                                            }else  {
                                                return callback(null, spaitalLocation);
                                            }
                                        })
                                    }
                                        return callback(null, createdLocation);
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
}





 function getProfile(mobileNo, callback) {

     userDB.where({mobileNo: mobileNo},{varName : "user"},
         function (err, user) {
             if (err) {
                 return callback(err);
             } else {
                 if (user.length <= 0) {
                     return callback(new errors.ValidationError(
                         "this mobile no is doesn't exists."));
                 } else {

                     userDB.readComposition(user[0], 'profile' ,function (err, profile) {
                         if(err) {
                             return callback(err);
                         } else {
                             if(profile) {
                                 var userProfile = {
                                     profile: profile
                                 }
                                 var userPrfle = _.assign(user[0], userProfile);

                                 userDB.readComposition(user[0], 'profileOMeter' ,function (err, profileOMeter) {
                                    if(err) {
                                        return callback(err);
                                    } else {
                                        if(profileOMeter)
                                        {
                                            var ProfileOMeter = {
                                                profileOMeter : profileOMeter
                                            }

                                            var profMeter = _.assign(user[0],ProfileOMeter);
                                        }
                                    }
                                 }); 
                                 
                                 return callback(null, userPrfle);
                             } else { 
                                 return callback(null, user[0]);
                             }
                         } 

                     });

                 }
             }
         });
}


function getLocation(mobileNo, callback) {

    userDB.where({mobileNo: mobileNo},{varName : "user"},
        function (err, user) {
            if (err) {
                return callback(err);
            } else {
                if (user.length > 0) {
                    return callback(new errors.ValidationError(
                        "this mobile no is doesn't exists."));
                } else {

                    userDB.readComposition(user[0], 'location' ,function (err, location) {
                        if(err) {
                            return callback(err);
                        } else {
                            if(location.length > 0) {
                                var userLocation = {
                                    location: location
                                }
                                var updatedLocation = _.assign(user[0], userLocation);

                                return callback(null, updatedLocation);
                            } else {
                                return callback(null, new errors.ValidationError(
                                    "no location found"));
                            }
                        }

                    });

                }
            }
        });
}

function getAllCities(text,callback) {
    if(cities.cities) {
        var filterCities = cities.cities.filter(function(city){
            return city.name.toLowerCase().startsWith(text.toLowerCase());
        })
        return callback(null, filterCities)
    } else {
        return callback(new Error("no cities found"));
    }

}

var evalUserInitialRating = function(profile){
    var userMobileNo = profile.mobileNo;
    var ProfiloMeter;
    var userInitialRating = 0.0;
    
    if(profile.gender){
        userInitialRating = userInitialRating + registrationPoints;
    } 

    if(profile.profession){
        userInitialRating = userInitialRating + registrationPoints;
    }

    if(profile.aboutUs){
        userInitialRating = userInitialRating + registrationPoints;
    }

    if(profile.interest){
        userInitialRating = userInitialRating + registrationPoints;
    }

    if(profile.city){
        userInitialRating = userInitialRating + registrationPoints;
    }

    if(profile.dob){
        userInitialRating = userInitialRating + registrationPoints;
    }

    if(profile.profileHeadine){
        userInitialRating = userInitialRating + registrationPoints;
    }

    if(profile.languageKnown){
        userInitialRating = userInitialRating + registrationPoints;
    }

    if(profile.drinkLikes){
        userInitialRating = userInitialRating + registrationPoints;
    }
    return userInitialRating;
                        
}

function customizer(objValue, srcValue) {

    //console.log(typeof objValue[0]);

    if (_.isArray(objValue) &&  _.isArray(srcValue)) {
       return srcValue;
    }
}


function userSearch(fromMobileNo, toMobileNo , callback){

    var params = {
        fromMobileNo: fromMobileNo,
        toMobileNo: toMobileNo,   
    }
    if(toMobileNo ==''){
        return callback(new errors.ValidationError('Please Enter Some Word'))
    }
    // var cypher = "match(loginUser:user) where loginUser.mobileNo = {mobileNo} with loginUser " +
    //              "optional match(loginUser)-[:HAS_PROFILE]->(p:profile) " +
    //              "optional match(loginUser)-[:HAS_PROFILOMETER]->(po:profileOMeter) " +
    //             "optional match(loginUser)-[:HAS_LOCATION]->(location:location) " +
    //             " optional match(loginUser)-[r:REL_USER]->(u:user) " +
    //            "return loginUser as user,p as profile, po.overallRating as profileOMeter, location, r.status as status";
   
   var cypher = "match(loginUser:user) where loginUser.mobileNo = {fromMobileNo}  with loginUser " +
                "match(other:user) where other.mobileNo = {toMobileNo} with other, loginUser where other.mobileNo <> {toMobileNo} " +
                "match(location:location)<-[:HAS_LOCATION]-(other) " +
                "optional match(other)-[:HAS_PROFILE]->(p:profile) optional match(other)-[:HAS_PROFILOMETER]->(po:profileOMeter) " +
                "optional match(other)<-[r:REL_USER]-(loginUser) " +
                 "return other as user, p as profile, po.overallRating as profileOMeter, r.status as status";

    seraphDB.query(cypher,params,function(err, userfind){
        if (err) {
            return callback(err);
            
        } else {
            
            return callback(null, userfind);
        }
    })


}

module.exports = {
    createOrUpdateProfile : createOrUpdateProfile,
    createOrUpdateLocation: createOrUpdateLocation,
    getProfile: getProfile,
    getLocation: getLocation,
    getAllCities: getAllCities,
    userSearch: userSearch

}


