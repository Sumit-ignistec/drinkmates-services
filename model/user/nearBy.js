var seraphDB = require('../../utils/db').seraphDB;



function getNearByUser(userId, latitude, langitude, distance, queryObj , callback) {
    var drinkLikes = [];
    var interest = [];

    if(queryObj.drinklikes) {
        if(queryObj.drinklikes.includes(',')) {
            drinkLikes = queryObj.drinklikes.split(',');
        } else {
            drinkLikes.push(queryObj.drinklikes);
        }
    }

    if(queryObj.interest) {
        if(queryObj.interest.includes(',')) {
            interest = queryObj.interest.split(',');
        } else {
            interest.push(queryObj.interest);
        }
    }

    var params = {
        latitude: parseFloat(latitude),
        langitude: parseFloat(langitude),
        distance: parseFloat(distance),
        city: queryObj.city,
        gender: queryObj.gender,
        drinkLikes: drinkLikes,
        interest: interest,
        userId: parseInt(userId)
    }
    // var cypher = "match(loginUser:user) where ID(loginUser) =  {userId}  with loginUser Call spatial.withinDistance('drinkmates',{latitude:{latitude},longitude:{langitude}},{distance}) yield  " +
    //     "distance as d, node as location  with d,location match(location)<-[:HAS_LOCATION]-(u:user) optional match(u)-[:HAS_PROFILE]->(p:profile) optional " +
    //     "match(u)-[:HAS_PROFILOMETER]->(po:profileOMeter) optional match(u)<-[r:REL_USER]-(loginUser)";
    // var endQuery =  " return u as user,d as distance, p as profile, po as profileOMeter, r.status as status";

    var cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude},longitude:{langitude}},{distance}) yield " +
            "distance as d, node as location  with d,location match(loginUser:user) where ID(loginUser) =  {userId}  with loginUser ,d,location match(location)<-[:HAS_LOCATION]-(u:user) where ID(u) <> {userId}" +
            " match(u)-[:HAS_PROFILE]->(p:profile) ";
    var endQuery = "optional match(u)-[:HAS_PROFILOMETER]->(po:profileOMeter)  optional  match(u)<-[r:REL_USER]->(loginUser) " +
        "return u as user,d as distance, p as profile, po.overallRating as profileOMeter, r.status as status";



    if(queryObj.city || queryObj.gender || queryObj.drinklikes || queryObj.interest) {
        cypher = cypher + ' where';
    }

    if(queryObj.city) {
        cypher = cypher + ' p.city = {city} AND'
    }

    if(queryObj.gender) {
        cypher = cypher + ' p.gender = {gender} AND'
    }

    if(queryObj.drinklikes) {
        console.log(queryObj.drinklikes);
        cypher = cypher + ' ANY (x IN p.drinkLikes WHERE x IN {drinkLikes}) AND'
    }

    if(queryObj.interest) {
        console.log(interest);
        cypher = cypher + ' ANY (x IN p.interest WHERE x IN {interest})'
    }

    if (cypher.endsWith('AND')) {
        cypher = cypher.substring(0, cypher.length - 3);
    }

    cypher = cypher + endQuery;

    console.log(cypher);

    seraphDB.query(cypher,params,function(err, nearByUsers) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, nearByUsers);
        }
    });

}


// function getNearByVendor(userId, latitude, langitude, distance, callback){

//     var params = {
//          userId: parseInt(userId),
//          latitude: parseFloat(latitude),
//          langitude: parseFloat(langitude),
//          distance: parseFloat(distance)
//     }

//     var cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude}, longitude:{langitude}},{distance}) yield distance as d, node as location  with d,location " +
//                  "match(loginUser:user) where ID(loginUser) =  {userId} with loginUser ,d,location " +
//                  "match(location{type:'vendor'})<-[:HAS_LOCATION]-(v:vendor) with  loginUser ,d,location, v " +
//                  "optional  match(v)-[:HAS_MENU]->(m:menu) optional match(v)-[:HAS_OPENDAYS]->(open:openDays) " +
//                  "optional match(v)-[:HAS_DETAILS]->(details:details) " +
//                  "optional match(v)-[:HAS_ADDRESS]->(vl:vendorLocation)  " +
//                  "optional match(v)-[:HAS_PHOTO]->(photo:photo) " +
//                  "return v{.*, .logo, id:ID(v) ,vendorlocation: vl, menu: m.cuisineType, opendays: open , Cuisine:details.typeCuisin, facility:details, distance: d , photo: collect(photo)} as vendor ORDER BY vendor.distance asc";


//                  seraphDB.query(cypher,params, function(err, nearVendor){

//                     if(err){
//                         return callback(err)
//                     }else{
//                         return callback(null, nearVendor)
//                     }
//                 })      
// }

function getNearByVendor(latitude, langitude, distance, queryObj, callback){

    var params = {
         latitude: parseFloat(latitude),
         langitude: parseFloat(langitude),
         distance: parseFloat(distance),
         city : queryObj.city,

    }
    
    //  var cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude}, longitude:{langitude}},{distance}) yield distance as d, node as location  with d,location " +
    //     "match(location{type:'vendor'})<-[:HAS_LOCATION]-(v:vendor) with d,location, v " +
    //     "optional  match(v)-[:HAS_MENU]->(m:menu) optional match(v)-[:HAS_OPENDAYS]->(open:openDays) " +
    //     "optional match(v)-[:HAS_DETAILS]->(details:details) " +
    //     "optional match(v)-[:HAS_ADDRESS]->(vl:vendorLocation) optional match(v)-[:HAS_PHOTO]->(photo:photo)";
    //     "return v{.*, .logo, id:ID(v) ,vendorlocation: vl, menu: m.cuisineType, opendays: open , Cuisine:details.typeCuisin, distance: d , photo: collect(photo)} as vendor ORDER BY vendor.distance asc";

   var cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude}, longitude:{langitude}},{distance}) yield distance as d, node as location  with d,location " +
                "match(location{type:'vendor'})<-[:HAS_LOCATION]-(v:vendor) with d,location, v " +
                "optional  match(v)-[:HAS_MENU]->(m:menu) optional match(v)-[:HAS_OPENDAYS]->(open:openDays) " +
                "optional match(v)-[:HAS_DETAILS]->(details:details) with v,open,details,m,d " +
                "match(v)-[:HAS_ADDRESS]->(vl:vendorLocation)";
  var endQuery = " optional match(v)-[:HAS_PHOTO]->(photo:photo) return v{.*, .logo, id:ID(v) ,vendorlocation: vl, menu: m.cuisineType, opendays: open , Cuisine:details.typeCuisin, distance: d , photo: collect(photo)} as vendor ORDER BY vendor.distance asc"

    if(queryObj.city){
        cypher = cypher + ' where';
    }

    if(queryObj.city) {
        cypher = cypher + ' vl.city = {city}'
    }
    
 
    

cypher = cypher + endQuery;

                 seraphDB.query(cypher,params, function(err, nearVendor){

                    if(err){
                        return callback(err)
                    }else{
                        return callback(null, nearVendor)
                    }
                })      
}




function vendorFilterByLocationAndCuisine(city, callback){

    var params = {
        city : city
    }

   

    var cypher = "match(v:vendor) optional match(v)-[:HAS_MENU]->(m:menu) " +
                 "optional match(v)-[:HAS_OPENDAYS]->(open:openDays) optional match(v)-[:HAS_DETAILS]->(details:details) with v,open,details,m " + 
                 "match(v)-[:HAS_ADDRESS]->(vl:vendorLocation) where vl.city = {city} " +
                "optional match(v)-[:HAS_PHOTO]->(photo:photo) " +
                "return v{.*, .logo, id:ID(v) ,vendorlocation: vl, menu: m.cuisineType, opendays: open , Cuisine:details.typeCuisin, facility:details, photo: collect(photo)} as vendor";

    console.log(cypher);

    seraphDB.query(cypher,params,function(err, aroundByVendor) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, aroundByVendor);
        }
    });

}


module.exports = {
    getNearByUser: getNearByUser,
    getNearByVendor: getNearByVendor,
    vendorFilterByLocationAndCuisine: vendorFilterByLocationAndCuisine
}