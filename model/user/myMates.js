
var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
var config = require("../../utils/config");
var notification = require("../../utils/notification")





function sendRequest(fromMobileNo, toMobileNo, callback){

    var params = {
        fromMobileNo: fromMobileNo,
        toMobileNo: toMobileNo,
        status: "pending",
        updated: new Date().getTime()
    };

    var cypher = "MATCH (user:user { mobileNo:{fromMobileNo}}),(other:user { mobileNo:{toMobileNo}})"
                 +" CREATE UNIQUE (user)-[r:REL_USER]->(other)  SET r.status={status}, r.updated = timestamp()" 
                 +"RETURN user, r.status as rel, other"

    seraphDB.query(cypher,params,function(err, result) {
        if (err) {
            return callback(err);
        } else {
            if(result.length > 0) {

                var notificationParams = {
                    notifyTo: result[0].other.id,
                    notifyFrom: result[0].user.id,
                    activityRequired: true,
                    notiFyType: 'sendRequest',
                    notifyFromName: result[0].user.name
                }

                if(result[0].other && result[0].other.pushToken) {
                    notificationParams.pushToken = result[0].other.pushToken;
                }

                notification.createNotification(notificationParams, function (err, notification) {
                    if (err) {
                        return callback(err);
                    } else{

                    }
                });



                return callback(null, result[0]);



            } else {
                return callback(new errors.ValidationError(
                    "user doesn't exist."));
            }

        }
    });

}


function acceptRequest(fromMobileNo, toMobileNo, callback){

    var params = {
        fromMobileNo: fromMobileNo,
        toMobileNo: toMobileNo,
        status: "accepted",
        updated: new Date().getTime()
    };


    var cypher = "MATCH (user:user { mobileNo:{fromMobileNo}}),(other:user { mobileNo:{toMobileNo}})"
                 +" MERGE (user)<-[r:REL_USER]->(other)  SET r.status={status}, r.updated = timestamp()"
                 + "RETURN user, r.status as rel, other"
               

    seraphDB.query(cypher,params,function(err, result) {

        if (err) {
            return callback(err);
        } else {
            if(result.length > 0) {

                var notificationParams = {
                    notifyTo: result[0].other.id,
                    notifyFrom: result[0].user.id,
                    activityRequired: true,
                    notiFyType: 'acceptRequest',
                    notifyFromName: result[0].user.name
                }

                if(result[0].other && result[0].other.pushToken) {
                    notificationParams.pushToken = result[0].other.pushToken;
                }

                notification.createNotification(notificationParams, function (err, notification) {
                    if (err) {
                        return callback(err);
                    } else{

                    }
                });



                return callback(null, result[0]);
            } else {
                return callback(new errors.ValidationError(
                    "user doesn't exist."));
            }
        }
    });

}

function updateVisited(fromMobileNo, toMobileNo, callback){

    var params = {
        fromMobileNo: fromMobileNo,
        toMobileNo: toMobileNo,
        //status: "Visited",
       // updated: new Date().getTime()
    };


    var cypher = "Match(u:user{mobileNo:{fromMobileNo}}),(other:user{mobileNo:{toMobileNo}}) match(u) WHERE NOT (u)-[:REL_USER{status:'accepted'}]->(other:user)"
                +" MERGE (u)-[r:VISITED]->(other) SET r.updated = timestamp()"
                +" RETURN u, type(r) as rel, other";


    seraphDB.query(cypher,params,function(err, result) {
        
                if (err) {
                    return callback(err);
                } else {
                    if(result.length > 0) {
                        return callback(null, result[0]);
                    } else {
                        return callback(new errors.ValidationError(
                            "user doesn't exist."));
                    }
                }
            });

}

function updateBlocked(fromMobileNo, toMobileNo, callback){
    
        var params = {
            fromMobileNo: fromMobileNo,
            toMobileNo: toMobileNo,
            status: "Blocked",
           // updated: new Date().getTime()
        };
    
    
            var cypher = "MATCH (user:user { mobileNo:{fromMobileNo}}),(other:user { mobileNo:{toMobileNo}})"
                        +"MERGE (user)<-[r:REL_USER]->(other)  SET r.status={status}, r.updated = timestamp()"
                        +"RETURN user, r.status as rel, other"
            
    
    
        seraphDB.query(cypher,params,function(err, matesBlocked) {
                    if (err) {
                        return callback(err);
                    } else {
                        if(matesBlocked) {

                            var notificationParams = {
                                notifyTo: matesBlocked[0].other.id,
                                notifyFrom: matesBlocked[0].user.id,
                                activityRequired: true,
                                notiFyType: 'blocked',
                                notifyFromName: matesBlocked[0].user.name
                            }
            
                            if(matesBlocked[0].other && matesBlocked[0].other.pushToken) {
                                notificationParams.pushToken = matesBlocked[0].other.pushToken;
                            }
            
                            notification.createNotification(notificationParams, function (err, notification) {
                                if (err) {
                                    return callback(err);
                                } else{
            
                                }
                            });
            
            
            
                            return callback(null, matesBlocked[0]);
            
            
            
                        } else {
                            return callback(new errors.ValidationError(
                                "user doesn't exist."));
                        }
                    }
                });
    
    }
    
    function updatePending(fromMobileNo, toMobileNo, callback){
        
            var params = {
                fromMobileNo: fromMobileNo,
                toMobileNo: toMobileNo,
                status: "pending",
               // updated: new Date().getTime()
            };
        
        
            var cypher = "MATCH (user:user { mobileNo:{fromMobileNo}}),(other:user { mobileNo:{toMobileNo}})"
                        +"MERGE (user)-[r:REL_USER]->(other)  SET r.status={status}, r.updated = timestamp()"
                        +"RETURN user, r.status as rel, other";
        
        
            seraphDB.query(cypher,params,function(err, matesPending) {
                
                        if (err) {
                            return callback(err);
                        } else {
                            if(matesPending.length > 0) {
                                return callback(null, matesPending[0]);
                            } else {
                                return callback(new errors.ValidationError(
                                    "user doesn't exist."));
                            }
                        }
                    });
        
        }
function unfriendRequest(fromMobileNo,toMobileNo, callback){

    var params = {
        fromMobileNo: fromMobileNo,
        toMobileNo: toMobileNo,
        status: "accepted",
        updated: new Date().getTime()
    };


    var cypher = "MATCH (user:user {mobileNo:{fromMobileNo}})"
                + "MATCH (other:user {mobileNo:{toMobileNo}})"
                + "MATCH (user)<-[r:REL_USER{status:{status}}]->(other)"
                + "DELETE r Return user,other";

      seraphDB.query(cypher,params,function(err, result) {
          if (err) {
              return callback(err);
          } else {
              if(result.length > 0) {
                  return callback(null, result[0]);
              } else {
                  return callback(new errors.ValidationError(
                      "user doesn't exist."));
              }
          }
      });

}

function cancelRequest(fromMobileNo, toMobileNo, callback){
    
        var params = {
            fromMobileNo: fromMobileNo,
            toMobileNo: toMobileNo,
            status: "pending",
            updated: new Date().getTime()
        };
    
    
        var cypher = "MATCH (user:user {mobileNo:{fromMobileNo}})"
                    + "MATCH (other:user {mobileNo:{toMobileNo}})"
                    + "MATCH (user)<-[r:REL_USER{status:{status}}]->(other)"
                    + "DELETE r Return user,other"
                   
    
        seraphDB.query(cypher,params,function(err, result) {
    
            if (err) {
                return callback(err);
            } else {
                if(result.length > 0) {
                    return callback(null, result[0]);
                } else {
                    return callback(new errors.ValidationError(
                        "user doesn't exist."));
                }
            }
        });
    
    }

    function rejectRequest(fromMobileNo, toMobileNo, callback){
        
            var params = {
                fromMobileNo: fromMobileNo,
                toMobileNo: toMobileNo,
                status: "pending",
                updated: new Date().getTime()
            };
        
        
            var cypher = "MATCH (user:user {mobileNo:{fromMobileNo}})"
                        + "MATCH (other:user {mobileNo:{toMobileNo}})"
                        + "MATCH (user)<-[r:REL_USER{status:{status}}]-(other)"
                        + "DELETE r Return user,other"
                       
        
            seraphDB.query(cypher,params,function(err, result) {
        
                if (err) {
                    return callback(err);
                } else {
                    if(result.length > 0) {
                        return callback(null, result[0]);
                    } else {
                        return callback(new errors.ValidationError(
                            "user doesn't exist."));
                    }
                }
            });
        
        }

function getStatus(mobileNo,status, callback){

        var params = {
                  status:status,
                  mobileNo: mobileNo
                }
    var cypher = "Match(user:user{mobileNo:{mobileNo}})-[r:REL_USER{status:{status}}]-(x:user) " +
        "with x, r optional match (x)-[:HAS_PROFILE]->(p:profile) RETURN x  AS user, p as profile, r.status as status,r.isSecret as secret";

    seraphDB.query(cypher,params,function(err, mateList) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, mateList);
        }
});
}

function getVisted(mobileNo, callback){

    var params = {
        mobileNo: mobileNo
    }

    // var cypher = "Match(user:user{mobileNo:{mobileNo}})-[r:VISITED]-(y:user) OPTIONAL MATCH (y)-[:HAS_PROFILE]->(p:profile) " +
    //     " with y, user, p, r.updated as updated match (y)-[rel]-(user) with y, filter(n IN collect(rel) where " +
    //     "type(n) ='REL_USER') AS filter , p, updated return y as user,p as profile, filter[0].status as status order by updated desc " ;
    
    var cypher = "Match(user:user{mobileNo:{mobileNo}})-[visited:VISITED]->(y:user) where y.mobileNo <> {mobileNo} " +
             "OPTIONAL MATCH (y)-[:HAS_PROFILE]->(p:profile) optional match(user)-[:HAS_PROFILOMETER]->(po:profileOMeter) " +
             "optional match (y)<-[r:REL_USER]->(user)" +
             "return y as user,p as profile,po as profileOMeter ,r.status as status order by visited.updated desc"

    seraphDB.query(cypher,params,function(err, mateList) {

        if (err) {
            return callback(err);
        } else {
            return callback(null, mateList);
        }
    });
}


function getUsers(mobileNo, callback){
    console.log(mobileNo.toString());
    var params = {
        mobileNo: mobileNo
    }

    var cypher = "Match (y:user) where y.mobileNo in {mobileNo} OPTIONAL MATCH (y)-[:HAS_PROFILE]->(p:profile)   return y as user ,p as profile";
    seraphDB.query(cypher,params,function(err, userList) {

        if (err) {
            return callback(err);
        } else {
            return callback(null, userList);
        }
    });
}

function getReceived(mobileNo,status, callback){
    //console.log(mobileNo.toString());
    var params = {
        status: status,
        mobileNo: mobileNo
    }

    var cypher = "MATCH (user:user {mobileNo:{mobileNo}})<-[r:REL_USER{status:{status}}]-(other)-[:HAS_PROFILE]->(p:profile) " +
                "Optional Match(other)-[:HAS_PROFILOMETER]->(po:profileOMeter) " +
                "RETURN  other as user,p as profile, po.overallRating as profileOMeter, r.status as status"
                 
    seraphDB.query(cypher,params,function(err, userReceived) {

        if (err) {
            return callback(err);
        } else {
            return callback(null, userReceived);
        }
    });
}


function getSent(mobileNo,status, callback){
    //console.log(mobileNo.toString());
    var params = {
        status: status,
        mobileNo: mobileNo
    }

    var cypher = "MATCH (user:user {mobileNo:{mobileNo}})-[r:REL_USER{status:{status}}]->(other)-[:HAS_PROFILE]->(p:profile) " +
                 "Optional Match(other)-[:HAS_PROFILOMETER]->(po:profileOMeter) " +
                 "RETURN  other as user,p as profile, po.overallRating as profileOMeter, r.status as status"   
        

    seraphDB.query(cypher,params,function(err, userReceived) {

        if (err) {
            return callback(err);
        } else {
            return callback(null, userReceived);
        }
    });
}
// function getfilter(req, res){
//     //console.log(mobileNo.toString());
//     // var params = {
//     //     filter:queryObj,
//     //     // latitude:queryObj.parseFloat(latitude),
//     //     // longitude:queryObj.parseFloat(langitude),
//     //     distance:queryObj
//     // }
//     //     console.log(params)
//     // var cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude},longitude:{longitude}},{distance})" 
//     //              " yield  distance as d, node as location  with d,location"
//     //             " match(location)<-[:HAS_LOCATION]-(user:user)-[r:REL_USER{status:'accepted'}]-(other)-[:HAS_PROFILE]->(p:profile{city:{filter}})"
//     //             "RETURN  other,p as Profile,d ";

//     var params = {
//         mobileNo: req.params.mobileNo,
//         gender:req.query.gender
//     }

//     var cypher ="Match(user:user{mobileNo:{mobileNo}})-[r:REL_USER{status:'accepted'}]-(other)-[:HAS_PROFILE]->(p:profile{gender:{gender}})"
//                  +" RETURN  other,p as Profile"

//     seraphDB.query(cypher,params,function(err, userReceived) {

//         if (err) {
//             return callback(err);
//         } else {
//             return callback(null, userReceived);
//         }
//     });
// }


module.exports = {
    sendRequest : sendRequest,
    acceptRequest: acceptRequest,
    unfriendRequest: unfriendRequest,
    cancelRequest: cancelRequest,
    rejectRequest : rejectRequest,
    getStatus: getStatus,
    updateVisited: updateVisited,
    updateBlocked: updateBlocked,
    updatePending: updatePending,
    getVisted: getVisted,
    getUsers: getUsers,
    getReceived: getReceived,
    getSent: getSent
    //getfilter: getfilter
}   