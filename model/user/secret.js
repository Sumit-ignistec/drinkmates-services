var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
var config = require("../../utils/config");



function addSecretMates(fromuserId, touserId, secret , callback){
    
    var params = {
        fromuserId: parseInt(fromuserId),
        touserId: parseInt(touserId),
        secret: secret
    };
  
    var cypher = "Match(user:user) where ID(user)={fromuserId} with user match(x:user) where ID(x)={touserId} with x ,user " +
                 "Merge(user)-[r:REL_USER{status:'accepted'}]->(x) SET r.isSecret={secret}, r.modified=timestamp() RETURN x as other, user, r.isSecret as secret"

    console.log(cypher)
    seraphDB.query(cypher,params,function(err, secretUser) {
                    if (err) {
                        return callback(err);
                        
                    } else {
                        return callback(null, secretUser);
                        
                    }
    });

}


// function addSecretGroup(groupId, userId, secret, isCreated ,callback){

//     var params = {
//         userId: parseInt(userId),
//         groupId: parseInt(groupId),
//         secret: secret
//     };

//     if(isCreated) {
//         params.relation = "GROUP_CREATED_BY"
//     } else {
//         params.relation = "HAS_MEMBER"
//     }
//         console.log(params.relation)
//     var cypher = "MATCH(g:group) where ID(g)={groupId} with g match (u:user) where ID(u)= {userId} with g, u" 
//                  +" merge (g)<-[r:{relation}]-(u)  set r.secret = {secret}, r.modified=timestamp() return  g as group, r as user";

//     seraphDB.query(cypher,params,function(err, secretGroup) {
//         if (err) {
//             return callback(err);

//         } else {
//             return callback(null, secretGroup);

//         }
//     });

// }

function addSecretGroup(groupId, userId, secret, isCreated, callback){

    var params = {
        userId: parseInt(userId),
        groupId: parseInt(groupId),
        secret: secret,
        isCreated: isCreated
       
    };
     var cypher;
     if(isCreated == true) {
         //params.relation = "GROUP_CREATED_BY"
         cypher = "MATCH(g:group) where ID(g)= {groupId} with g match (u:user) where ID(u)= {userId} with g, u" 
              +" merge(g)<-[r:GROUP_CREATED_BY]-(u)  set r.secret = {secret}, r.isCreated={isCreated}, r.modified=timestamp() return  g as group, r as user";
     } else {
         //params.relation = "HAS_MEMBER"
         cypher = "MATCH(g:group) where ID(g)= {groupId} with g match (u:user) where ID(u)= {userId} with g, u" 
              +" merge(g)<-[r:HAS_MEMBER]-(u)  set r.secret = {secret},r.modified=timestamp() return  g as group, r as user";
    }
    
   console.log(isCreated)


    seraphDB.query(cypher,params,function(err, secretGroup) {
        if (err) {
            return callback(err);

        } else {
            return callback(null, secretGroup);

        }
    });

}


module.exports = {
    addSecretMates: addSecretMates,
    addSecretGroup: addSecretGroup
}