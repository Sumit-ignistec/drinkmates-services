

'use strict';


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../post/schema");
const schemaVendor = require('../vendor/vendor_schema/schema')


var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');

var _ = require("lodash");
var profileOMeter = require('./profilometer');



var userDB = model(seraphDB, 'user');
var vendor = model(seraphDB, 'vendor')
var location = model(seraphDB, 'location');
var event = model(seraphDB, 'event');
var eventLocation = model(seraphDB, 'eventLocation');
var post = model(seraphDB,'post');
var activity = model(seraphDB,'activity');

userDB.schema = schema.user;
vendor.schemaVendor = schemaVendor.vendor
location.schemaVendor = schemaVendor.location;
event.schema = schema.event;
eventLocation.schema = schema.eventLocation;
post.schema = schema.post;
activity.schema =  schema.activity;



userDB.compose(post, 'post', 'POST_CREATED_BY', {updatesTimestamp: true});
vendor.compose(post, 'post', 'POST_CREATED_BY',{updatesTimestamp:true});
post.compose(event, 'event', 'HAS_EVENT', {updatesTimestamp:true});
post.compose(location, 'location', 'POST_GEO_LOCATION', {updatesTimestamp:true});
post.compose(activity, 'activity', 'HAS_ACTIVITY', {updatesTimestamp: true});
event.compose(eventLocation, 'eventLocation', 'PROPOSED_LOCATION', {many: true, updatesTimestamp:true});
eventLocation.compose(location, 'location', 'EVENT_GEO_LOCATION', {updatesTimestamp:true});

post.useTimestamps('created','updated');

function createEvent (userPost, callback) {
    userDB.where({mobileNo: userPost.mobileNo},{varName : "user"},
        function (err, user) {
            if (err) {
                return callback(err);
            } else {

                if (user.length <= 0) {
                    return callback(new errors.ValidationError(
                         "this mobile no  doesn't exists."));
                } else {

                  userPost.post.event.join = false;
                    var newPost = {
                        post: userPost.post
                    }
                    var userpost = _.assign(user[0], newPost);
                    userDB.update(userpost, function (err, post) {
                        if (err) {
                            return callback(err);
                        } else {
                            if(post.post.location && post.post.location.id) {
                                addToSpacial(post.post.location.id);
                            }

                            if(post.post.event && post.post.event.eventLocation) {

                                if(Array.isArray(post.post.event.eventLocation)) {
                                    post.post.event.eventLocation.forEach(event => {
                                        if(event.location && event.location.id) {
                                            addToSpacial(event.location.id);
                                        }
                                    })
                                } else {
                                    if(post.post.event.eventLocation.location.id) {
                                        addToSpacial(post.post.event.eventLocation.location.id);
                                    }
                                }

                                if(userPost.shareToFrndIds) {
                                    shareToFrnd(post.post.id, userPost.shareToFrndIds, true);
                                }

                                if(userPost.notShareToFrndIDs) {
                                    shareToFrnd(post.post.id, userPost.notShareToFrndIDs, false);
                                }

                                if(userPost.shareToGroupIds) {
                                    shareToGrp(post.post.id, userPost.shareToGroupIds, true);
                                }

                                if(userPost.coHostIds) {
                                    coHost(post.post.id, userPost.coHostIds, user[0]);
                                }

                            }
                            return callback(null, post);
                        }
                    });

                }
                }

        });
}


function createdVendorEvent(vendorEvent, callback){
    vendor.read(vendorEvent.id, function(err, vendorevent){
        if(err){
            return callback(err)
        }else{
            if(!vendorevent){
                return callback(new errors.ValidationError(
                    "this ID  doesn't exists."))
            }else{
                var newPost = {
                    post: vendorEvent.post
                }
                var event = _.assign(vendorevent, newPost);
                vendor.update(event, function(err, post){
                    if(err){
                        return callback(err)
                    }else{
                        if(post.post.location && post.post.location.id) {
                            addToSpacial(post.post.location.id);
                        }

                        if(post.post.event && post.post.event.eventLocation) {

                            if(Array.isArray(post.post.event.eventLocation)) {
                                post.post.event.eventLocation.forEach(event => {
                                    if(event.location && event.location.id) {
                                        addToSpacial(event.location.id);
                                    }
                                })
                            } else {
                                if(post.post.event.eventLocation.location.id) {
                                    addToSpacial(post.post.event.eventLocation.location.id);
                                }
                            }
                        }
                        return callback(null, post)
                    }
                })
            }
        }
    })
}


function updateEvent(eventBody, callback) {

    post.readComposition(eventBody.id, 'event' ,function (err, existingEvent) {
                if(err) {
                    return callback(err);
                } else {
                    if (existingEvent) {
                    //    console.log(existingEvent);
                        var updatedEvent = _.mergeWith(existingEvent, eventBody.event, customizer);
                   //     console.log(updatedEvent);
                        event.update(updatedEvent, function (err, afterUpdatedEvent) {
                            if (err) {
                                return callback(err);
                            } else {
                                return callback(null, afterUpdatedEvent);
                            }
                        });
                    }
                }
            });
}

function addToSpacial(id) {
    var params = {
        id: id
    }
    var cypher = "Match(l:location)  where ID(l) ={id} "
        + "with  l call spatial.addNode('drinkmates', l) yield node return l as location"

    seraphDB.query(cypher,params,function(err, spaitalLocation){
    })
}


function shareToFrnd(postId, FrndsList, isShared) {

    var params = {
        postId: postId,
        frndList: FrndsList,
        isShared: isShared
    }
    var cypher = "MATCH(p:post) where ID(p)= {postId} with p Match(u:user) where ID(u) in {frndList} with p,u " +
        "Merge (p)-[r:SHARED_TO]->(u)  SET r.isShared={isShared},r.created=timestamp() return u as user,p as post";

    console.log(FrndsList);

    seraphDB.query(cypher,params,function(err, shared){
        if(err) {
            console.log(err.message);
        }
    })

}

function shareToGrp(postId, groupList, isShared) {

    var params = {
        postId: postId,
        groupList: groupList,
        isShared: isShared
    }
    var cypher = "MATCH(p:post) where ID(p)= {postId} with p Match(g:group) where ID(g) in {groupList} with p,g " +
        "Merge (p)-[r:SHARED_TO]->(g)  SET r.isShared={isShared}, r.created=timestamp() return g as group,p as post";

    console.log(groupList);

    seraphDB.query(cypher,params,function(err, shared){
        if(err) {
            console.log(err.message);
        }
    })

}


function coHost(postId, userIds, fromUser){

    var params = {
        postId: postId,
        userIds: userIds,
        
    }

    var cypher = "MATCH(p:post) where ID(p)={postId} with p Match(u:user) where ID(u) in {userIds} with p,u merge (p)<-[c:CO_HOST]-(u)"
                +" return u as user,p as post ,c as cohost"

         seraphDB.query(cypher,params,function(err, cohost){
                    if(err) {
                        console.log(err.message);
                    }else{

                        if(cohost.length > 0){
                      cohost.forEach(host => {
                          
                     
                            var notificationParams = {
                                notifyTo: host.user.id,
                                notifyFrom: fromUser.id,
                                activityRequired: true,
                                notiFyType: 'coHost',
                                notifyFromName: fromUser.name
                            }

                            console.log(notificationParams)

                            if(host.user && host.user.pushToken) {
                                notificationParams.pushToken = host.user.pushToken;
                                
                            }
            
                            notification.createNotification(notificationParams, function (err, notification) {
                                if (err) {
                                    console.log(err.message);;
                                } else{
            
                                }
                            });
                        });
                            console.log(null, cohost[0]);
                            //return callback(null, cohost[0]);
                        }
                    
                        else {
                            
                            console.log(new errors.ValidationError(
                                "user doesn't exist."));
                        }

                    }  
                    
                })
            

}

function customizer(objValue, srcValue) {

    //console.log(typeof objValue[0]);

    if (_.isArray(objValue) &&  objValue.length > 0 && (typeof objValue[0] == 'object')) {
       return  _.map(objValue, function(item) {
            return _.merge(item, _.find(srcValue, { 'id' : item.id }));
        });
    }
}

function getNearByEvent(type,latitude, longitude, distance, callback) {
    
    var params = {
        type : type,
        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
        distance: parseFloat(distance)
    }
    var cypher;
    if(type == 'user') {
        cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude},longitude:{longitude}},{distance}) yield distance as d, " +
            "node as location  with d, location MATCH (post)-[:POST_GEO_LOCATION]->(location{type: 'user_post'}) WITH d, post , " +
            "location MATCH (post)<-[:POST_CREATED_BY]-(u:user) WITH u, d, post , location MATCH (post)-[:HAS_EVENT]->(event)-[:PROPOSED_LOCATION]-> " +
            "(eventLocation) with d as distance, post, event, collect(eventLocation) as location, u as user optional match " +
            "(event)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with distance, post, event, location, user, count(interestedUser) " +
            "as interested optional match(post)<-[comms:COMMENTED]-(comm:user) with distance, post, event, location, user, comms,comm," +
            " interested  optional match(post)<-[coHost:CO_HOST]-(hostUser:user)  with  distance, post, event,location,user, comms,comm, " +
            "hostUser, interested  optional match(post)<-[liks:LIKE]-(likeUsers:user)   return post{.*, id:ID(post), " +
            "createdBy:  user{id:ID(user), .name, .imageName, .mobileNo}, event:event{.*, eventLocation: location, interested: interested}," +
            " comments:collect(comm {.name, comment: comms.comment, .imageName, created: comms.created})," +
            "likes:{avg:avg(liks.star),users:collect(likeUsers{.name, star: liks.star, .imageName, created: liks.created,id: ID(liks)})}, distance: distance, coHost: collect(hostUser{.name})} as post ORDER BY post.id desc"
    } else {
        cypher = "Call spatial.withinDistance('drinkmates',{latitude:{latitude},longitude:{longitude}},{distance}) yield distance as d, node as location  with d, location" +
            " MATCH (eventLocation)-[:EVENT_GEO_LOCATION]->(location{type: 'event'}) WITH d,eventLocation, location " +
            "MATCH (eventLocation)<-[:PROPOSED_LOCATION]-(event)<-[:HAS_EVENT]- (post) WITH d as distance,event, post  " +
            "match (el:eventLocation)<-[:PROPOSED_LOCATION]-(event) with distance,event, post, collect(el) as location  " +
            "MATCH (post)<-[:POST_CREATED_BY]-(u:user) with  distance, post, event, location, u as user optional " +
            "match (event)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with distance, post, event, location, user, " +
            "count(interestedUser) as interested optional match(post)<-[comms:COMMENTED]-(comm:user) with distance, post, " +
            "event, location, user, comms,comm, interested  optional match(post)<-[coHost:CO_HOST]-(hostUser:user)  " +
            "with  distance, post, event,location,user, comms,comm, hostUser, interested  optional match(post)<-[liks:LIKE]-(likeUsers:user)   " +
            "return post{.*, id:ID(post), createdBy:  user{id:ID(user), .name, .imageName, .mobileNo}, event:event{.*, eventLocation: location, " +
            "interested: interested}, comments:collect(comm {.name, comment: comms.comment, .imageName, created: comms.created}),likes:{avg:avg(liks.star),users:collect(likeUsers{.name, star: liks.star, .imageName, created: liks.created,id: ID(liks)})}," +
            " distance: distance, coHost: collect(hostUser{.name})} as post ORDER BY post.id desc";
    }

    seraphDB.query(cypher,params,function(err, nearByUsers) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, nearByUsers);
        }
    });

}



function commentPost(postId, userId, comment, callback){

    var params = {
        postId: postId,
        userId: userId,
        comment: comment
    }

    var cypher = "MATCH(p:post) where ID(p)= {postId} with p Match(u:user) where ID(u) = {userId} with p,u " +
                 "optional match(u)-[:HAS_PROFILOMETER]->(po:profileOMeter) with p, u ,po merge (p)<-[c:COMMENTED{comment:{comment}, created: timestamp()}]-(u) " +
                 "return u as user,p as post,c as comment,po as profileoMeter"

                seraphDB.query(cypher,params,function(err, PostComment) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, PostComment);
                    }
                });            
}


function updatecommentPost(postId, userId,commentId, comment, callback){

    var params = {
        postId: postId,
        userId: userId,
        commentId: commentId,
        comment: comment
    }

    var cypher = "MATCH(u:user) where ID(u)= {userId} with u match(u)-[:COMMENTED]->(p:post) where ID(p)={postId} with u,p " +
                 " match(u)-[comm:COMMENTED]->(p) where ID(comm)={commentId} SET comm.comment={comment} " +
                 " return u as user,p as post,comm as comment"

                seraphDB.query(cypher,params,function(err, PostComment) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, PostComment);
                    }
                });
}


function deletecommentPost(postId, userId,commentId,  callback){

    var params = {
        postId: parseInt(postId), 
        userId: parseInt(userId),
        commentId: parseInt(commentId)

    }

    var cypher = "MATCH(u:user) where ID(u)={userId} with u match(u)-[:COMMENTED]->(p:post) where ID(p)={postId} with u,p " +
                 " match(u)-[r:COMMENTED]->(p)  where ID(r)={commentId} delete r"

                seraphDB.query(cypher,params,function(err, PostComment) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, PostComment);
                    }
                });
}




// function likesPost(postId, userId, star, callback){

//     var params = {
//         postId: postId,
//         userId: userId,
//         star: star
//     }

//     var cypher = "MATCH(p:post) where ID(p)= {postId} with p Match(u:user) where ID(u) = {userId} with p,u  " +
//                  "merge (p)<-[liks:LIKE{star:{star}}]-(u) set liks.created = timestamp()" +
//                  "return p{.*,avg:avg(liks.star),users:collect(u{.name, .mobileNo, star: liks.star, .imageName, created: liks.created,id: ID(liks), createdId: ID(u)})} as likes";

//                 seraphDB.query(cypher,params,function(err, likesPost) {
//                     if (err) {
//                         return callback(err);
//                     } else {
//                         return callback(null, likesPost);
//                     }
//                 });
// }


function likesPost(postId, userId, star, callback){

    var params = {
        postId: postId,
        userId: userId,
        star: star,
        //createdPostUser: createdPostUser
    }


    // var cypher = "MATCH(p:post) where ID(p)= {postId} with p Match(u:user) where ID(u) = {userId} with p,u  " +
    //              "merge (p)<-[liks:LIKE{star:{star}, created: timestamp()}]-(u) " +
    //              "return p{.*,avg:avg(liks.star),users:collect(u{.name, .mobileNo, star: liks.star, .imageName, created: liks.created,id: ID(liks), createdId: ID(u)})} as likes";

    var cypher = "MATCH(fromUser:user)-[:POST_CREATED_BY]->(p:post) where ID(p)= {postId} with fromUser ,p Match(likeByUser:user) where ID(likeByUser) = {userId} with p,likeByUser, fromUser " +
                 "merge (p)<-[liks:LIKE]-(likeByUser) SET liks.star = {star}, liks.created = timestamp()" +
                 "return p{.*,postCreatedUser: fromUser.mobileNo,avg:avg(liks.star),users:collect(likeByUser{.name, .mobileNo, star: liks.star, .imageName, created: liks.created,id: ID(liks), createdId: ID(likeByUser)})} as likes";

                seraphDB.query(cypher,params,function(err, likesPost) {
                    if (err) {
                        return callback(err);
                    } else {
                        console.log(likesPost)
                        var toMobileNo = likesPost[0].postCreatedUser;
                        var fromMobileNo = likesPost[0].users[0].mobileNo;
                        var props = {
                            ratingReceived:star,
                            ratingByMobileNo: fromMobileNo,
                            ratingToMobileNo: toMobileNo,
                            previousRating: null
                        }
                        profileOMeter.calculateProfilometer(props, function(err, like){
                            return callback(null, likesPost);
                        });
                    }
                });
}


function eventInterested(eventId, userId, input, callback){

    var params = {
        eventId: eventId,
        userId: userId,
        input: input
    }

    var cypher = "MATCH(e:event) where ID(e)={eventId} with e Match(u:user) where ID(u) ={userId} with e,u " +
                 "merge (e)<-[i:INTERESTED{input:{input}, created: timestamp()}]-(u)"
                +" return u as user,e as event ,i as interested"

                seraphDB.query(cypher,params,function(err, interested) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, interested);
                    }
                });
}


function eventlocationInterested(eventlocationId, userId, input, callback){

    var params = {
        eventlocationId: eventlocationId,
        userId: userId,
        input: input
    }

    var cypher = "MATCH(e:eventLocation) where ID(e)={eventlocationId} with e Match(u:user) where ID(u) ={userId} with e,u " +
                 "merge (e)<-[i:INTERESTED{input:{input}, created: timestamp()}]-(u)"
                +" return u as user,e as eventLocation ,i as interested"

                seraphDB.query(cypher,params,function(err, interested) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, interested);
                    }
                });
}



function getEventbyPostId(postId, callback){

    var params = {
        postId: parseInt(postId)
    }

     console.log(postId)
    var cypher = "MATCH (p:post)-[:HAS_EVENT]-(e:event)-[:PROPOSED_LOCATION]->(el:eventLocation)-[:EVENT_GEO_LOCATION]->(l:location{type: 'event'}) " +
                "where ID(p)={postId}  with p,e,el,l {.*, geoLoc:l} match (p)<-[:POST_CREATED_BY]-(u) with p as post, e as event, " +
                 "collect(el) as location, l,u as user optional match(post)-[comm:COMMENTED]-(users:user)-[:HAS_PROFILE]-(p:profile) " +
                 " with  user,post,location, l, event ,collect(users {.name, comment: comm.comment, image: p.imageName, created: comm.created}) as comments " +
                 "optional match(post)<-[liks:LIKE]-(likeUsers:user)-[:HAS_PROFILE]->(pr:profile)" +
                  "return user,post,event,location,l,comments , collect(likeUsers {.name, star: liks.star, image: pr.imageName, created: liks.created}) as likes"



         seraphDB.query(cypher,params,function(err, eventPlace) {
                    if (err) {
                        return callback(err);
                    } else {
                        return callback(null, eventPlace);

                    }
                });
}

function getMyEvent(userId, callback){

    var params = {
        userId: parseInt(userId)
    }

    var cypher = "match(u:user) where ID(u) =  {userId}  with u MATCH (post{type:'event'})<-[:POST_CREATED_BY]-(u) WITH  post, u " +
        "MATCH (post)-[:HAS_EVENT]->(event)-[:PROPOSED_LOCATION]-> (eventLocation) with post, event, collect(eventLocation) as location, " +
        "u as user optional match (event)<-[i:INTERESTED{input:'yes'}]-(interestedUser:user) with  post, event, location, user, " +
        "count(interestedUser) as interested optional match(post)<-[comms:COMMENTED]-(comm:user) with  post, event, location, " +
        "user, comms,comm, interested  optional match(post)<-[coHost:CO_HOST]-(hostUser:user)  with  post, event,location,user, " +
        "comms,comm, hostUser, interested  optional match(post)<-[liks:LIKE]-(likeUsers:user)   return post{.*, id:ID(post), " +
        "createdBy:  user{id:ID(user), .name, .imageName, .mobileNo}, event:event{.*, eventLocation: location, interested: interested}, " +
        "comments:collect(comm {.name, comment: comms.comment, .imageName, created: comms.created}), " +
        "likes:{avg:avg(liks.star),users:count(likeUsers)}, coHost: collect(hostUser{.name})} as post ORDER BY post.id desc";

    seraphDB.query(cypher,params,function(err, events) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, events);

        }
    });
}

function getVendorEvent(id, callback){
    vendor.read(id,
     function(err, vendorStatus){
        if(err){
            return callback(err)
        }else{
            return callback(null, vendorStatus)
        }
    })
}


function getVendorEventTotal(vendorId, callback){

    var params = {
        vendorId: parseInt(vendorId)
    }

    var cypher = "Match(v:vendor) where ID(v)= {vendorId} with v match(v)-[:POST_CREATED_BY]->(p:post{type:'vendor_event'}) with v,p " +
                 "match(p)-[:HAS_EVENT]->(e:event) return count(e) as vendorTotalEvent"

    seraphDB.query(cypher, params, function(err, vendorEventTotal){
        if(err){
            return callback(err);
        }else{
            return callback(null, vendorEventTotal);
        }
    })             
}

module.exports = {

    createEvent: createEvent,
    createdVendorEvent: createdVendorEvent,
    updateEvent:updateEvent,
    getNearByEvent: getNearByEvent,
    commentPost: commentPost,
    likesPost: likesPost,
    eventInterested: eventInterested,
    eventlocationInterested: eventlocationInterested,
    getEventbyPostId: getEventbyPostId,
    getMyEvent: getMyEvent,
    addToSpacial: addToSpacial,
    shareToFrnd: shareToFrnd,
    shareToGrp: shareToGrp,
    deletecommentPost: deletecommentPost,
    updatecommentPost:  updatecommentPost,
    getVendorEvent: getVendorEvent,
    getVendorEventTotal: getVendorEventTotal


}