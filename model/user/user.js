/**
 * Created by sumitgabhane on 6/21/17.
 */


var seraphDB = require('../../utils/db').seraphDB;
var errors = require('./../errors');
const  schema = require("../post/schema");
var validator = require("validator");
var config = require("../../utils/config");
var defer = require("node-promise").defer;
var model = require('seraph-model');
var unirest = require("unirest");
var profile_ = require('./profile');
var _ = require("lodash");

var userDB = model(seraphDB, 'user');
var profile = model(seraphDB, 'profile');
var token = model(seraphDB, 'token');
var location = model(seraphDB, 'location');
var profileOMeter = model(seraphDB,'profileOMeter');

userDB.schema = schema.user;
profile.schema = schema.profile;
token.schema = schema.token;
location.schema = schema.location;
profileOMeter.schema = schema.profileOMeter;

userDB.compose(profileOMeter, 'profileOMeter', 'HAS_PROFILOMETER');

var User = module.exports  = function User(_user) {
    this._user = _user;
}


Object.defineProperty(User.prototype, 'userId', {
    get: function () { return this._user.id; }
});


User.createorUpdate = function (props, callback) {

   // userDB.setUniqueKey("mobileNo");

    if (!validator.isMobilePhone(props.countryCode + props.mobileNo, 'en-IN')) {
       return callback(new errors.ValidationError(
            props.mobileNo + ' this mobile no is not valid mobile no.'));
    }

    userDB.where({mobileNo: props.mobileNo},{varName : "user"},
        function (err, user) {
            if (err)  return callback(err);

       if(user.length > 0) {
          return callback(new errors.ValidationError(
               props.mobileNo + ' this mobile no is already exists.'));
       } else {
           sendOtp(props.mobileNo).then(function (res) {
               if(res.Status === "Success") {
                   props.otpSession =  res.Details;
                   props.profileOMeter = {
                       overallRating:0,
                       fiveStar:0,
                       fourStar:0,
                       threeStar:0,
                       twoStar:0,
                       oneStar:0
                   }
                   userDB.save(props, function (err, user) {
                       if (err)  {
                           return callback(err);
                       } else {
                           var user = new User(user);
                           callback(null, user);
                       }
                   });
               } else {
                return callback(new errors.ValidationError(
                       props.mobileNo + ' this mobile no is not in a service'));
               }
            }).then(function (err) {
               if(err)  return callback(err);

           });
       }
    });
}


User.update = function (props, callback) {
    userDB.read(props.id,  function (err, usr) {

        if (err) {
            return callback(err);
        } else {
            if (usr) {
                var updatedUser = _.assign({}, usr, props);
                userDB.update(updatedUser, function (err, user) {
                    if (err)  return callback(err);
                    var user = new User(user);
                    console.log(JSON.stringify(user));
                    return callback(null, user);
                });

            }
        }
    });
}


    User.get = function (id, callback) {  
    userDB.read(id, function (err, user) {  
        if (err)  return callback(err);  
        var user = new User(user[0]); 
        callback(null, user); 
    }); 
    }   

    User.getbyMobileNo = function (mobileNo, callback) {  
    userDB.where({mobileNo: mobileNo},{varName : "user"},
        function (err, user) {  
        if (err)  return callback(err);  
        var user = new User(user[0]); 
        callback(null, user); 


    }); 
    }   

    User.validateUserOtp = function( props, callback) {  

        userDB.where({mobileNo: props.mobileNo},{varName : "user"},
            function (err, users) {

                if (err) {
                    return callback(err);
                    //     console.log(JSON.stringify(users[0]));
                } else {
                    if(users.length > 0) {
                        validateOtpNo(props.otpNo, users[0].otpSession).then(function (res) {
                            if (res.Status === "Success") {
                                users[0]['status'] = res.Status;
                                users[0]['verified'] = true;

                                try {
                                    userDB.update(users[0], function (err, updated_user) {


                                        console.log(JSON.stringify(err));
                                        if (err) {
                                            return callback(err);
                                        } else {
                                            profile_.getProfile(props.mobileNo, function (err, get_profile) {
                                                if (err) {
                                                    return callback(err);
                                                } else {
                                                    if(get_profile) {
                                                        return callback(null, get_profile);
                                                    } else {
                                                        return callback(null, updated_user);
                                                    }
                                                }

                                            });

                                        }
                                    });
                                } catch (err) {
                                    console.log(err);
                                    throw err;
                                }

                                //  return callback(null, users[0]);

                            } else {
                                console.log(res);
                                return callback(new errors.ValidationError(res.Details));
                            }
                        }, function (err) {
                            // console.log(err);
                            return callback(err);

                        });
                    } else {
                        return callback(new errors.ValidationError(
                            props.mobileNo + ' this mobile no is already exists.'));
                    }
                }
            });
    }

 User.authenticate = function(username, password, cb) {

     userDB.where({mobileNo: username},{varName : "user"},
         function (err, user) {
             if (err)  {
                 cb(err, false);
             } else {
                 user.verified = true;
                 cb(null, user[0]);
             }

         });
 }


 User.sendOtp = function( MobileNo , callback) {
    userDB.where({mobileNo: MobileNo},{varName : "user"}, function (err, users) {
        if(err) {
            return callback(err);
        } else {
            if(users.length > 0) {
                var user_ = users[0];
                sendOtp(user_.mobileNo).then(function (res) {
                    if (res.Status === "Success") {
                        user_.otpSession = res.Details;
                        user_.verified = false;

                        userDB.save(user_, function (err, updateusers) {

                            if (err) {
                                return callback(err);
                            }
                            return callback(null, updateusers);
                        });
                    } else {
                        return callback(new errors.ValidationError(
                            "Mobile no is incorrect or not in service"));
                    }
                },function (err) {
                    //  console.log(err.message);
                    return callback(err);

                });
            }
        }
        });

 };

    function sendOtp(mobileNo) {

        var deferred =  defer();
        var req = unirest("GET", config.otpApiUrl+config.OtpApiKey+ "/SMS/"+mobileNo+ "/AUTOGEN");

        req.headers({
            "content-type": "application/x-www-form-urlencoded"
        });

        req.form({});

        req.end(function (res) {
            console.log(JSON.stringify(res));
            if (res.error) {
                console.log(JSON.stringify(res.error));
                deferred.reject(new Error(res.body.Details));
                //   return;
            } else {
                deferred.resolve(res.body);
            }
        });
        return deferred.promise;

}


function validateOtpNo(otpNo, sessionCode) {


        var deferred =   defer();
        var req = unirest("GET", config.otpApiUrl+config.OtpApiKey + "/SMS/VERIFY/" + sessionCode + "/" + otpNo);

        req.headers({
            "content-type": "application/x-www-form-urlencoded"
        });

        req.form({});

        req.end(function (res) {

            console.log(JSON.stringify(res));
            if (res.error) {
                console.log(JSON.stringify(res.error));
                deferred.reject(new Error(res.body.Details));
             //   return;
            } else {
                deferred.resolve(res.body);
            }
          //  if (res.error) return reject(res.body);


          //  return resolve(res.body)

        });


    return deferred.promise;

}

User.profileCreate = function(value, callback){
    
         if (!validator.isDecimal(value.latitude,value.langitude)) {
           return callback(new errors.ValidationError(
                value.latitude +'Enter Decimal format only .'));
        }
            profile.where({dob: value.dob} ,{ varName: "profile"},function (err, users) {
            if (err)  return callback(err);
            
           if(users.length > 0) {
              return callback(new errors.ValidationError(
                   value.dob + ' this ID is already exists.'));
           }else
               profile.save(value, function(err, profile){
    
                    if (err) return callback(err);
    
                     var profile = new User(profile);
                        callback(null, profile);
               
                })
    });
    
    
}

User.getNotification = function(userId, callback){

    var params = {
        userId: parseInt(userId)
    }

    var cypher = "match (u:user) where ID(u) = {userId} with u match (u)<-[r:NOTIFY]-(fromUser:user) " +
                 "return r{.*,  fromName: fromUser.name, imageName: fromUser.imageName, fromId: ID(fromUser)  } ORDER BY r.created desc"

    seraphDB.query(cypher, params, function(err, userNotification){
        if(err){
            return callback(err)
        }else{
            return callback(null, userNotification)
        }
    })
},

seraphDB.constraints.uniqueness.create('user', 'mobileNo', function(err, constraint) {

 });