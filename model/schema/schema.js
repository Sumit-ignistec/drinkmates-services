


/**
 * Created by sumitgabhane on 7/1/17.
 */


const user = {
    name: { type: String, required: true },
    mobileNo: { type: String, required: true },
    countryCode: { type: String},
    keySecret:{type:String},
    pushToken:{type:String}

}


 const token = {
     accessToken: { type: String},
     refreshToken : { type: String },
     lastloginTimestamp : { type: Number }
 }


 const profile = {
     gender : { type: String},
     dob : {type: String },
     city : {type: String },
     imageName : {type : String},
     profileHeadine : {type : String},
     aboutUs : {type : String},
     profession: {type : Array},
     languageKnown: {type : Array},
     drinkLikes: {type : Array},
     interest: {type : Array}
}

 const location = {
     latitude : { type:  Number},
     longitude : {type : Number},
     type: {type: String}
 }

 const events = {
    
   name:{type:String, required:true},
   description:{type:String, required:true},
   open:{type:String, required:true},
   close:{type:String, required:true},
   special:{type:String, required:true},
   private:{type:String, required:true},
   commerical:{type:String, required:true},
   entryFees:{type:Number, required:true},
   sharetoallfriend:{type:Boolean},
   sharetoNearBy:{type:Boolean},
   sharetoGroup:{type:Boolean},
   photo:{type : Array},
   //location:{type : Array}
}

const eventlocation = {
        city:{type:String, required:true},
        pincode:{type:Number, required:true},
        address1:{type:String, required:true},
        address2:{type:String, required:true},
        website:{type:String, required:true},
        name:{type:String, required:true},
        photo:{type:Array}
}

const albhum = {
    albhumName:{type:String, required:true}
}

const post = {
            type:{type:String, required:true},
            description:{type:String, required:true},
            sharetoallfriend:{type:Boolean},
            sharetoNearBy:{type:Boolean},
            sharetoGroup:{type:Boolean},
            //url:{type:String, required:true}, 

}

const content = {
    
        type:{type:String, required:true},
        content:{type:Array}
    
   }

const activity = {
    type:{type:String, required:true},
    name:{type:String, required:true},
    helpingVerbs:{type:String, required:true},
    icon:{type:String, required:true},

} 

const checkin = {
            name:{type:String, required:true},
            address:{type:String, required:true},
            city:{type:String, required:true},
            latitude : { type:  Number},
            longitude : {type : Number},
     }

  const  tagMates = {
                id:{type:Number}
    }   


 module.exports = {
     user: user,
     token: token,
     profile: profile,
     location: location,
     post : post,
     content: content,
     activity : activity,
     checkin : checkin,
     tagMates : tagMates


 }