


/**
 * Created by sumitgabhane on 7/1/17.
 */


const user = {
    name: { type: String},
    mobileNo: { type: String},
    countryCode: { type: String},
    keySecret:{type:String},
    pushToken:{type:String}

}

const profileOMeter = {
    overallRating:{type:Number},
    fiveStar:{type:Number},
    fourStar:{type:Number},
    threeStar:{type:Number},
    twoStar:{type:Number},
    oneStar:{type:Number}
} 

 const token = {
     accessToken: { type: String},
     refreshToken : { type: String },
     lastloginTimestamp : { type: Number }
 }


 const profile = {
     gender : { type: String},
     dob : {type: String },
     city : {type: String },
     imageName : {type : String},
     profileHeadine : {type : String},
     aboutUs : {type : String},
     profession: {type : Array},
     languageKnown: {type : Array},
     drinkLikes: {type : Array},
     interest: {type : Array}
}

 const location = {
     latitude : { type:  Number},
     longitude : {type : Number},
     type: {type: String}
 }

 const event = {
    
     name:{type:String, required:true},
     description:{type:String, required:true},
     eventDate: {type:String, required:true},
     eventTime: {type:String, required:true},
     eventStartTime:{type:String},
     eventEndTime:{type:String},
     category: {type:String, required:true},
     hostType: {type:String, required:true},
     personEntryFees:{type:Number},
     coupleEntryFees:{type:Number},
     minBudget:{type:Number},
     miniReqPerson:{type:Number},
     photo:{type : Array},
     eventAttraction:{type:String},
     join:{type:Boolean}
   //location:{type : Array}
}

const eventLocation = {
        city:{type:String, required:true},
        pincode:{type:String},
        address1:{type:String, required:true},
        address2:{type:String, required:true},
        website:{type:String, required:true},
        name:{type:String, required:true},
        photo:{type:Array},
        isFinalized: {type:Boolean},
        photo:{type : Array},
        rating: {type: Number}
}

const album = {
    name:{type:String, required:true}
}

const post = {
    type: {type: String, required: true},
    shareToAllfriend: {type: Boolean},
    shareToGroup: {type: Boolean},
    sharetoNearBy: {type: Boolean},
    description: {type: String},
    distancetoShare: {type: Number}
}

const content = {
        type:{type:String, required:true},
        content:{type:String}
    
   }

const activity = {
    type:{type:String, required:true},
    name:{type:String, required:true},
    helpingVerbs:{type:String, required:true},
    unicode:{type : String},

} 


 
  const community = {
      name:{type:String, required:true},
      description:{type:String, required:true},
      type:{type:String, required:true},
      icon:{type:String}
  }  

 module.exports = {
     user: user,
     profileOMeter: profileOMeter,
     token: token,
     profile: profile,
     location: location,
     post : post,
     content: content,
     activity : activity,
     community: community,
     event: event,
     eventLocation: eventLocation,
     album: album


 }