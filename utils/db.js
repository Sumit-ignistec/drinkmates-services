/**
 * Created by sumitgabhane on 6/25/17.
 */

config = require('./config');
url = require('url').parse(process.env['GRAPHENEDB_URL'] ||  config.DB.DB_URL);



var db = module.exports  = function DataBase() {


}

db.seraphDB = require("seraph")({
    server: url.protocol + '//' + url.host,
    user: url.auth.split(':')[0],
    pass: url.auth.split(':')[1]
});


db.neo4j =  require('neo4j').GraphDatabase({
    url: url,

});