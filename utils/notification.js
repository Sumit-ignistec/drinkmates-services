/**
 * Created by sumitgabhane on 3/4/18.
 */
var seraphDB = require('./db').seraphDB;
var errors = require('../model/errors');
var config = require("./config");
var notificationType = require("./notificationType.json");
var admin = require('firebase-admin');
var defer = require("node-promise").defer;
var unirest = require("unirest");

var serviceAccount = require('./serviceAccounKey.json');

//var user = require('./../model/post/schema');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://drinkmates-mobile.firebaseio.com/'
});



function createNotification(params, callback) {

    if(notificationType[params.notiFyType]) {

        var message = notificationType[params.notiFyType].body.replace("$notifyFromName$", params.notifyFromName);
        if(params.notiFyType == '') {
            message.replace("", "");
        }


        var queryParams = {
            notifyTo: params.notifyTo,
            notifyFrom: params.notifyFrom,
            activityRequired: params.activityRequired,
            notiFyType: params.notiFyType,
            message: message
        }

        var cypher;
        if(params.allFriend) {
            cypher = "match(notifyFrom:user) where ID(notifyFrom)= {notifyFrom} with notifyFrom match(notifyFrom)-[:REL_USER{status:'accepted'}]-(friends:user) with friends, notifyFrom"
                + " CREATE  (notifyFrom)-[r:NOTIFY]->(friends)  SET r.message={message}, r.type={notiFyType}, r.activityRequired = {activityRequired},r.created = timestamp()"
                + " RETURN r.message as message";
        } else {
             cypher = "match(notifyFrom:user) where ID(notifyFrom)= {notifyFrom} with notifyFrom match(notifyTo:user) where ID(notifyTo)= {notifyTo} with notifyTo, notifyFrom"
                + " CREATE  (notifyFrom)-[r:NOTIFY]->(notifyTo)  SET r.message={message}, r.type={notiFyType}, r.activityRequired = {activityRequired},r.created = timestamp()"
                + " RETURN r.message as message";
        }


        seraphDB.query(cypher, queryParams, function (err, result) {
            if (err) {
                return callback(err);
            } else {
                sendPushNotification(params);
                return callback(null, result);

            }
        });
    } else {
        return callback(new errors.ValidationError(
            "type doesn't exist"));
    }



}

function replaceKeys(key, value, type) {
    var message = notificationType[type].replace(key, value);
    if(params.notiFyType == '') {
        message.replace("", "");
    }
}


function sendPushNotification(params) {
    // var pushToken = "fy_3i40u8Sk:APA91bH4NVS-yK3TwldV1sJTPi5pcW7Qqb2Mw5afoCuk2HPmP0cvAhdd8Z-IZfqXnLelXAXfkVlBbdVb4x736XB65R7-" +
    //     "EtutS5qPKfY3HHpqx4fKkt7hJ0wBJhPeSK392lAuV8llxpx-"

    //  var pushToken = "f4jtbonkU74:APA91bGm6MIfeGnbP3cpvJV2pLQ2i_ImUwAH0bDZZhv3cTqoDWyS0vKDpfU8_QlClNfsBoqwXEnsoy9ZEtM2iDnY_zgBUji7mhCtAyc4Fyfc5n-" +
    //         "TZPYZImtKLCR1znR1LntRD7N6dElr"    

    //var pushToken = user.pushToken;

    if(notificationType[params.notiFyType] && params.pushToken) {

        var message = notificationType[params.notiFyType].body.replace("$notifyFromName$", params.notifyFromName);

        var payload = {
            notification: {
                title : notificationType[params.notiFyType].title,
                body: message,
                icon: '/res/drawable-xhdpi/banana.png'
            },
            data: {
                id: 'GOOG',
                type: params.notiFyType
            }
        };

// Set the message as high  have it expire after 24 hours.
        var options = {
            priority: 'high',
            timeToLive: 60 * 60 * 24
        };

        admin.messaging().sendToDevice(params.pushToken, payload, options)
            .then(function (response) {
                console.log('Successfully sent message:', response);
                console.log(response.results[0].error);
            })
            .catch(function (error) {
                console.log('Error sending message:', error);
            });
    }

}


// function createPushNotificationGroup(otpNo, sessionCode) {
//
//
//     var deferred =   defer();
//     var req = unirest("GET", config.otpApiUrl+config.OtpApiKey + "/SMS/VERIFY/" + sessionCode + "/" + otpNo);
//
//     req.headers({
//         "content-type": "application/x-www-form-urlencoded"
//     });
//
//     req.form({});
//
//     req.end(function (res) {
//
//         console.log(JSON.stringify(res));
//         if (res.error) {
//             console.log(JSON.stringify(res.error));
//             deferred.reject(new Error(res.body.Details));
//             //   return;
//         } else {
//             deferred.resolve(res.body);
//         }
//         //  if (res.error) return reject(res.body);
//
//
//         //  return resolve(res.body)
//
//     });

// https://android.googleapis.com/gcm/notification
//     Content-Type:application/json
// Authorization:key=API_KEY
// project_id:SENDER_ID
//
// {
//     "operation": "create",
//     "notification_key_name": "appUser-Chris",
//     "registration_ids": ["4", "8", "15", "16", "23", "42"]
// }
//
//
//     return deferred.promise;
//
// }

module.exports = {
    createNotification: createNotification
};