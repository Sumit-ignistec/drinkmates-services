const aws = require('aws-sdk');
const config = require('./config');
var fs = require('fs');
const sharp = require('sharp');

aws.config.update({
    region: config.REGION
});




function createAlbum(bucket, albumName, callback) {

    albumName = albumName.trim();

    const s3 = new aws.S3({
        params: {Bucket: bucket}
    });

    if (!albumName) {
        return callback('Album names must contain at least one non-space character.');
    }
    if (albumName.indexOf('/') !== -1) {
        return callback('Album names cannot contain slashes.');
    }
    var albumKey = encodeURIComponent(albumName) + '/';
    s3.headObject({Key: albumKey}, function(err, data) {
        if (!err) {
            return callback(null,"exists");
        }
        if (err.code !== 'NotFound') {
            return  err.message;
        }
        s3.putObject({Key: albumKey}, function(err, data) {
            if (err) {
                console.log(err.message);
                return  callback(err);
            } else {
            return callback(null, "sucess");
        }
        });
    });
}


function awsUpload(bucket, file, filename, callback) {

    const s3 = new aws.S3({
        params: {Bucket: bucket}
    });
    fs.readFile(file.path, function (err, data) {
        if (err) {
            return callback(err.message);
        }else {
            sharp(data)
                .resize(512, 512)
                .toFormat(sharp.format.webp)
                .toBuffer()
                .then( data =>
            s3.upload({
                Key: filename,
                Body: data,
                ACL: 'public-read',
                ContentType: file.mimetype
            }, function (err, data) {
                fs.unlink(file.path, function (err) {
                    if (err) {
                        console.error(err);
                    }
                    console.log('Temp File Delete');
                });
                if (err) {
                    console.log(err.message);
                    return  callback(err);
                } else {
                    return callback(null, data);
                }
            })).catch( err =>  {console.log(err.message);
            return  callback(err)});

        }
    });
}


function getAlbum(bucket, albumName, callback) {


    const s3 = new aws.S3({
        params: {Bucket: bucket}
    });

    s3.listObjects({Prefix: albumName}, function(err, data) {
        if (err) {
            return callback(err.message);
        } else {
            if(data.Contents && data.Contents.length > 0) {
                return callback(null, data.Contents.slice(1));
            } else {
                return callback(null, new Array());
            }

        }

    });

}

module.exports = {
    createAlbum: createAlbum,
    awsUpload: awsUpload,
    getAlbum: getAlbum
}