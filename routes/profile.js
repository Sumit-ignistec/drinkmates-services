var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');




var neo4j = require('neo4j');
var errors = require('../model/errors');
var profile = require('../model/user/profile');
const jwt = require('jsonwebtoken');

const passport = require('passport');
const Strategy = require('passport-local');
const config = require('../utils/config');
var aws = require('../utils/aws-upload');


// const authenticate = expressJwt({
//     secret: config.secret
// });
//
//
// passport.use(new Strategy(
//     function(username, password, done) {
//         User.authenticate(username, password, done);
//     }
// ));





router.post('/', function(req, res, next) {
 
    profile.createOrUpdateProfile(req.body, function (err, profile) {
        if (err) {
            return next(err);
        }
        res.send(profile);
    });
});


router.put('/', function(req, res, next) {

    profile.createOrUpdateProfile(req.body, function (err, profile) {
        if (err) {
            return next(err);
        }
        res.send(profile);
    });
});


router.get('/mobileNo/:mobileNo', function(req, res, next) {

    profile.getProfile(req.params.mobileNo, function (err, profile) {
        if (err) { 
            return next(err);
        }
        res.send(profile);
    });
}); 

router.get('/cities', function(req, res, next) {
    profile.getAllCities(req.query.text, function (err, cities) {
        if (err) {
            return next(err);
        }
        res.send(cities);
    }); 


});

router.post('/location', function(req, res, next) {

    profile.createOrUpdateLocation(req.body, function (err, location) {
        if (err) {
            return next(err);
        }
        res.send(location);
    });
});


router.put('/location', function(req, res, next) {

    profile.createOrUpdateLocation(req.body, function (err, location) {
        if (err) {
            return next(err);
        }
        res.send(location);
    });
});


router.get('/location/:mobileNo', function(req, res, next) {

    profile.getLocation(req.params.mobileNo, function (err, location) {
        if (err) {
            return next(err);
        }
        res.send(location);
    });
});



var storage = multer.diskStorage({

    destination: function(req, file, callback) {

        var filePath = './public/images/';
        callback(null, filePath)
    },

    filename: function(req, file, callback) {
        var fileName = new Date().getTime().toString()+"_avatar"+path.extname(file.originalname);
        callback(null,  fileName);
    }


})


router.post('/uploadprofilepic/:mobileNo', function(req, res, next) {

    console.log(req.params.mobileNo);
    var upload = multer({
        storage: storage
    }).array('filename',1);
    upload(req, res, function(err, file) {

        if(req.params.mobileNo) {
            aws.createAlbum(config.BUCKET_NAME+'/images', req.params.mobileNo, function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    aws.createAlbum(config.BUCKET_NAME+'/images/'+req.params.mobileNo, 'avatar', function(err, data) {
                        if (err) {
                            return next(err);
                        } else {
                            aws.awsUpload(config.BUCKET_NAME+'/images/'+req.params.mobileNo+'/avatar',
                                req.files[0], req.files[0].filename, function (err, data) {
                                if(err) {
                                   return next(err);
                                } else {

                                    var userProfile = {
                                        mobileNo: req.params.mobileNo,
                                        imageName: req.files[0].filename,
                                        profile: {
                                                 imageName: req.files[0].filename
                                        }
                                    }

                                    profile.createOrUpdateProfile(userProfile, function (err, profile) {
                                        if (err) {
                                            return next(err);
                                        }
                                        res.send(profile);
                                    });


                                }

                            })
                        }
                    });
                }
            })
        }
    })
});


router.get('/album/:mobileNo', function(req, res, next) {

    if(req.params.mobileNo) {
        aws.getAlbum(config.BUCKET_NAME, 'images/'+req.params.mobileNo+ '/avatar/', function (err, data) {
            if (err) {
                return next(err);
            } else {
                res.send(data);
            }


        });
    }

})

router.get('/search/:fromMobileNo', function(req, res, next) {

    profile.userSearch(req.params.fromMobileNo, req.query.toMobileNo, function(err, search){
        if(err){
            return next(err)
        }
            res.send(search)
        
    })

})



function serialize(req, res, next) {
    User.update(req.user, function(err, user) {
        if (err) {
            return next(err);
        }
        req.user = {
            id: user._user.id
        };
        next();
    });
}

function generateToken(req, res, next) {
    req.token = jwt.sign({
        id: req.user.id,
    }, config.secret, {
        expiresIn: config.tokenTime
    });
    next();
}

function respond(req, res) {
    res.status(200).json({
        user: req.user,
        token: req.token
    });
}




module.exports = router;
