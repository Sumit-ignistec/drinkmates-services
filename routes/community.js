var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
var aws = require('../utils/aws-upload');

var community = require('../model/user/community')

var neo4j = require('neo4j');
var errors = require('../model/errors');
const jwt = require('jsonwebtoken');

const passport = require('passport');
const Strategy = require('passport-local');
const config = require('../utils/config')


router.post('/', function(req,res,next){
    
    community.createGroup(req.body,function(err, group){
            if(err){
                    return next(err);
            } else{
                res.send(group);
            }

        })
    });

router.post('/update', function(req,res,next){

    community.updateGroup(req.body,function(err, group){
        if(err){
            return next(err);
        } else {
            res.send(group);
        }

    })
});


router.post('/addMember', function(req,res,next){
        
    community.addMember(req.body.mobileNos, req.body.groupID, function(err, tagMember){
                if(err){
                        return next(err);
                } else {
                    res.send(tagMember);
                }

            })
 });  

 router.post('/removeMember', function(req,res,next){
    
    community.removeMember(req.body.mobileNos, req.body.groupID, function(err, untagMember){
            if(err){
                    return next(err);
            } else {
                res.send(untagMember);
            }

        })
    });  

 router.get('/:groupID', function(req,res,next){
        
    community.getGroupAndMember(req.params.groupID, function(err, groupMember){
                if(err){
                        return next(err);
                } else {
                    res.send(groupMember);
                }

            })
 });  
      
 router.get('/getGroupList/:mobileNo', function(req,res,next){
    
    community.getGroupList(req.params.mobileNo, function(err, memberList){
            if(err){
                    return next(err);
            } else {
                res.send(memberList);
            }

        })
});  
   

  router.get('/nearbygroups/:latitude/:longitude/:distance', function(req,res,next){

    community.nearByGroups(req.params.latitude, req.params.longitude, req.params.distance, function(err ,nearGroups){
            if(err){
                return next(err);
            } else{
                res.send(nearGroups);
            }
        })
  })  

   var storage = multer.diskStorage({
    
        destination: function(req, file, callback) {
    
            var filePath = './public/images/'
            callback(null, filePath)
        },
    
        filename: function(req, file, callback) {
            var fileName = new Date().getTime().toString()+"_group"+path.extname(file.originalname);
            callback(null,  fileName);
        }
    
    
    })
    
    
router.post('/uploadgrouppic/:mobileNo', function(req, res, next) {
    
        console.log(req.params.mobileNo);
        var upload = multer({
            storage: storage
        }).array('filename',1);
        upload(req, res, function(err, file) {
    
            if(req.params.mobileNo) {
                aws.createAlbum(config.BUCKET_NAME+'/images', req.params.mobileNo, function(err, data) {
                    if (err) {
                        return next(err);
                    } else {
                        aws.createAlbum(config.BUCKET_NAME+'/images/'+req.params.mobileNo, 'group', function(err, data) {
                            if (err) {
                                return next(err);
                            } else {
                                aws.createAlbum(config.BUCKET_NAME+'/images/'+req.params.mobileNo+ '/group', req.body.groupID, function(err, data) {
                                    if (err) {
                                        return next(err);
                                    } else {
                                        aws.awsUpload(config.BUCKET_NAME + '/images/' + req.params.mobileNo + '/group/' +  req.body.groupID,
                                            req.files[0], req.files[0].filename, function (err, data) {
                                                if (err) {
                                                    return next(err);
                                                } else {
                                                    var updatedGroup = {
                                                        icon: req.files[0].filename,
                                                        id: req.body.groupID

                                                    }


                                                    community.updateGroup(updatedGroup, function (err, group) {
                                                        if (err) {
                                                            return next(err);
                                                        } else {
                                                            res.send(group);
                                                        }
                                                    });


                                                }

                                            })
                                    }
                                });
                        }
                        });
                    }
                })
            }
        })
    });
    


router.get('/delete/:groupID', function(req,res,next){
    
    community.deleteGroup(req.params.groupID, function(err ,deleteGroups){
            if(err){
                return next(err);
            } else{
                res.send(deleteGroups);
            }
        })
  })  

router.get('/post/:groupID', function(req, res, next){

    community.getmyGroupPost(req.params.groupID, function(err, groupPost){
        if(err){
            return next(err)
        }
        res.send(groupPost)
    })
})

router.get('/my/:userId', function(req, res, next){

    community.getownGroup(req.params.userId, function(err, groupPost){
        if(err){
            return next(err)
        }
        res.send(groupPost)
    })
})

module.exports = router;