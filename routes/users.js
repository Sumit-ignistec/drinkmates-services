var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');





var neo4j = require('neo4j');
var errors = require('../model/errors');
var User = require('../model/user/user');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const logger = require('morgan');
const passport = require('passport');
const Strategy = require('passport-local');
const config = require('../utils/config')


var seraphDB = require('../utils/db').seraphDB;
var model = require('seraph-model');
var profile = model(seraphDB, 'Profile');

const authenticate = expressJwt({
    secret: config.secret
});


passport.use(new Strategy(
    function(username, password, done) {
        User.authenticate(username, password, done);
    }
));
// const authenticate = expressJwt({
//     secret: config.secret
// });
//
//
// passport.use(new Strategy(
//     function(username, password, done) {
//         User.authenticate(username, password, done);
//     }
// ));


router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});



router.post('/', function(req, res, next) {


    User.createorUpdate(req.body, function (err, users) {
        if (err) {
           return next(err);
        }
        res.send(users._user);
    });
}); 

router.post('/update', function(req, res, next) {


    User.update(req.body, function (err, users) {
        if (err) {
           return next(err);
        }
        res.send(users);
    });
});


router.get('/:id', function(req, res, next) {
    User.get(req.params.id, function (err, users) {


        if (err) {
            return next(err);

        }

        res.send(users._user);

    });
});

router.get('/mobile/:mobileNo', function(req, res, next) {
    User.getbyMobileNo(req.params.mobileNo, function (err, users) {


        if (err) {
            return next(err);

        }

        res.send(users._user);
    });
});


router.get('/sendotp/:mobileNo', function(req, res, next) {

    User.sendOtp(req.params.mobileNo, function (err, users) {
        if (err) {
            return next(err);
        } else{
            res.send(users);
        }

    });
});


router.get('/verifyotp/:mobileNo/:otpNo', function(req, res, next) {
    User.validateUserOtp(req.params, function (err, users) {
        if (err) {
            return next(err);
        } else {
            res.send(users);
        }
    });
});


router.get('/notification/:userId', function(req, res, next){
    User.getNotification(req.params.userId, function(err, userNotify){
        if(err){
            return next(err)
        }
        res.send(userNotify)
    })
})



router.post('/profile/:id', function(req, res, next) {
    res.send('respond with a resource');
});




// router.post('/auth', passport.initialize(),
//     passport.authenticate(
//     'local', {
//         session: false,
//         scope: []
//     }), serialize, generateToken, respond);




router.get('/me', function(req, res) {
    res.status(200).json(req.user);
});




function serialize(req, res, next) {
    User.update(req.user, function(err, user) {
        if (err) {
            return next(err);
        }
            req.user = {
            id: user._user.id
        };
        next();
    });
}

function generateToken(req, res, next) {
    req.token = jwt.sign({
        id: req.user.id,
    }, config.secret, {
        expiresIn: config.tokenTime
    });
    next();
}

function respond(req, res) {
    res.status(200).json({
        user: req.user,
        token: req.token
    });
}

module.exports = router;
