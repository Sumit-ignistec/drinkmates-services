var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
 
var neo4j = require('neo4j');
var errors = require('../model/errors');
var ProfiloMeter = require('../model/user/profilometer');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const logger = require('morgan');
const Strategy = require('passport-local');
const config = require('../utils/config')

var seraphDB = require('../utils/db').seraphDB;
var model = require('seraph-model');
var profile = model(seraphDB, 'Profile'); 

 
router.get('/', function(req, res, next) {
    ProfiloMeter.calculateProfilometer(req.query, function (err, users) {
        if (err) { 
           return next(err);
        }
        res.send(users.profileOMeter);
    }); 
});  

router.get('/getUserProfileOMeter',function(req,res,next){
    ProfiloMeter.getUserProfileOMeter(req.query, function (err, users) {
        if (err) {
           return next(err);
        }
        res.send(users);
    });
});

module.exports = router;