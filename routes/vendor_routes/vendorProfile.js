var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
var aws = require('../../utils/aws-upload');
var defer = require("node-promise").defer;
var all = require("node-promise").all;
var _ = require("lodash");


var vendorProfile = require('../../model/vendor/vendorProfile');

router.get('/', function(req, res, next) {
    res.send('respond with a resource');
  });

router.post('/create', function(req, res, next){

    vendorProfile.createVendorProfile(req.body, function(err, vendorProfiles){
        if(err) {
            return next(err)
        }

        res.send(vendorProfiles)
    })
});

router.put('/update', function(req, res, next){

    vendorProfile.updateVendorProfile(req.body, function(err, vendorProfiles){
        if(err) {
            return next(err)
        }

        res.send(vendorProfiles)
    })
});

var storage = multer.diskStorage({

    destination: function(req, file, callback) {

        var filePath = './public/images/';
        callback(null, filePath)
    },

    filename: function(req, file, callback) {
        var fileName = new Date().getTime().toString()+"_vendor"+path.extname(file.originalname);
        callback(null,  fileName);
    }


})




router.post('/upload/:type/:id', function(req, res, next) {

    console.log(req.params.id);
    var upload = multer({
        storage: storage
    }).array( "filename", 10);
    upload(req, res, function(err, file) {
        let fileNames = [];
        if(req.params.id) {
            uploadPics(req.files, req.params.id, function(err, files)  {
                let fileArray = files.map(file => {
                    return  {
                        name: file.name,
                        type:req.params.type
                    }
                    
                });
                
                    var updatedAlbum = {
                        id: parseInt(req.params.id),
                        photo:fileArray                    
                    };
                    console.log(updatedAlbum)
                    
              vendorProfile.updateVendorProfile(updatedAlbum , function (err, menu) {
                    if (err) {
                        return next(err);
                    } else {
                        res.send(menu);
                    }
                });

            });
        }

    });
});


function uploadPics(files, vendorId, callback) {
    let fileNames = [];

    var filesUploads = files.map(file => uploadPic(file, vendorId));
    var promises = all(filesUploads);

    promises.then(function(values) {
        return callback(null, values);
    });
}

function uploadPic(file, vendorId) {
    var deferred =   defer();
    aws.createAlbum(config.BUCKET_NAME + '/images', 'vendor', function (err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            aws.createAlbum(config.BUCKET_NAME + '/images/vendor/', vendorId, function (err, data) {
                if (err) {
                    deferred.reject(err);
                } else {

                            aws.awsUpload(config.BUCKET_NAME + '/images/vendor/' + vendorId ,
                                file, file.filename, function (err, data) {
                                    if (err) {
                                        deferred.reject(err);
                                    } else {
                                        deferred.resolve({name:file.filename, mineType:file.mimetype});
                                    }

                                })
                        }
                    });
                }
            });



    return deferred.promise;
}

router.get('/vendor/:id', function(req, res, next){

    vendorProfile.getVendor(req.params.id, function(err, status){
        if(err){
            return next(err)
        }
        res.send(status)

    })
})


router.get('/getPhotos/:vendorId/:type', function(req, res, next){

    vendorProfile.getVendorPhoto(req.params.vendorId, req.params.type, function(err, getphoto){
        if(err){
            return next(err)
        }
        res.send(getphoto)
    })
})


router.get('/forgetBusinessId/:phoneNo', function(req, res, next){

    vendorProfile.forgetBusinessId(req.params.phoneNo, function(err, status){
        if(err){
            return next(err)
        }
        res.send(status)

    })
})

// router.get('/getMenu/:vendorId', function(req, res, next) {

//     if(req.params.vendorId) {
//         aws.getAlbum(config.BUCKET_NAME, + '/images/vendor/'+req.params.vendorId, function (err, data) {
//             if (err) {
//                 return next(err);
//             } else {
//                 res.send(data);
//             }


//         });
//     }

// })


router.post('/menu', function(req, res, next){

    vendorProfile.createdMenu(req.body, function(err, status){
        if(err){
            return next(err)
        }
        res.send(status)

    })
})


router.get('/item', function(req, res, next){
    vendorProfile.getAllMenu(req.query.text, function(err, itemList){
        if(err){
            return next(err)
        }
        res.send(itemList);
    })
})


router.get('/recommended', function(req, res, next){
    vendorProfile.getAllrecommendedType(req.query.text, function(err, itemLists){
        if(err){
            return next(err)
        }
        res.send(itemLists);
    })
})



router.get('/tableList/:vendorId', function(req, res, next) {

    var params = {
        floorCode: req.query.floorCode,
        date: req.query.date,
        time: req.query.time
    }

    vendorProfile.getVendorTableStructure(req.params.vendorId, params, function(err, tables){
        if(err){
            return next(err)
        }
        res.send(tables)

    })
})












module.exports = router;