var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
var aws = require('../../utils/aws-upload');
var defer = require("node-promise").defer;
var all = require("node-promise").all;

var _ = require("lodash");



var document= require('../../model/vendor/document');




router.post('/createDocument', function(req, res, next){
    document.createdDoument(req.body, function(err, document){
        if(err){
            return next(err)
        }
        res.send(document)
    })
})

router.post('/update', function(req, res, next){
    document.updateVendoDocument(req.body, function(err, document){
        if(err){
            return next(err)
        }
        res.send(document)
    })
})


router.get('/list/:id', function(req, res, next){
    document.getVendorDcoument(req.params.id ,function(err, document){
        if(err){
            return next(err)
        }
        res.send(document)
    })
})

router.post('/verify', function(req, res, next){
    document.updateVerifyDocument(req.body, function(err, document){
        if(err){
            return next(err)
        }
        res.send(document)
    })
})

var storage = multer.diskStorage({

    destination: function(req, file, callback) {

        var filePath = './public/images/';
        callback(null, filePath)
    },

    filename: function(req, file, callback) {
        var fileName = new Date().getTime().toString()+"_document"+path.extname(file.originalname);
        callback(null,  fileName);
    }


})


router.post('/documentUpload/:vendorId', function(req, res, next) {

    console.log(req.params.vendorId);
    var upload = multer({
        storage: storage
    }).array('filename',2);
    upload(req, res, function(err, file) {
        if(req.params.vendorId) {
            aws.createAlbum(config.BUCKET_NAME+'/images/vendor/', req.params.vendorId, function(err, data) {
                if (err) {
                    return next(err);
                } else {
                    aws.createAlbum(config.BUCKET_NAME+'/images/vendor/'+req.params.vendorId, 'document', function(err, data) {
                        if (err) {
                            return next(err);
                        } else {
                            aws.awsUpload(config.BUCKET_NAME+'/images/vendor/'+req.params.vendorId+'/document',
                                req.files[0], req.files[0].filename, function (err, data) {
                                if(err) {
                                   return next(err);
                                } else {
                                   var vendorDocument = {
                                        image: req.files[0].filename,
                                        type: req.body.type,
                                        isVerified:false,            
                               }
                               if(req.body.documentID) {
                                vendorDocument.documentID = req.body.documentID;
                               }

                               if(req.body.documentName){
                                vendorDocument.documentName = req.body.documentName;
                               }

                               if(req.body.registerAddress){
                                vendorDocument.registerAddress = req.body.registerAddress;
                               }

                               if(req.body.address2){
                                vendorDocument.address2 = req.body.address2;
                               }

                               if(req.body.state){
                                vendorDocument.state = req.body.state;
                               }

                               if(req.body.city){
                                vendorDocument.city = req.body.city;
                               }

                               if(req.body.pincode){
                                vendorDocument.pincode = req.body.pincode;
                               }

                               if(req.body.expiryDate){
                                vendorDocument.expiryDate = req.body.expiryDate;
                               }
                                    var vendorDocuments = {
                                       id: parseInt(req.params.vendorId),
                                       document: vendorDocument
                                    }
                                    
                                    console.log(vendorDocuments)
                                   document.updateVendoDocument(vendorDocuments, function (err, vendordocument) {
                                        if (err) {
                                            return next(err);
                                        }
                                        res.send(vendordocument);
                                    });

                                }
                            
                            
                            })
                        }
                    });
                }
            })
        }
   })
});


module.exports = router;
