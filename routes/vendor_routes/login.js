var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');

var vendorDB = require('../../model/vendor/login');






router.get('/login/:busniessId', function(req, res, next){
    vendorDB.existingVendor(req.params.busniessId, function(err, busniessID){
        if(err){
            return next(err)
        }
        res.send(busniessID)
    })  
})

router.post('/', function(req, res, next) {

    vendorDB.createvendorProperty(req.body, function (err, login) {
        if (err) {
            return next(err);
        }
        res.send(login);
    });
});

router.post('/addproperty', function(req, res, next) {

    vendorDB.addclaimProperty(req.body, function (err, login) {
        if (err) {
            return next(err);
        }
        res.send(login);
    });
});

router.post('/update', function(req, res, next) {

    vendorDB.update(req.body, function (err, login) {
        if (err) {
            return next(err);
        }
        res.send(login);
    });
});

router.get('/sendotpbyId/:id', function(req, res, next) {

    vendorDB.sendOtpbyId(req.params.id, function (err, logins) {
        if (err) {
            return next(err);
        }
        res.send(logins);
    });
});

router.get('/getclaimproperty', function(req, res, next) {
    
    vendorDB.checkpropertyName(req.query.city, req.query.name, function (err, claims) {
         if (err) {
             return next(err);
         } else {
             res.send(claims);
         }
     });
 });

 router.get('/autoSearch', function(req, res, next) {
    
    vendorDB.autocompelete(req.query.name, req.query.city, function (err, claims) {
         if (err) {
             return next(err);
         } else {
             res.send(claims);
         }
     });
 });

router.get('/verifyotp/:id/:otpNo', function(req, res, next) {
    
   vendorDB.validateUserOtp(req.params, function (err, users) {
        if (err) {
            return next(err);
        } else {
            res.send(users);
        }
    });
});


router.get('/resendOtp/:id', function(req, res, next) {

    vendorDB.resendOtp(req.params.id, function (err, vendor) {
        if (err) {
            return next(err);
        } else{
            res.send(vendor);
        }

    });
});


router.get('/vendorStatus/:id', function(req, res, next){

    vendorDB.getStatus(req.params.id, function(err, status){
        if(err){
            return next(err)
        }
        res.send(status)

    })
})








module.exports = router;