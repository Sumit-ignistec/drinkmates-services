var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
var aws = require('../../utils/aws-upload')
var defer = require("node-promise").defer;
var all = require("node-promise").all;
var _ = require("lodash");



var community = require('../../model/vendor/community');


router.post('/create', function(req, res, next){
    community.createdCommunity(req.body, function(err, commmunitys){
        if(err){
            return next(err)
        }
        res.send(commmunitys)
    })
})


// var storage = multer.diskStorage({

//     destination: function(req, file, callback) {

//         var filePath = './public/images/';
//         callback(null, filePath)
//     },

//     filename: function(req, file, callback) {
//         var fileName = new Date().getTime().toString()+"_community"+path.extname(file.originalname);
//         callback(null,  fileName);
//     }


// })


// router.post('/create/:vendorId', function(req, res, next) {

//     console.log(req.params.vendorId);
//     var upload = multer({
//         storage: storage
//     }).array('filename',1);
//     upload(req, res, function(err, file) {
//         if(req.params.vendorId) {
//             aws.createAlbum(config.BUCKET_NAME+'/images/vendor/', req.params.vendorId, function(err, data) {
//                 if (err) {
//                     return next(err);
//                 } else {
//                     aws.createAlbum(config.BUCKET_NAME+'/images/vendor/'+req.params.vendorId, 'community', function(err, data) {
//                         if (err) {
//                             return next(err);
//                         } else {
//                             aws.awsUpload(config.BUCKET_NAME+'/images/vendor/'+req.params.vendorId+'/community',
//                                 req.files[0], req.files[0].filename, function (err, data) {
//                                 if(err) {
//                                    return next(err);
//                                 } else {
//                                    var vendorDocument = {
//                                         icon: req.files[0].filename,
//                                         type: "vendor_community",
//                                         //isVerified:false,            
//                                }
//                                if(req.body.name) {
//                                 vendorDocument.name = req.body.name;
//                                }

//                                if(req.body.description){
//                                 vendorDocument.description = req.body.description;
//                                }

                               
//                                     var vendorDocuments = {
//                                        id: parseInt(req.params.vendorId),
//                                        community: vendorDocument
//                                     }
                                    
//                                     console.log(vendorDocuments)
//                                    community.updateCommunity(vendorDocuments, function (err, vendordocument) {
//                                         if (err) {
//                                             return next(err);
//                                         }
//                                         res.send(vendordocument);
//                                     });

//                                 }
                            
                            
//                             })
//                         }
//                     });
//                 }
//             })
//         }
//    })
// });


router.get('/user/:vendorId/:latitude/:longitude/:distance', function(req, res, next){

    community.communityNearByUser(req.params.vendorId, req.params.latitude, req.params.longitude, req.params.distance ,function (err, nearByUser) {
        if (err) {
            return next(err);
        } else{
            res.send( nearByUser);
        }
    });
});


router.post('/sharePost', function(req, res, next){

    var params = {
        description: req.body.description,
        scheduleDate: req.body.scheduleDate,
        scheduleTime: req.body.scheduleTime
    }

    community.shareToComunityPost(req.body.postId, req.body.communityId , params, function (err, sharecommunityPosts) {
        if (err) {
            return next(err);
        } else{
            res.send(sharecommunityPosts);
        }
    });
});


router.get('/my/:vendorId', function(req, res, next){

    community.getVendorCommunity(req.params.vendorId, function(err, communityVendor){
        if(err){
            return next(err)
        }
        res.send(communityVendor)
    })
})

router.post('/invite/', function(req, res, next){

    community.vendorInviteUser(req.body.vendorId, req.body.userId, function(err, communityVendorInvite){
        if(err){
            return next(err)
        }
        res.send(communityVendorInvite)
    })
})


router.get('/post/:vendorId', function(req, res, next){

   community.communityPost(req.params.vendorId, function(err, postCommunity){
        if(err){
            return next(err)
        }
        res.send(postCommunity)
    })
})


module.exports = router;