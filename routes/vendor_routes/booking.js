var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
var aws = require('../../utils/aws-upload');
var defer = require("node-promise").defer;
var all = require("node-promise").all;

var _ = require("lodash");



var booking = require('../../model/vendor/booking');


router.get('/', function(req, res){
    res.send('Welcome Booking')
})

router.post('/create', function(req, res , next){

    booking.createdBooking(req.body, function(err, bookingUser){
        if(err){
            return next(err)
        }
        res.send(bookingUser)
    })
})

router.post('/update', function(req, res , next){

    booking.bookingRequestUpdate(req.body, function(err, bookingUser){
        if(err){
            return next(err)
        }
        res.send(bookingUser)
    })
})

router.post('/createWalkIn', function(req, res , next){

    booking.createdWalkinBooking(req.body, function(err, bookingUser){
        if(err){
            return next(err)
        }
        res.send(bookingUser)
    })
})

router.get('/request/:vendorId', function(req, res , next){

    booking.getBookingRequest(req.params.vendorId, function(err, bookingUser){
        if(err){
            return next(err)
        }
        res.send(bookingUser)
    })
})

router.get('/confirmList/:vendorId', function(req, res , next){

    booking.confirmBookingList(req.params.vendorId, function(err, bookingConfirm){
        if(err){
            return next(err)
        }
        res.send(bookingConfirm)
    })
})



router.get('/userList/:userId', function(req, res , next){

    booking.getuserBooking(req.params.userId, function(err, bookingUser){
        if(err){
            return next(err)
        }
        res.send(bookingUser)
    })
})

router.get('/activeTableList/:vendorId', function(req, res , next){

    booking.activeTableList(req.params.vendorId, function(err, vendorActiveTable){
        if(err){
            return next(err)
        }
        res.send(vendorActiveTable)
    })
})

router.get('/allocateTable/:bookingId/:tableId', function(req, res , next){

    booking.allocateTableByVendor(req.params.bookingId, req.params.tableId, function(err, tableAllocate){
        if(err){
            return next(err)
        }
        res.send(tableAllocate)
    })
})


router.post('/orderMenu', function(req, res , next){

    booking.menuOrderByUser(req.body.bookingId, req.body.menuIds, req.body.orderDishQuantity, function(err, menuOrder){
        if(err){
            return next(err)
        }
        res.send(menuOrder)
    })
})

module.exports = router;

