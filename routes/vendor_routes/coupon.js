var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
var aws = require('../../utils/aws-upload');
var defer = require("node-promise").defer;
var all = require("node-promise").all;

var _ = require("lodash");



var coupon = require('../../model/vendor/coupon');


router.post('/create', function(req, res, next){
    coupon.createdCoupon(req.body, function(err, coupons){
        if(err){
            return next(err)
        }
        res.send(coupons)
    })
})

router.post('/update', function(req, res, next){
    coupon.updateVendorCoupon(req.body, function(err, vendorCoupon){
        if(err){
            return next(err)
        }
        res.send(vendorCoupon)
    })
})


var storage = multer.diskStorage({

    destination: function(req, file, callback) {

        var filePath = './public/images/'
        callback(null, filePath)
    },

    filename: function(req, file, callback) {
        var fileName = new Date().getTime().toString()+"_coupon"+path.extname(file.originalname);
        callback(null,  fileName);
    }


})


router.post('/backgroundUpload/:vendorId', function(req, res, next) {

    console.log(req.params.vendorId);
    var upload = multer({
        storage: storage
    }).array( "filename", 3);
    upload(req, res, function(err, file) {
        let fileNames = [];
        if(req.params.vendorId) {
            uploadPics(req.files, req.params.vendorId, req.body.postId, function(err, files)  {
                let fileArray = files.map(file => {
                    return file.name;
                });

                var updatedPost = {
                    id: parseInt(req.body.postId),
                    coupon: {
                            image: fileArray,
                    }
                }
                console.log(updatedPost);

                coupon.updateVendorCoupon(updatedPost, function (err, posts) {
                    if (err) {
                        return next(err);
                    } else {
                        res.send(posts);
                    }
                });

            });
        }

    });
});


function uploadPics(files, vendorId ,postId, callback) {
    let fileNames = [];

    var filesUploads = files.map(file => uploadPic(file, vendorId ,postId));
    var promises = all(filesUploads);

    promises.then(function(values) {
        return callback(null, values);
    });
}

function uploadPic(file, vendorId ,postId) {
    var deferred =   defer();
    aws.createAlbum(config.BUCKET_NAME + '/images/vendor', vendorId, function (err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            aws.createAlbum(config.BUCKET_NAME + '/images/vendor/' + vendorId, 'coupon', function (err, data) {
                if (err) {
                    deferred.reject(err);
                } else {
                    aws.createAlbum(config.BUCKET_NAME + '/images/vendor/' + vendorId + '/coupon', postId, function (err, data) {
                        if (err) {
                            deferred.reject(err);
                        } else {
                            aws.awsUpload(config.BUCKET_NAME + '/images/vendor/' + vendorId + '/coupon/' + postId,
                                file, file.filename, function (err, data) {
                                    if (err) {
                                        deferred.reject(err);
                                    } else {
                                        deferred.resolve({name:file.filename, mineType:file.mimetype});
                                    }

                                })
                        }
                    });
                }
            });
        }
    });

    return deferred.promise;
}


router.get('/vendorCoupon/:vendorId', function(req, res, next){
    
    coupon.getVendorCoupon(req.params.vendorId, function(err, updateCoupons){
        if(err){
            return next(err)
        }
        res.send(updateCoupons)
    })
})


router.get('/promoTotal/:vendorId', function(req, res, next){

    coupon.getVendorCouponTotal(req.params.vendorId, function(err, eventTotal){
        if(err){
            return next(err)
        }
        res.send(eventTotal)
    })
})



module.exports = router;