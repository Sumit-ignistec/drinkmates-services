var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
var aws = require('../../utils/aws-upload')
var defer = require("node-promise").defer;
var all = require("node-promise").all;
var _ = require("lodash");


var vendorAlbum = require('../../model/vendor/album');


router.post('/vendor', function(req, res, next){
    vendorAlbum.createVendorAlbum(req.body, function(err, vendoralbum){
            if(err){
                return next(err)
            }
            res.send(vendoralbum)
        })
})
    
router.post('/updateAlbum', function(req,res,next){
    
        vendorAlbum.updateAlbums(req.body,function(err, event){
            if(err){
                    return next(err);
            }
            res.send(event);
        })
});

router.get('/vendorAlbum/:vendorId', function(req,res,next){

    vendorAlbum.getVendorAlbum(req.params.vendorId,function(err, albums){
        if(err){
                return next(err);
        }
        res.send(albums);
    })
});



var storage = multer.diskStorage({

    destination: function(req, file, callback) {

        var filePath = './public/images/';
        callback(null, filePath)
    },

    filename: function(req, file, callback) {
        var fileName = new Date().getTime().toString()+"_vendor"+path.extname(file.originalname);
        callback(null,  fileName);
    }


})




router.post('/upload/:vendorId', function(req, res, next) {

    console.log(req.params.vendorId);
    var upload = multer({
        storage: storage
    }).array( "filename", 10);
    upload(req, res, function(err, file) {
        let fileNames = [];
        if(req.params.vendorId) {
            uploadPics(req.files, req.params.vendorId, req.body.albumId, function(err, files)  {
                let fileArray = files.map(file => {
                    return file.name;
                });
                
                var updatedPost = {
                    id: parseInt(req.body.albumId),
                    
                        photo: [{
                           image: fileArray,
                           type:'vendor_album' 
                        }]
                    
                }
                    console.log(updatedPost)
              vendorAlbum.updateAlbums(updatedPost , function (err, albumVendor) {
                    if (err) {
                        return next(err);
                    } else {
                        res.send(albumVendor);
                    }
                });

            });
        }

    });
});


function uploadPics(files, vendorId, albumId, callback) {
    let fileNames = [];

    var filesUploads = files.map(file => uploadPic(file, vendorId, albumId));
    var promises = all(filesUploads);

    promises.then(function(values) {
        return callback(null, values);
    });
}

function uploadPic(file, vendorId, albumId) {
    var deferred =   defer();
    aws.createAlbum(config.BUCKET_NAME + '/images/vendor', vendorId, function (err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            aws.createAlbum(config.BUCKET_NAME + '/images/vendor/' + vendorId, 'album', function (err, data) {
                if (err) {
                    deferred.reject(err);
                } else {
                    aws.createAlbum(config.BUCKET_NAME + '/images/vendor/' + vendorId + '/album', albumId, function (err, data) {
                        if (err) {
                            deferred.reject(err);
                        } else {
                            aws.awsUpload(config.BUCKET_NAME + '/images/vendor/' + vendorId + '/album/' + albumId,
                                file, file.filename, function (err, data) {
                                    if (err) {
                                        deferred.reject(err);
                                    } else {
                                        deferred.resolve({name:file.filename, mineType:file.mimetype});
                                    }

                                })
                        }
                    });
                }
            });
        }
    });

    return deferred.promise;
}

module.exports = router;    