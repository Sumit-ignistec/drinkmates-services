var express = require('express');
var router = express.Router();
var multer  = require('multer')
var path = require('path')
var fs = require('fs');
var aws = require('../../utils/aws-upload');
var defer = require("node-promise").defer;
var all = require("node-promise").all;

var vendorEvent = require('../../model/user/event');



router.get('/', function(req, res, next){
    res.send('Welcome to event');
})


router.post('/vendor', function(req, res, next){

    vendorEvent.createdVendorEvent(req.body, function(err, events){
        if(err){
            return next(err)
        }
        res.send(events)
    })
})


router.put('/updateVendorEvent', function(req, res, next){
    
    vendorEvent.updateEvent(req.body, function(err, updateEvents){
        if(err){
            return next(err)
        }
        res.send(updateEvents)
    })
})


router.get('/eventVendor/:vendorId', function(req, res, next){
    
    vendorEvent.getVendorEvent(req.params.vendorId, function(err, updateEvents){
        if(err){
            return next(err)
        }
        res.send(updateEvents)
    })
})


var storage = multer.diskStorage({
    
    destination: function(req, file, callback) {

        var filePath = './public/images/'
        callback(null, filePath)
    },

    filename: function(req, file, callback) {
        var fileName = new Date().getTime().toString()+"_vendorevent"+path.extname(file.originalname);
        callback(null,  fileName);
    }


})

router.post('/upload/:id', function(req, res, next) {

    console.log(req.params.id);
    var upload = multer({
        storage: storage
    }).array( "filename", 3);
    upload(req, res, function(err, file) {
        let fileNames = [];
        if(req.params.id) {
            uploadPics(req.files, req.params.id, req.body.vendorId, function(err, files)  {
                let fileArray = files.map(file => {
                    return file.name;
                });

                var updatedPost = {
                    id: parseInt(req.body.vendorId),
                     photo: fileArray, 
                }
                console.log(updatedPost);

                vendorEvent.updateVendorEvent(updatedPost, function (err, posts) {
                    if (err) {
                        return next(err);
                    } else {
                        res.send(posts);
                    }
                });

            });
        }

    });
});


function uploadPics(files, id ,vendorId, callback) {
    let fileNames = [];

    var filesUploads = files.map(file => uploadPic(file, id ,vendorId));
    var promises = all(filesUploads);

    promises.then(function(values) {
        return callback(null, values);
    });
}

function uploadPic(file, id, vendorId) {
    var deferred =   defer();
    aws.createAlbum(config.BUCKET_NAME + '/images', id, function (err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            aws.createAlbum(config.BUCKET_NAME + '/images/' + id, 'vendorEvent', function (err, data) {
                if (err) {
                    deferred.reject(err);
                } else {
                    aws.createAlbum(config.BUCKET_NAME + '/images/' + id + '/vendorEvent', vendorId, function (err, data) {
                        if (err) {
                            deferred.reject(err);
                        } else {
                            aws.awsUpload(config.BUCKET_NAME + '/images/' + id + '/vendorEvent/' + vendorId,
                                file, file.filename, function (err, data) {
                                    if (err) {
                                        deferred.reject(err);
                                    } else {
                                        deferred.resolve({name:file.filename, mineType:file.mimetype});
                                    }

                                })
                        }
                    });
                }
            });
        }
    });

    return deferred.promise;
}

router.get('/total/:vendorId', function(req, res, next){

    vendorEvent.getVendorEventTotal(req.params.vendorId, function(err, eventTotal){
        if(err){
            return next(err)
        }
        res.send(eventTotal)
    })
})




module.exports = router;