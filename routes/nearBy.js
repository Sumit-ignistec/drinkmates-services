/**
 * Created by sumitgabhane on 11/19/17.
 */
var express = require('express');
var router = express.Router();
var nearBy = require('../model/user/nearBy');

router.get('/user/:id/:latitude/:longitude/:distance', function(req, res, next){

    nearBy.getNearByUser(req.params.id, req.params.latitude, req.params.longitude, req.params.distance, req.query ,function (err, nearByUser) {
        if (err) {
            return next(err);
        } else{
            res.send( nearByUser);
        }
    });
});

router.get('/vendor/:latitude/:longitude/:distance', function(req, res, next){

    nearBy.getNearByVendor(req.params.latitude, req.params.longitude, req.params.distance, req.query, function (err, nearByVendor) {
        if (err) {
            return next(err);
        } else{
            res.send( nearByVendor);
        }
    });
});


router.get('/specificLocationVendor/:city', function(req, res, next){

    nearBy.vendorFilterByLocationAndCuisine(req.params.city, function (err, locationByVendor) {
        if (err) {
            return next(err);
        } else{
            res.send(locationByVendor);
        }
    });
});

module.exports = router;
