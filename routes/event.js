var express = require('express');
var router = express.Router();
var events = require('../model/user/event');
var post = require('../model/user/post');
var aws = require('../utils/aws-upload');
var multer  = require('multer');
var path = require('path');

var defer = require("node-promise").defer;
var all = require("node-promise").all;


router.post('/', function(req,res,next){

    events.createEvent(req.body,function(err, event){
        if(err){
                return next(err);
        }
        res.send(event);
    })
});

router.put('/', function(req,res,next){

    events.updateEvent(req.body,function(err, event){
        if(err){
            return next(err);
        }
        res.send(event);
    })
});


router.get('/nearByEvent/:type/:latitude/:longitude/:distance', function(req, res, next){
    
    events.getNearByEvent(req.params.type,req.params.latitude, req.params.longitude, req.params.distance ,function (err, nearByEvent) {
        if (err) {
            return next(err);
        } else{
            res.send( nearByEvent);
        }
    });
});




var storage = multer.diskStorage({

    destination: function(req, file, callback) {

        var filePath = './public/images/'
        callback(null, filePath)
    },

    filename: function(req, file, callback) {
        var fileName = new Date().getTime().toString()+"_post"+path.extname(file.originalname);
        callback(null,  fileName);
    }


})


router.post('/postupload/:type/:mobileNo', function(req, res, next) {

   /// console.log(" mobileNo+"+req.params.mobileNo);
    var upload = multer({
        storage: storage
    }).array( "filename", 15);
    upload(req, res, function(err, file) {
        let fileNames = [];
        if(req.params.mobileNo) {
            uploadPics(req.files, req.params.mobileNo, req.body.postId, function(err, files)  {
                if(req.params.type == 'event' || req.params.type == 'vendor_event') {
                    let fileArray = files.map(file => {
                        return file.name;
                    });
                    var updatedPost = {
                        id: parseInt(req.body.postId),
                        event: {
                            photo: fileArray,
                            id: parseInt(req.body.eventId)
                        }
                    }
                    console.log(updatedPost);

                    events.updateEvent(updatedPost, function (err, posts) {
                        if (err) {
                            return next(err);
                        } else {
                            res.send(posts);
                        }
                    });
                } else if(req.params.type == 'post') {

                    let contents = [];
                    files.forEach(file => {

                        let content = {
                            type: file.mineType.substring(0, file.mineType.indexOf('/')),
                            content: file.name
                        }
                        contents.push(content);
                    });
                    var updatedPost = {
                        id: parseInt(req.body.postId),
                        content: contents
                    };
                    console.log(updatedPost);

                    post.updatePost(updatedPost, function (err, posts) {
                        if (err) {
                            return next(err);
                        } else {
                            res.send(posts);
                        }
                    });
                }

            });
        }

    });
});

router.post('/uploadlocationpic/:mobileNo', function(req, res, next) {

    console.log(req.params.mobileNo);
    var upload = multer({
        storage: storage
    }).array( "filename", 3);
    upload(req, res, function(err, file) {
        let fileNames = [];
        if(req.params.mobileNo) {
            uploadPics(req.files, req.params.mobileNo, req.body.postId, function(err, files)  {
                let fileArray = files.map(file => {
                    return file.name;
                });

                var updatedPost = {
                    id: parseInt(req.body.postId),
                    event: {
                        eventLocation: [{
                            photo: fileArray,
                            id: parseInt(req.body.locationId)
                        }]
                    }
                }
                console.log(updatedPost);

                events.updateEvent(updatedPost, function (err, posts) {
                    if (err) {
                        return next(err);
                    } else {
                        res.send(posts);
                    }
                });

            });
        }

    });
});

router.post('/post/comment', function(req, res, next){
    
    events.commentPost(req.body.postId, req.body.userId, req.body.comment ,function (err, comments) {
        if (err) {
            return next(err);
        } else{
            res.send(comments);
        }
    });
});


router.post('/post/likes', function(req, res, next){
    
    events.likesPost(req.body.postId, req.body.userId, req.body.star ,function (err, likes) {
        if (err) {
            return next(err);
        } else{
            res.send(likes);
        }
    });
});


router.post('/post/interest/:type', function(req, res, next){
    if(req.params.type == 'event') {
        events.eventInterested(req.body.eventId, req.body.userId, req.body.input, function (err, interests) {
            if (err) {
                return next(err);
            } else {
                res.send(interests);
            }
        });
    } else {
        events.eventlocationInterested(req.body.eventlocationId, req.body.userId, req.body.input ,function (err, interests) {
            if (err) {
                return next(err);
            } else{
                res.send(interests);
            }
        });
    }
});



router.get('/:postId', function(req, res, next){
    
    events.getEventbyPostId(req.params.postId ,function (err, eventplaces) {
        if (err) {
            return next(err);
        } else{
            res.send(eventplaces);
        }
    });
});

router.get('/my/:userId', function(req, res, next){

    events.getMyEvent(req.params.userId ,function (err, events) {
        if (err) {
            return next(err);
        } else{
            res.send(events);
        }
    });
});


router.get('/post/deletecomment/:postId/:userId/:commentId', function(req, res, next){
    
    events.deletecommentPost(req.params.postId, req.params.userId , req.params.commentId ,function (err, comments) {
        if (err) {
            return next(err);
        } else{
            res.send(comments);
        }
    });
});


router.post('/post/updatecomment', function(req, res, next){
    
    events.updatecommentPost(req.body.postId, req.body.userId , req.body.commentId , req.body.comment,function (err, comments) {
        if (err) {
            return next(err);
        } else{
            res.send(comments);
        }
    });
});

function uploadPics(files, mobileNo ,postId, callback) {
    let fileNames = [];

    var filesUploads = files.map(file => uploadPic(file, mobileNo ,postId));
    var promises = all(filesUploads);

    promises.then(function(values) {
        return callback(null, values);
    });
}

function uploadPic(file, mobileNo, postId) {
    var deferred =   defer();
    aws.createAlbum(config.BUCKET_NAME + '/images', mobileNo, function (err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            aws.createAlbum(config.BUCKET_NAME + '/images/' + mobileNo, 'post', function (err, data) {
                if (err) {
                    deferred.reject(err);
                } else {
                    aws.createAlbum(config.BUCKET_NAME + '/images/' + mobileNo + '/post', postId, function (err, data) {
                        if (err) {
                            deferred.reject(err);
                        } else {
                            aws.awsUpload(config.BUCKET_NAME + '/images/' + mobileNo + '/post/' + postId,
                                file, file.filename, function (err, data) {
                                    if (err) {
                                        deferred.reject(err);
                                    } else {
                                        deferred.resolve({name:file.filename, mineType:file.mimetype});
                                    }

                                })
                        }
                    });
                }
            });
        }
    });

    return deferred.promise;
}

module.exports = router;
