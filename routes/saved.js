var express = require('express');
var router = express.Router();
var saved = require('../model/user/saved');

router.get('/', function(req, res, next){
    res.send('Welcome to Saved')
})


router.post('/', function(req, res, next){
    saved.savedPost(req.body.userId, req.body.postId, function(err, save){
        if(err){
            return next(err)
        }
        res.send(save)
    })
})

router.get('/mysaved/:userId/:type', function(req, res , next){
    
    saved.getmySavedPost(req.params.userId, req.params.type, function(err, getmySave){

        if(err){
            return next(err)
        }
        res.send(getmySave)
    })

})

router.post('/mates', function(req, res, next){

    saved.savedMates(req.body.fromuserId, req.body.touserId, function(err, savemates){
        if(err){
            return next(err)
        }
        res.send(savemates)
    })
})


router.get('/getmates/:userId', function(req, res, next){

    saved.getsaveMates(req.params.userId, function(err, savemates){
        if(err){
            return next(err)
        }
        res.send(savemates)
    })
})


router.post('/checkin', function(req, res, next){

    saved.saveCheckin(req.body.userId, req.body.checkinId, function(err, checkin){
        if(err){
            return  next(err)
        }
        res.send(checkin)
    })
})

module.exports = router;