var express = require('express');
var router = express.Router();
var post = require('../model/user/post');
var profileOMeter = require('../model/user/profilometer');
var aws = require('../utils/aws-upload');

router.post('/', function(req, res, next){

    post.createPost(req.body, function(err, userPost){

        if(err){
            return next(err);
        }
        res.send(userPost);
    })
    
})


router.get('/:userId', function(req, res, next){

    post.getUserPost(req.params.userId, function(err, userPost){

        if(err){
            return next(err);
        }
        res.send(userPost);
    })

})

router.get('/getmyPost/:userId', function(req, res, next){

    post.getmyPost(req.params.userId, function(err, userPost){

        if(err){
            return next(err);
        }
        res.send(userPost);
    })

})



router.post('/rateUserPost',function(req,res,next){
    profileOMeter.calculateProfilometer(req.query,function(err,profileOmeter){
        if(err){
            return next(err);
        } 
        res.send(profileOmeter);
    });
});


// router.get('/getLikes/:postId', function(req, res, next){
//     post.getAvgStarByUser(req.params.postId, function(err, getlikes){
//         if(err){
//             return next(err)
//         }
//         res.send(getlikes)
//     })
// })


router.get('/getImages/:mobileNo', function(req, res, next) {

    if(req.params.mobileNo){
      aws.getAlbum(config.BUCKET_NAME, 'images/'+req.params.mobileNo+ '/post',function(err, data){
          if(err){
              return next(err)
          }else{
              res.send(data)
          }
      })
    }
  
  })

module.exports = router;
