var express = require('express');
var router = express.Router();
var myMate = require('../model/user/myMates');





router.get('/sendfrndrequest/:fromMobileNo/to/:toMobileNo', function(req,res, next){
    myMate.sendRequest(req.params.fromMobileNo, req.params.toMobileNo, function (err, profile) {
        if (err) {
            return next(err);
        } else{
            res.send(profile);
        }
    });
 });

router.get('/acceptfrndrequest/:fromMobileNo/to/:toMobileNo', function(req,res, next){

    myMate.acceptRequest(req.params.fromMobileNo, req.params.toMobileNo, function (err, profile) {
        if (err) {
            return next(err);
        } else{
            res.send(profile);
        }

    });
});

router.get('/visited/:fromMobileNo/to/:toMobileNo', function(req,res, next){

    myMate.updateVisited(req.params.fromMobileNo, req.params.toMobileNo, function (err, profile) {
        if (err) {
            return next(err);
        } else{
            res.send(profile);
        }

    });
});


 router.get('/userstatuslist/:mobileNo/:status', function(req, res, next){
    myMate.getStatus(req.params.mobileNo , req.params.status, function (err, profile) {
        if (err) {
            return next(err);
        } else{
            res.send(profile);
        }
    });
 });

 router.get('/visiteduser/:mobileNo', function(req, res, next){
        myMate.getVisted(req.params.mobileNo , function (err, mateVisited) {
            if (err) {
                return next(err);
            } else{
                res.send(mateVisited);
            }
        });
 });


 router.get('/blocked/:fromMobileNo/to/:toMobileNo', function(req, res, next){
        
            myMate.updateBlocked(req.params.fromMobileNo , req.params.toMobileNo, function (err, mateBlocked) {
                if (err) {
                    return next(err);
                } else{
                    res.send( mateBlocked);
                }
        
            });
 });  


 router.get('/unfriendRequest/:fromMobileNo/to/:toMobileNo', function(req,res, next){

    myMate.unfriendRequest(req.params.fromMobileNo, req.params.toMobileNo, function (err, profile) {
        if (err) {
            return next(err);
        } else{
            res.send(profile);
        }

    });
})


router.get('/cancelRequest/:fromMobileNo/to/:toMobileNo', function(req,res, next){
    
        myMate.cancelRequest(req.params.fromMobileNo, req.params.toMobileNo, function (err, cancelRequest) {
            if (err) {
                return next(err);
            } else{
                res.send(cancelRequest);
            }
    
        });
})

router.get('/rejectRequest/:fromMobileNo/to/:toMobileNo', function(req,res, next){
        
        myMate.cancelRequest(req.params.fromMobileNo, req.params.toMobileNo, function (err, rejectRequest) {
            if (err) {
                return next(err);
            } else{
                res.send(rejectRequest);
            }
    
        });
})

router.post('/userList', function(req, res, next) {
    myMate.getUsers(req.body.mobileNo, function (err, userList) {
        if (err) {
            return next(err);
        } else{
            res.send(userList);
        }
    });
});

router.get('/userReceivedlist/:mobileNo/:status', function(req, res, next){
    myMate.getReceived(req.params.mobileNo , req.params.status, function (err, userReceived) {
        if (err) {
            return next(err);
        } else{
            res.send(userReceived);
        }
    });
 });


router.get('/userSentlist/:mobileNo/:status', function(req, res, next){
    myMate.getSent(req.params.mobileNo , req.params.status, function (err, sent) {
        if (err) {
            return next(err);
        } else{
            res.send(sent);
        }
    });
});

module.exports = router;