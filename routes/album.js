var express = require('express');
var router = express.Router();
var album = require('../model/user/album');
var post = require('../model/user/post');
var aws = require('../utils/aws-upload');
var multer  = require('multer');
var path = require('path');
var fs = require('fs');
var defer = require("node-promise").defer;
var all = require("node-promise").all;


router.post('/', function(req,res,next){

    album.createAlbum(req.body,function(err, event){
        if(err){
                return next(err);
        }
        res.send(event);
    })
});


router.post('/updateAlbum', function(req,res,next){

    album.updateAlbum(req.body,function(err, event){
        if(err){
                return next(err);
        }
        res.send(event);
    })
});

router.get('/:userId', function(req,res,next){

    album.getuserAlbum(req.params.userId,function(err, albums){
        if(err){
                return next(err);
        }
        res.send(albums);
    })
});

router.get('/getalbum/:albumId', function(req,res,next){

    album.getalbumContent(req.params.albumId,function(err, albums){
        if(err){
                return next(err);
        }
        res.send(albums);
    })
});

router.post('/delete', function(req,res,next){

    album.deleteAlbum(req.body.userId ,req.body.albumId,function(err, albums){
        if(err){
            return next(err);
        }
        res.send(albums);
    })
});

router.post('/deleteContent', function(req,res,next){

    album.deleteAlbumContent(req.body.userId ,req.body.albumId, req.body.contentId,function(err, albums){
        if(err){
                return next(err);
        }
        res.send(albums);
    })
});



// router.get('/album/:mobileNo', function(req, res, next) {

//     if(req.params.mobileNo) {
//         aws.getAlbum(config.BUCKET_NAME, 'images/'+req.params.mobileNo+'/album/', function (err, data) {
//             if (err) {
//                 return next(err);
//             } else {
//                 res.send(data);
//             }


//         });
//     }

// })


var storage = multer.diskStorage({
    
    destination: function(req, file, callback) {

        var filePath = './public/images/'
        callback(null, filePath)
    },

    filename: function(req, file, callback) {
        var fileName = new Date().getTime().toString()+"_post"+path.extname(file.originalname);
        callback(null,  fileName);
    }


})


router.post('/albumUpload/:mobileNo', function(req, res, next) {

    console.log(req.params.mobileNo);
    var upload = multer({
        storage: storage
    }).array( "filename", 10);
    upload(req, res, function(err, file) {
        let fileNames = [];
        if(req.params.mobileNo) {
            uploadPics(req.files, req.params.mobileNo, req.body.albumId, function(err, files)  {
                let fileArray = files.map(file => {
                    return file.name;
                });
                
                let contents = [];
                files.forEach(file => {

                    let content = {
                        type: file.mineType.substring(0, file.mineType.indexOf('/')),
                        content: file.name
                    }
                    contents.push(content);
                });
                
                    var updatedAlbum = {
                        id: parseInt(req.body.albumId),
                        content: contents
                    };
                console.log(updatedAlbum);

                album.updateAlbum(updatedAlbum, function (err, albums) {
                    if (err) {
                        return next(err);
                    } else {
                        res.send(albums);
                    }
                });

            });
        }

    });
});

function uploadPics(files, mobileNo ,postId, callback) {
    let fileNames = [];

    var filesUploads = files.map(file => uploadPic(file, mobileNo ,postId));
    var promises = all(filesUploads);

    promises.then(function(values) {
        return callback(null, values);
    });
}

function uploadPic(file, mobileNo, postId) {
    var deferred =   defer();
    aws.createAlbum(config.BUCKET_NAME + '/images', mobileNo, function (err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            aws.createAlbum(config.BUCKET_NAME + '/images/' + mobileNo, 'album', function (err, data) {
                if (err) {
                    deferred.reject(err);
                } else {
                    aws.createAlbum(config.BUCKET_NAME + '/images/' + mobileNo + '/album', postId, function (err, data) {
                        if (err) {
                            deferred.reject(err);
                        } else {
                            aws.awsUpload(config.BUCKET_NAME + '/images/' + mobileNo + '/album/' + postId,
                                file, file.filename, function (err, data) {
                                    if (err) {
                                        deferred.reject(err);
                                    } else {
                                        deferred.resolve({name:file.filename, mineType:file.mimetype});
                                    }

                                })
                        }
                    });
                }
            });
        }
    });

    return deferred.promise;
}



module.exports = router;
