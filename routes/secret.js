var express = require('express');
var router = express.Router();
var secret = require('../model/user/secret');



router.post('/mates', function(req, res, next){

    secret.addSecretMates(req.body.fromuserId, req.body.touserId, req.body.secret ,function(err, secretMates){
        if(err){
                return next(err);
        } else {
            res.send(secretMates);
        }

    })

})

router.post('/group', function(req, res, next){

    secret.addSecretGroup(req.body.groupId, req.body.userId, req.body.secret, req.body.isCreated,function(err, secretMates){
        if(err){
            return next(err);
        } else {
            res.send(secretMates);
        }

    })

})


module.exports = router;